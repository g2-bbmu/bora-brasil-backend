#!/bin/bash
set -e

rm -rf package-lock.json
echo "\n===================================================\nCommand rm -rf package-lock.json executed at"
date
echo "===================================================\n"

yarn install
echo "\n===================================================\nCommand yarn install executed at"
date
echo "===================================================\n"
cd ../cms
yarn install

echo "\n===================================================\nCommand yarn install executed at"
date
echo "===================================================\n"

cd ../backend
yarn build:docker
echo "\n===================================================\nCommand yarn build:docker executed at"
date
echo "===================================================\n"

cd ../cms
yarn build:prod
echo "\n===================================================\nCommand yarn build:prod executed at"
date
echo "===================================================\n"

cd ../ 
docker build -t gcr.io/bora-brasil-g2/taxi:latest .
TMPFILE=$(mktemp)
docker push gcr.io/bora-brasil-g2/taxi:latest | tee -a "$TMPFILE"
DIGEST=$(awk '/digest: / {print $3}' "$TMPFILE")
rm "$TMPFILE"
echo "\n===================================================\nCommand docker build -t gcr.io/bora-brasil-g2/taxi:latest . && docker push gcr.io/bora-brasil-g2/taxi:latest executed at" 
date 
echo "===================================================\n"


gcloud container clusters get-credentials bora-brasil --zone southamerica-east1-a --project bora-brasil-g2
kubectl set image deployment/taxi taxi=gcr.io/bora-brasil-g2/taxi:latest@"$DIGEST"

# 1 - Dar npm install nas pastas backend/backend e backend/cms
# 2 - Buildar backend -> terminal na pasta backend/backend rodar npm run build:docker
# 3 - Buildar cms -> terminal na pasta backend/cms rodar npm run build:prod
# 4 - Buildar Dockerfile -> terminal na pasta /backend rodar o comando dentro do arquivo Dockerfile docker build -t gcr.io/bora-brasil-g2/taxi:latest .
# 5 - Subir na gcloud
# - Primeiro tem que conseguir acesso ao cluster do bora brasil, precisa ter o gcloud sdk instalado e acesso ao projeto como kubernetes admin ou dev
# - Apontar o kubectl para o cluster do bora brasil (Só precisa rodar 1 vez pra apontar, e outras vezes caso for apontar pra outros clusters) 
# gcloud container clusters get-credentials bora-brasil --zone southamerica-east1-a --project bora-brasil-g2
# - Na pasta ./kubernetes/scripts, dar permissão e rodar o arquivo taxi-stag.sh
# chmod +x taxi-stag.sh
# ./taxi-stag.sh

# docker build -t gcr.io/bora-brasil-g2/taxi:latest . && docker push gcr.io/bora-brasil-g2/taxi:latest