import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";
import {PaymentGatewayDocumentsStatus} from '../entities/driver';

export class AlterTableDriver1598391381312 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('driver', new TableColumn({
            name: 'paymentGatewayDocumentsStatus',
            type: 'enum',
            enumName: 'paymentGatewayDocumentsStatus',
            enum: [
                PaymentGatewayDocumentsStatus.NaoEnviado,
                PaymentGatewayDocumentsStatus.EmAnalise,
                PaymentGatewayDocumentsStatus.AprovadoManualmente,
                PaymentGatewayDocumentsStatus.AprovadoAutomaticamente,
                PaymentGatewayDocumentsStatus.Negado
            ],
            default: `"${PaymentGatewayDocumentsStatus.NaoEnviado}"`
        }));

        await queryRunner.addColumns('driver', [
            new TableColumn({
                name: 'paymentGatewayDocumentsPendingDate',
                type: 'date',
                isNullable: true
            }),
            new TableColumn({
                name: 'paymentGatewayDocumentsActivatedDate',
                type: 'date',
                isNullable: true
            }),
            new TableColumn({
                name: 'paymentGatewayDocumentsEnabledDate',
                type: 'date',
                isNullable: true
            }),
            new TableColumn({
                name: 'paymentGatewayDocumentsDeniedDate',
                type: 'date',
                isNullable: true
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('driver', 'paymentGatewayDocumentsPendingDate');
        await queryRunner.dropColumn('driver', 'paymentGatewayDocumentsActivatedDate');
        await queryRunner.dropColumn('driver', 'paymentGatewayDocumentsEnabledDate');
        await queryRunner.dropColumn('driver', 'paymentGatewayDocumentsDeniedDate');
        await queryRunner.dropColumn('driver', 'paymentGatewayDocumentsStatus');
    }

}
