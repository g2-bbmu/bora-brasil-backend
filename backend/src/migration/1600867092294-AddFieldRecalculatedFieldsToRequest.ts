import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AddFieldRecalculatedFieldsToRequest1600867092294
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "recalculatedTravelTime",
        type: "int",
        isNullable: true,
      }),
    );

    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "recalculatedTravelDistance",
        type: "decimal",
        precision: 12,
        isNullable: true,
      }),
    );

    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "recalculatedTravelPath",
        type: "text",
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("request", "recalculatedTravelPath");
    await queryRunner.dropColumn("request", "recalculatedTravelDistance");
    await queryRunner.dropColumn("request", "recalculatedTravelTime");
  }
}
