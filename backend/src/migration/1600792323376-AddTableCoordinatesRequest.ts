import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from "typeorm";

export class AddTableCoordinatesRequest1600792323376
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "coordinates_request",
        columns: [
          {
            name: "id",
            type: "int",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "request_id",
            type: "int",
          },
          {
            name: "coordX",
            type: "decimal",
            precision: 9,
            scale: 6,
          },
          {
            name: "coordY",
            type: "decimal",
            precision: 8,
            scale: 6,
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("coordinates_request");
  }
}
