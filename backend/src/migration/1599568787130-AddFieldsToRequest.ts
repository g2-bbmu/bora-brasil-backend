import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AddFieldsToRequest1599568787130 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "estimatedTravelTime",
        type: "int",
        isNullable: true,
      }),
    );

    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "estimatedTravelDistance",
        type: "decimal",
        precision: 12,
        isNullable: true,
      }),
    );

    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "estimatedTravelPath",
        type: "varchar",
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("request", "estimatedTravelPath");
    await queryRunner.dropColumn("request", "estimatedTravelDistance");
    await queryRunner.dropColumn("request", "estimatedTravelTime");
  }
}
