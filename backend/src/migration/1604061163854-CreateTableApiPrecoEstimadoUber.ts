import { query } from 'express';
import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateTableApiPrecoEstimadoUber1604061163854 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE \`api_preco_estimado_uber\` (
            \`id\` int(11) NOT NULL AUTO_INCREMENT,
            \`startLat\` varchar(500) DEFAULT NULL,
            \`startLong\` varchar(500) DEFAULT NULL,
            \`endLat\` varchar(500) DEFAULT NULL,
            \`endLong\` varchar(500) DEFAULT NULL,
            \`id_produto_uber\` varchar(255) DEFAULT NULL,
            \`produto_uber\` varchar(200) DEFAULT NULL,
            \`maior_preco\` decimal(18,2) DEFAULT NULL,
            \`menor_preco\` decimal(18,2) DEFAULT NULL,
            \`distancia\` varchar(45) DEFAULT NULL,
            \`duracao\` varchar(45) DEFAULT NULL,
            \`id_categoria_bora\` int(11) DEFAULT NULL,
            \`categoria_bora\` varchar(45) DEFAULT NULL,
            \`cidade\` varchar(255) DEFAULT NULL,
            \`uf\` varchar(30) DEFAULT NULL,
            \`data_consulta\` datetime DEFAULT NULL,
            \`idRider\` int(11) DEFAULT NULL,
            \`bairro\` varchar(255) DEFAULT NULL,
            \`porcentagem_aplicada\` decimal(18,3) DEFAULT NULL,
            \`valor_calculado\` decimal(18,2) DEFAULT NULL,
            PRIMARY KEY (\`id\`)
          ) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='retorno de consultas de trajetos com valores estimados por produto uber';
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('api_preco_estimado_uber')
    }

}
