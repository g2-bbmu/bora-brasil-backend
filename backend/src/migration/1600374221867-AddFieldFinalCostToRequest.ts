import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AddFieldFinalCostToRequest1600374221867
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "finalCost",
        type: "decimal",
        precision: 12,
        isNullable: true,
      })
    );

    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "accomplishedDuration",
        type: "integer",
        isNullable: true,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("request", "accomplishedDuration");
    await queryRunner.dropColumn("request", "finalCost");
  }
}
