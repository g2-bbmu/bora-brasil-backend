import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class ModifyFieldInviationCode1602102681340
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "driver",
      new TableColumn({
        name: "invitationCodeReceived",
        type: "varchar",
        isNullable: true,
        default: "' '",
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("driver", "invitationCodeReceived");
  }
}
