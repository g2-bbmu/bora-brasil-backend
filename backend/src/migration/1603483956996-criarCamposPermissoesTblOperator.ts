import {MigrationInterface, QueryRunner} from "typeorm";
import {TableColumn} from "typeorm/index";

export class criarCamposPermissoesTblOperator1603483956996 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('operator', new TableColumn({
            name: 'permissionChangeIndicadorMotorista',
            type: 'int',
            isNullable: true
        }));

        await queryRunner.addColumn('operator', new TableColumn({
            name: 'permissionChangeFranquiaMotorista',
            type: 'int',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
