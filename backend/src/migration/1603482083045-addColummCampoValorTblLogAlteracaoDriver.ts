import {MigrationInterface, QueryRunner} from "typeorm";
import {TableColumn} from "typeorm/index";

export class addColummCampoValorTblLogAlteracaoDriver1603482083045 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('log_alteracao_Driver', new TableColumn({
            name: 'campo',
            type: 'varchar',
            isNullable: true
        }));

        await queryRunner.addColumn('log_alteracao_Driver', new TableColumn({
            name: 'valor_anterior',
            type: 'varchar',
            isNullable: true
        }));

        await queryRunner.addColumn('log_alteracao_Driver', new TableColumn({
            name: 'valor_atual',
            type: 'varchar',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
