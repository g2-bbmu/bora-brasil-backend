import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AddNewFieldsToRequest1600549164701 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "pickupTime",
        type: "int",
        isNullable: true,
      })
    );

    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "pickupDistance",
        type: "decimal",
        precision: 12,
        isNullable: true,
      })
    );

    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "pickupPath",
        type: "TEXT",
        isNullable: true,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("request", "pickupPath");
    await queryRunner.dropColumn("request", "pickupDistance");
    await queryRunner.dropColumn("request", "pickupTime");
  }
}
