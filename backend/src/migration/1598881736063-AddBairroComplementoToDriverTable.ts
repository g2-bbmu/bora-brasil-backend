import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AddBairroComplementoToDriverTable1598881736063 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('driver', new TableColumn({
            name: 'bairro',
            type: 'varchar',
            isNullable: true
        }));

        await queryRunner.addColumn('driver', new TableColumn({
            name: 'complemento',
            type: 'varchar',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('driver', 'complemento');
        await queryRunner.dropColumn('driver', 'bairro');
    }

}
