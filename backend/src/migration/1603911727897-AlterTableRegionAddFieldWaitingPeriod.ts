import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterTableRegionAddFieldWaitingPeriod1603911727897 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
          "region",
          new TableColumn({
            name: "waitingPeriodMinutes",
            type: "tinyint",
            isNullable: true,
          })
        );
      }
    
      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("region", "waitingPeriodMinutes");
      }

}
