import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AddFieldCalledFinishToRequest1601132766758
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "request",
      new TableColumn({
        name: "calledFinish",
        type: "boolean",
        isNullable: true,
        default: false,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("request", "calledFinish");
  }
}
