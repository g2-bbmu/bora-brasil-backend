import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class UpdateUsuarioAddIdDriver1603904084312 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('usuario', new TableColumn({
            name: 'idDriver',
            type: 'int',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('usuario', 'idDriver')
    }
}
