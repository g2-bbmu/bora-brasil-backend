import { table } from "console";
import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AddFieldInvitationCodetoDriver1601404656428
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "driver",
      new TableColumn({
        name: "invitationCodeReceived",
        type: "int",
        isNullable: true,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("driver", "invitationCodeReceived");
  }
}
