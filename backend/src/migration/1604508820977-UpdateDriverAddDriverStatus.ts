import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateDriverAddDriverStatus1604508820977 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`driver\` 
            CHANGE COLUMN \`status\` \`status\` ENUM('online', 'offline', 'blocked', 'in service', 'waiting documents', 'pending approval', 'soft reject', 'hard reject', 'expired') NOT NULL DEFAULT 'waiting documents'`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`driver\` 
            CHANGE COLUMN \`status\` \`status\` ENUM('online', 'offline', 'blocked', 'in service', 'waiting documents', 'pending approval', 'soft reject', 'hard reject') NOT NULL DEFAULT 'waiting documents'`)
    }

}
