import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class UpdateRequestAddOSAndVersion1602703609513 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumns(
          "request",
          [
            new TableColumn({
                name: "riderRequestOS",
                type: "varchar",
                isNullable: true,
            }),
            new TableColumn({
                name: "riderRequestVersion",
                type: "varchar",
                isNullable: true,
            }),
            new TableColumn({
                name: "driverRequestOS",
                type: "varchar",
                isNullable: true,
            }),
            new TableColumn({
                name: "driverRequestVersion",
                type: "varchar",
                isNullable: true,
            }),
          ]
        );
      }
    
      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("request", "riderRequestOS");
        await queryRunner.dropColumn("request", "riderRequestVersion");
        await queryRunner.dropColumn("request", "driverRequestOS");
        await queryRunner.dropColumn("request", "driverRequestVersion");
      }

}
