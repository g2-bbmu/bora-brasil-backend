import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AddFieldToRequest1600198348283 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('request', new TableColumn({
            name: 'isChecked',
            type: 'boolean',
            default: false
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
