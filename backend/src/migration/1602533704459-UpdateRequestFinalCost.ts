import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateRequestFinalCost1602533704459 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE \`request\` CHANGE COLUMN \`finalCost\` \`finalCost\` DECIMAL(12,2) NULL DEFAULT NULL ;');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE \`request\` CHANGE COLUMN \`finalCost\` \`finalCost\` DECIMAL(12,0) NULL DEFAULT NULL ;');
    }

}
