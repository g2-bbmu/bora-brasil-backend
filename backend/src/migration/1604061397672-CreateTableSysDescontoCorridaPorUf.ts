import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateTableSysDescontoCorridaPorUf1604061397672 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE \`sys_desconto_corrida_por_uf\` (
                \`uf\` varchar(5) DEFAULT NULL,
                \`coduf\` int(11) DEFAULT NULL,
                \`descricao\` varchar(30) DEFAULT NULL,
                \`porcentagem\` decimal(18,3) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('sys_desconto_corrida_por_uf')
    }

}
