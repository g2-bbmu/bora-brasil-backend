import {MigrationInterface, QueryRunner} from "typeorm";
import {PaymentGatewayDocumentsStatus} from "../entities/driver";
import {TableColumn} from "typeorm/index";

export class incluirCampoCnpjTabelaOperador1598454242384 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('operator', new TableColumn({
            name: 'cnpjFranquia',
            type: 'varchar',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('operator', 'cnpjFranquia');
    }

}
