import {
    MigrationInterface,
    QueryRunner,
    Table,
    TableForeignKey,
  } from "typeorm";

export class criarTabelaNetHomolog1603480342298 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
              name: "net",
              columns: [
                {
                  name: "up",
                  type: "int",
                },
                {
                  name: "down",
                  type: "int",
                },
                {
                  name: "level",
                  type: "int",
                },
              ],
            })
          );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
