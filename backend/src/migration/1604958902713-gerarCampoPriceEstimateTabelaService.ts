import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class gerarCampoPriceEstimateTabelaService1604958902713 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            "service",
            new TableColumn({
              name: "priceEstimate",
              type: "float",
              isNullable: false,
              default: 0.0,
            })
          );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("service", "priceEstimate");
    }

}
