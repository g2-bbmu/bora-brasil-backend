import {
    MigrationInterface,
    QueryRunner,
    Table,
    TableForeignKey,
  } from "typeorm";

export class CriarTabelaLogAlteracaoDriver1603403805514 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
              name: "log_alteracao_Driver",
              columns: [
                {
                  name: "id",
                  type: "int",
                  isPrimary: true,
                },
                {
                  name: "idOperator",
                  type: "int",
                },
                {
                  name: "idDriver",
                  type: "int",
                },
                {
                  name: "data_alteracao",
                  type: "timestamp",
                  default: 'now()',
                },
                {
                    name: "tabela",
                    type: "varchar",
                    isNullable: true,
                  },
              ],
            })
          );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
