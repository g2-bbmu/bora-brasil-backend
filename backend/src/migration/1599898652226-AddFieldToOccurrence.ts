import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AddFieldToOccurrence1599898652226 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "occurrences",
      new TableColumn({
        name: "startDate",
        type: "datetime",
        isNullable: true,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("occurrences", "startDate");
  }
}
