import { MigrationInterface, QueryRunner, TableForeignKey } from "typeorm";

export class AddTableCoordinatesRequestForeignKey1600793374527
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createForeignKey(
      "coordinates_request",
      new TableForeignKey({
        name: "coordinates-request",
        columnNames: ["request_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "request",
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      "coordinates_request",
      "coordinates-request"
    );
  }
}
