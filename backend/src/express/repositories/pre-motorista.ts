import { PreMotorista } from '../../entities/pre-motorista'

export const findPreMotoristaByEmail = async (email: string) => PreMotorista.findOne({ where: { email }});
export const findPreMotoristaByCpf = async (cpf: string) => PreMotorista.findOne({ where: { cpf } });
export const findPreMotoristaByContatoMovel = async (contatoMovel: string) => PreMotorista.findOne({ where: { contatoMovel } });