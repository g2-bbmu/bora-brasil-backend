import { getConnection } from 'typeorm';
import { Driver } from "../../entities/driver";
import { MediaType } from '../../entities/media';

export const findDriverByEmail = async (email: string) => Driver.findOne({ where: { email } });
export const findDriverByCpf = async (cpf: string) => Driver.findOne({ where: { cpf } });
export const findDriverByMobileNumber = async (mobileNumber: number) => Driver.findOne({ where: { mobileNumber } });
export const findDriverById = async (id: number) => Driver.findOne(id);

export const findDriverSeparateDocumentsByDriverId = async (id: number) => {
    const documents = await getConnection().createQueryBuilder().relation(Driver, 'documents').of(id).loadMany();

    const driverMedia = documents.filter((media) => media.type === MediaType.DriverImage)?.[0];
    const proofOfAddressMedia = documents.filter((media) => media.type === MediaType.ProofOfAddress)?.[0];
    const carLocationContractMedia = documents.filter((media) => media.type === MediaType.CarLocationContract)?.[0];
    const cnhMedia = documents.filter((media) => media.type === MediaType.Cnh)?.[0];
    const selfieMedia = documents.filter((media) => media.type === MediaType.Selfie)?.[0];
    const carMedia = documents.filter((media) => media.type === MediaType.CarImage)?.[0];
    const carDocumentMedia = documents.filter((media) => media.type === MediaType.CarDocument)?.[0];

    return {
        driverMedia,
        proofOfAddressMedia,
        carLocationContractMedia,
        cnhMedia,
        selfieMedia,
        carMedia,
        carDocumentMedia
    }
}