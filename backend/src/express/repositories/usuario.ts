import { Usuario } from '../../entities/usuario';

export const findUsuarioByEmail = async (email: string) => Usuario.findOne({
    where: {
        login: email
    }
})