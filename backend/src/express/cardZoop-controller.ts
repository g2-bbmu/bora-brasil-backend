import * as express from "express";
import {
  ZoopApi,
  marketplaceId,
  getBuyerByCPF,
  getCardById,
  updateCardById,
  deleteCardById,
} from "../services/PaymentApi";
import ExpressController from "./interfaces/express.controller.interface";

export class CardZoopController implements ExpressController {
  path = "/cardZoop";
  router: express.Router;

  constructor() {
    this.router = express.Router({});
    this.router.get("/:cpf", this.cardDetails);
    this.router.put("/:cpf", this.updateCardDetails);
    this.router.delete("/:cpf", this.deleteCard);
  }

  async cardDetails(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const { cpf } = req.params;
    const card = await getBuyerByCPF(marketplaceId, cpf);
    const { default_debit } = card.data;

    if (!default_debit) return res.json({ cards: [] });

    const cardDetails = await getCardById(marketplaceId, default_debit);

    return res.json({ cards: [cardDetails.data] });
  }

  async updateCardDetails(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const { cpf } = req.params;
    const data = req.body;

    const card = await getBuyerByCPF(marketplaceId, cpf);

    const { default_debit } = card.data;

    if (!default_debit) return res.json({ cards: [] });

    const cardDetails = await updateCardById(
      marketplaceId,
      default_debit,
      data
    );
    return res.json({ cards: [cardDetails.data] });
  }

  async deleteCard(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const { cpf } = req.params;

    const card = await getBuyerByCPF(marketplaceId, cpf);

    const { default_debit } = card.data;

    await deleteCardById(marketplaceId, default_debit);

    return res.status(204).send();
  }
}
