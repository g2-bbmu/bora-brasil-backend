import { BankAccountType } from '../../entities/bank-account';
import { CnhCategory } from '../../entities/driver';
import { Gender } from '../../models/enums/enums';

export interface BankAccountDto {
    holder: string;
    cpfCnpj: string;
    bankCode: string;
    agencyCode: string;
    accountNumber: string;
    type: BankAccountType;
}

interface ServiceDto {
    id: number;
}

interface DriverDto {
    email: string;
    firstName: string;
    lastName: string;
    cpf: string;
    gender: Gender;
    certificateNumber: string;
    cnhEar: boolean;
    cnhExpirationDate: number; // Timestamp
    cnhCategory: CnhCategory;
    cep: string;
    address: string;
    number: string;
    addressTwo?: string;
    city: string;
    state: string;
    carStatus: boolean; // Carro próprio ou alugado
    carPlate: string;
    carColor: string;
    carProductionYear: number;
    carId: number; // Modelo do veículo
    services: ServiceDto[] // Categorias
    birthDate?: string;
    website?: string;
    description?: string;
}

export interface RegisterRequestDto {
    invitationCodeReceived?: string;
    token: string;
    driver: DriverDto;
    bankAccount: BankAccountDto;
}
