import HttpException, { HTTPStatus } from "./http-exception";

export default class UnknownException extends HttpException {
    constructor(message: string) {
        super(HTTPStatus.BadRequest, `Unkown Exception occured: ${message}`)
    }
}