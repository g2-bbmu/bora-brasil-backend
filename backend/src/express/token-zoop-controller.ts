import * as express from 'express'
import {connectCardWithBuyer, createCardToken, getBuyerByCPF, marketplaceId, ZoopApi} from '../services/PaymentApi';
import ExpressController from './interfaces/express.controller.interface';

export interface Teste {
    id: string;
}

export class TokenZoopController implements ExpressController {
    path = "/tokenZoop"
    router: express.Router;

    constructor() {
        this.router = express.Router({});
        this.router.post('/credit/:cpf', this.addTokenCreditCard);
    }

    async addTokenCreditCard(req: express.Request, res: express.Response, next: express.NextFunction) {
        try {
            const {
                holder_name,
                expiration_month,
                expiration_year,
                card_number,
                security_code,
            } = req.body;

            const {cpf} = req.params;

            const buyer = await getBuyerByCPF(marketplaceId, cpf)

            const {id} = buyer.data;

            const token = await createCardToken(marketplaceId, {
                holder_name,
                expiration_month,
                expiration_year,
                card_number,
                security_code,
            });

            const cardData = {
                token: token.data.id,
                customer: id,
            }

            const card = await connectCardWithBuyer(marketplaceId, cardData);

            return res.status(200).json(card.data);
        } catch(err) {
            console.log(err)
            return res.sendStatus(500)
        }

    }
}