import {Driver} from "../../entities/driver";
import {PreMotorista} from "../../entities/pre-motorista";
import {Usuario} from "../../entities/usuario";
import { findDriverByCpf, findDriverByEmail, findDriverByMobileNumber } from '../repositories/driver';
import { findPreMotoristaByContatoMovel, findPreMotoristaByCpf, findPreMotoristaByEmail } from '../repositories/pre-motorista';
import { findUsuarioByEmail } from '../repositories/usuario';

export enum ValidateDriverExistsField {
    EMAIL = 'EMAIL',
    TELEFONE = 'TELEFONE',
    CPF = 'CPF'
}

export enum ValidateDriverExistsInAllTablesResponse {
    CPF_EXISTS = 'CPF_EXISTS',
    EMAIL_EXISTS = 'EMAIL_EXISTS',
    TELEFONE_EXISTS = 'TELEFONE_EXISTS'
}

export async function validateDriverExistsInAllTables(driver: Driver): Promise<ValidateDriverExistsInAllTablesResponse> {
    const [ 
            driverEmail,
            driverTelefone,
            driverCpf,
            preMotoristaEmail,
            preMotoristaCpf,
            preMotoristaTelefone,
            usuarioEmail 
        ] = await Promise.all([
            findDriverByEmail(driver.email),
            findDriverByMobileNumber(driver.mobileNumber),
            findDriverByCpf(driver.cpf),
            findPreMotoristaByEmail(driver.email),
            findPreMotoristaByContatoMovel(String(driver.mobileNumber)),
            findPreMotoristaByCpf(driver.cpf),
            findUsuarioByEmail(driver.email)
        ])

    if (driverCpf || preMotoristaCpf) {
        return ValidateDriverExistsInAllTablesResponse.CPF_EXISTS;
    }

    if (usuarioEmail || driverEmail || preMotoristaEmail) {
        return ValidateDriverExistsInAllTablesResponse.EMAIL_EXISTS;
    }

    if (driverTelefone || preMotoristaTelefone) {
        return ValidateDriverExistsInAllTablesResponse.TELEFONE_EXISTS;
    }
}