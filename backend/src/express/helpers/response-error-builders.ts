export function cpfAlreadyExistsResponseError(res: any) {
  return res.status(401).json({
    message: "Esse número de cpf já foi utilizado em um cadastro anteriormente",
  });
}

export function emailAlreadyExistsResponseError(res: any) {
  return res.status(401).json({
    message: "Esse email já foi utilizado em um cadastro anteriormente",
  });
}

export function driverAlreadyRegisteredError(res: any) {
  return res
    .status(400)
    .json({ message: "Driver registration is already completed" });
}

export function telefoneAlreadyExistsResponseError(res: any) {
  return res
    .status(400)
    .json({
      message:
        "Esse número de celular já foi utilizado em um cadastro anteriormente",
    });
}
