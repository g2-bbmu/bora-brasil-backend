import * as express from "express";
import Container from "typedi";
import { Driver } from "../entities/driver";
import { PreMotorista } from "../entities/pre-motorista";
import { Usuario } from "../entities/usuario";
import jwt from "jsonwebtoken";

import ExpressController from "./interfaces/express.controller.interface";

export class UsuarioController implements ExpressController {
  path = "/users";
  router: express.Router;

  constructor() {
    this.router = express.Router({});
    this.router.get("/:id", this.show);
  }

  async show(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const { id } = req.params;
    const usuario = await Usuario.findOne(id);

    if (!usuario)
      return res.sendStatus(404).json({ message: "User not found" });

    const preMotorista = await PreMotorista.findOne({
      where: { email: usuario.login },
    });

    if (!preMotorista)
      return res.sendStatus(404).json({ message: "User not found" });

    const indicadoPorCod =
      preMotorista.nivel5 ||
      preMotorista.nivel4 ||
      preMotorista.nivel3 ||
      preMotorista.nivel2 ||
      preMotorista.nivel1;

    if (!indicadoPorCod) {
    }

    let indicadoPor = "";

    const findUser = await Usuario.findOne(indicadoPorCod);
    indicadoPor = findUser.codIndicacao;

    const tokenJWT = jwt.sign(
      { id: usuario.id, t: "web" },
      Container.get("token"),
      { expiresIn: "1h" }
    );

    return res.json({
      codIndicacao: usuario.codIndicacao,
      cpf: preMotorista.cpf,
      email: preMotorista.email,
      indicadoPor: indicadoPor,
      nomeCompleto: preMotorista.nomeCompleto,
      telefone: preMotorista.contatoMovel,
      token: tokenJWT,
    });
  }
}
