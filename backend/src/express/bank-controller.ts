import * as express from 'express';
import ExpressController from "./interfaces/express.controller.interface";
import {Bank} from "../entities/bank";

export default class BankController implements ExpressController {
    path: string = "/bank";
    router: express.Router;

    constructor() {
        this.router = express.Router({});
        this.router.get('/', this.get);
    }

    async get(req: express.Request, res: express.Response, next: express.NextFunction) {
        const banks = await Bank.find({ order: { label: 'ASC' }});
        res.json({ banks });
    }
}