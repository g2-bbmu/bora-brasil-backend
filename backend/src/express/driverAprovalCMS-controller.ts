import * as express from "express";
import {
  connectBankAccountWithCustomer,
  createBankAccountToken,
  createBankAccountTransfer,
  createDocument,
  createIndividualSeller,
  getSellerAccountBalanceBySellerId,
  getSellerByCPFCNPJ,
  marketplaceId,
  ZoopWebhookEventTypes,
  updateIndividualSeller,
} from "../services/PaymentApi";
import ExpressController from "./interfaces/express.controller.interface";
import {
  Driver,
  DriverStatus,
  PaymentGatewayDocumentsStatus,
} from "../entities/driver";
import { Media, MediaType } from "../entities/media";
import FormData from "form-data";
import fs from "fs";
import {
  Authorize,
  TokenFromParamToHeader,
} from "./middlewares/authorize.middleware";
import { ClientType } from "../models/client-jwt-decoded";

export class DriverAprovalCMSController implements ExpressController {
  path = "/driver";
  router: express.Router;

  constructor() {
    this.router = express.Router({});
    this.router.post(
      `/sendToZoop/:id`,
      /* Authorize([ClientType.Operator]),*/ this.sendToZoop.bind(this)
    );
    this.router.post(
      "/sellerEnabledOrActivated/:token",
      TokenFromParamToHeader,
      Authorize([ClientType.Zoop]),
      this.changeDriverStatus.bind(this)
    );
    this.router.get(
      "/balance/:id",
      Authorize([ClientType.Driver]),
      this.getDriverBalance.bind(this)
    );
    this.router.post(
      "/withdrawal/:id",
      Authorize([ClientType.Driver]),
      this.requestWithdrawal.bind(this)
    );
  }

  async sendToZoop(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const { id } = req.params;

    const driver = await Driver.findOne(id, {
      relations: ["bankAccount", "documents"],
    });

    let seller = await getSellerByCPFCNPJ(marketplaceId, driver.cpf);

    if (!seller) {
      const createSellerData = {
        first_name: driver.firstName,
        last_name: driver.lastName,
        email: driver.email,
        phone_number: driver.mobileNumber,
        taxpayer_id: driver.cpf,
        address: {
          line1: driver.address,
          line2: driver.number,
          city: driver.city,
          state: driver.state,
          postal_code: driver.cep,
          neighborhood: driver.bairro,
        },
      };

      seller = await createIndividualSeller(marketplaceId, createSellerData);
    } else {
      const updateSellerData = {
        first_name: driver.firstName,
        last_name: driver.lastName,
        email: driver.email,
        phone_number: driver.mobileNumber,
        taxpayer_id: driver.cpf,
        address: {
          line1: driver.address,
          line2: driver.number,
          city: driver.city,
          state: driver.state,
          postal_code: driver.cep,
          neighborhood: driver.bairro,
        },
      };

      seller = await updateIndividualSeller(
        marketplaceId,
        seller.data.id,
        updateSellerData
      );
    }

    let cpf: string = undefined;
    let cnpj: string = undefined;

    if (driver.bankAccount.cpfCnpj.length === 11) {
      cpf = driver.bankAccount.cpfCnpj;
    } else {
      cnpj = driver.bankAccount.cpfCnpj;
    }

    const createBankTokenData = {
      holder_name: driver.bankAccount.holder,
      bank_code: driver.bankAccount.bankCode,
      routing_number: driver.bankAccount.agencyCode,
      account_number: driver.bankAccount.accountNumber,
      taxpayer_id: cpf,
      ein: cnpj,
      type: driver.bankAccount.type,
    };

    const tokenBank = await createBankAccountToken(
      marketplaceId,
      createBankTokenData
    );

    await connectBankAccountWithCustomer(marketplaceId, {
      customer: seller.data.id,
      token: tokenBank.data.id,
    });

    let sellerId: string = seller.data.id;

    const driverMedia = driver.documents.filter(
      (media) => media.type === MediaType.DriverImage
    );

    const proofOfAddressMedia = driver.documents.filter(
      (media) => media.type === MediaType.ProofOfAddress
    );

    const carLocationContractMedia = driver.documents.filter(
      (media) => media.type === MediaType.CarLocationContract
    );

    const cnhMedia = driver.documents.filter((media) => media.type === MediaType.Cnh);

    const selfieMedia = driver.documents.filter(
      (media) => media.type === MediaType.Selfie
    );

    const carMedia = driver.documents.filter(
      (media) => media.type === MediaType.CarImage
    );

    const carDocumentMedia = driver.documents.filter(
      (media) => media.type === MediaType.CarDocument
    );

    const documents = [
      {
        media: driverMedia,
        category: MediaType.DriverImage,
        description: "Imagem do motorista",
      },
      {
        media: proofOfAddressMedia,
        category: MediaType.ProofOfAddress,
        description: "Comprovante de residência",
      },
      {
        media: carLocationContractMedia,
        category: MediaType.CarLocationContract,
        description: "Contrato de aluguel de carro",
      },
      {
        media: cnhMedia,
        category: MediaType.Cnh,
        description: "Cnh",
      },
      {
        media: selfieMedia,
        category: MediaType.Selfie,
        description: "Selfie com cnh",
      },
      {
        media: carMedia,
        category: MediaType.CarImage,
        description: "Foto do carro",
      },
      {
        media: carDocumentMedia,
        category: MediaType.CarDocument,
        description: "Foto do documento do carro",
      },
    ]
      .filter((document) => document.media.length > 0)
      .map((document) => ({
        ...document,
        path: `${process.cwd()}/public/${document.media[0].address}`,
      }))
      .filter((document) => fs.existsSync(document.path));

    for (let document of documents) {
      await this.sendFile(
        sellerId,
        document.category,
        document.description,
        fs.createReadStream(document.path)
      );
    }

    driver.paymentGatewayDocumentsStatus =
      PaymentGatewayDocumentsStatus.EmAnalise;
    driver.paymentGatewayDocumentsPendingDate = new Date();

    await Driver.save(driver);

    return res.json();
  }

  async changeDriverStatus(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const {
      type,
      payload: {
        object: { taxpayer_id },
      },
    } = req.body;

    if (type === ZoopWebhookEventTypes.Ping) return res.status(200).json();

    const driver = await Driver.findOne({ cpf: taxpayer_id });

    if (!driver) {
      throw new Error(
        `Foi informado que o motorista de CPF ${taxpayer_id} foi cadastrado no meio de pagamento, mas o mesmo não está cadastrado no banco de dados.`
      );
    }

    switch (type) {
      case ZoopWebhookEventTypes.SellerActivated:
        driver.paymentGatewayDocumentsStatus =
          PaymentGatewayDocumentsStatus.AprovadoAutomaticamente;
        driver.status = DriverStatus.Offline;
        driver.paymentGatewayDocumentsActivatedDate = new Date();
        break;
      case ZoopWebhookEventTypes.SellerEnabled:
        driver.paymentGatewayDocumentsStatus =
          PaymentGatewayDocumentsStatus.AprovadoManualmente;
        driver.status = DriverStatus.Offline;
        driver.paymentGatewayDocumentsEnabledDate = new Date();
        break;
      case ZoopWebhookEventTypes.SellerDenied:
        driver.paymentGatewayDocumentsStatus =
          PaymentGatewayDocumentsStatus.Negado;
        driver.status = DriverStatus.HardReject;
        driver.paymentGatewayDocumentsDeniedDate = new Date();
        break;
      default:
        return res.status(400).json();
    }

    await Driver.save(driver);

    return res.status(200).json();
  }

  async getDriverBalance(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    try {
      const { id } = req.params;

      const driver = await Driver.findOne(id);

      const seller = await getSellerByCPFCNPJ(marketplaceId, driver.cpf);

      const balance = await getSellerAccountBalanceBySellerId(
        marketplaceId,
        seller.data.id
      );

      const { current_balance, account_balance } = balance.data.items;

      return res.json({
        currentBalance: Number(current_balance) / 100,
        accountBalance: Number(account_balance) / 100,
      });
    } catch (error) {
      return res.status(404).json({ message: "Seller not found!" });
    }
  }

  async requestWithdrawal(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    try {
      // const token = req.headers.authorization;

      const { id } = req.params;

      const driver = await Driver.findOne(id);
      const seller = await getSellerByCPFCNPJ(marketplaceId, driver.cpf);

      // TODO: Tratar Default credit null
      if (!seller?.data?.default_credit) {
        return res.status(400).json({ message: "Bank account not found!" });
      }

      if (Number(seller?.data?.current_balance) <= 0) {
        return res.status(400).json({ message: "Insufficient Funds" });
      }

      await createBankAccountTransfer(
        marketplaceId,
        seller.data.default_credit,
        {
          amount: Number(seller.data.current_balance),
          description: "Transação Bora Brasil",
        }
      );

      return res.sendStatus(203);
    } catch (error) {
      return res.status(404).json({ message: "Seller not found!" });
    }
  }

  private async sendFile(
    sellerId: string,
    category: string,
    description: string,
    file: any
  ) {
    const form = new FormData();
    form.append("category", category);
    form.append("description", description);
    form.append("file", file);

    await createDocument(marketplaceId, sellerId, form);
  }
}
