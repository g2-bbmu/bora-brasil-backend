import ExpressController from "./interfaces/express.controller.interface";
import * as express from "express";
import axios from "axios";
import * as qs from "querystring";
import { PowerBI } from "../entities/powerbi";
import { Authorize } from "./middlewares/authorize.middleware";
import { ClientType } from "../models/client-jwt-decoded";

export default class PowerBiController implements ExpressController {
  path = "/powerbi";
  router: express.Router;

  constructor() {
    this.router = express.Router({});
    this.router.get(
      "/url",
      Authorize([ClientType.Web]),
      this.getPowerBIUrls.bind(this)
    );
    this.router.get(
      "/report/:id",
      Authorize([ClientType.Web]),
      this.getReportToken.bind(this)
    );
  }

  async getPowerBIUrls(req: express.Request, res: express.Response) {
    try {
      const urls = await PowerBI.find();

      return res.status(200).json(urls);
    } catch (err) {
      console.log(err);
      return res.sendStatus(500);
    }
  }

  async getReportToken(req: express.Request, res: express.Response) {
    try {
      const { id: reportId } = req.params;

      const accessToken = await this.getAccessToken();

      const config = {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      };

      const report = await PowerBI.findOne({
        where: { id: reportId },
      });

      const reportBi = await axios.get(
        `https://api.powerbi.com/v1.0/myorg/reports/${reportId}`,
        config
      );

      const { datasetId } = reportBi.data;

      const datasets = [
        {
          id: datasetId,
        },
      ];

      const reports = [
        {
          id: report.id,
        },
      ];

      const generatedToken = await axios.post(
        "https://api.powerbi.com/v1.0/myorg/GenerateToken",
        { datasets, reports, allowEdit: false },
        config
      );

      const response = {
        token: generatedToken.data.token,
        embedLink: report.url,
      };

      return res.status(200).json(response);
    } catch (err) {
      console.log(err);
      return res.sendStatus(500);
    }
  }

  private getAccessToken() {
    const configs = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "cache-control": "no-cache",
      },
    };

    const data = {
      grant_type: "password",
      scope: "openid",
      resource: "https://analysis.windows.net/powerbi/api",
      client_id: "e457b35d-31c2-44a2-97b8-8723f098648e",
      username: process.env.POWER_BI_USER,
      client_secret: "HJKlkxX54cGM55FMPM8G8F9WcykUAd53dUbVVK3MpgM=",
      password: process.env.POWER_BI_PASS,
    };

    return axios
      .post(
        "https://login.microsoftonline.com/common/oauth2/token",
        qs.stringify(data),
        configs
      )
      .then((response) => {
        return response.data.access_token;
      });
  }
}
