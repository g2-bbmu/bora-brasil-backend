import * as jwt from "jsonwebtoken";
import Container from "typedi";
import * as express from "express";
import {ClientType} from "../../models/client-jwt-decoded";

export const Authorize = (clientTypes: ClientType[]) => {
    if (!clientTypes.length) {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => next();
    }
    return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const [, token] = req.headers.authorization?.split(' ')

            if (!token) {
                return res.status(401).json({message: 'Bearer token é obrigatório'})
            }

            const decoded: any = jwt.verify(token, Container.get('token'));

            if (!clientTypes.includes(decoded.t)) return res.status(403).json({message: 'Recurso proibido'});

            (req as any).user = decoded;
            next();
        } catch (err) {
            return res.status(403).json({message: 'Recurso proibido'});
        }
    }
};

export const TokenFromParamToHeader = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    req.headers.authorization = `Bearer ${req.params.token}`
    next();
}