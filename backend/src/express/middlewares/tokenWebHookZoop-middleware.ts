import * as jwt from "jsonwebtoken";
import * as express from "express";

export const WebHookZoop = (token: string, req: express.Request, res: express.Response) => {
    try {
            if (!token) {
                return res.status(401).json({message: 'Zoop token é obrigatório'})
            }

            jwt.verify(token, process.env.JWT_SECRET);

        } catch (err) {
            return res.status(403).json({message: 'Recurso proibido'});
        }
    }
