import * as express from 'express';
import ExpressController from "./interfaces/express.controller.interface";
import {Car} from "../entities/car";

export default class CarController implements ExpressController {
    path: string = "/car";
    router: express.Router;

    constructor() {
        this.router = express.Router({});
        this.router.get('/', this.get);
    }

    async get(req: express.Request, res: express.Response, next: express.NextFunction) {
        const cars = await Car.find({ order: { title: 'ASC' }});

        res.json({ cars });
    }

}