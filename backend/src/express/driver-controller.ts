import * as express from "express";
import * as jwt from "jsonwebtoken";
import {Driver, DriverStatus} from "../entities/driver";
import ExpressController from "./interfaces/express.controller.interface";
import HardRejectException from "./exceptions/hard-reject.exception";
import {Service} from "../entities/service";
import {Media, MediaType} from "../entities/media";
import UnknownException from "./exceptions/unknown.exception";
import InvalidCredentialsException from "./exceptions/invalid-credentials.exception";
import {app} from "firebase-admin";
import Container from "typedi";
import Redis from "../libs/redis/redis";
import {Request, RequestStatus} from "../entities/request";
import ClientJWTDecoded, {ClientType} from "../models/client-jwt-decoded";
import {BankAccount} from "../entities/bank-account";
import {celebrate, Joi, Segments} from "celebrate";
import {validateBr} from "js-brasil";
import {RedeService} from "../services/rede-service";
import {DriverService} from "../services/driver-service";
import {Usuario} from "../entities/usuario";
import {Authorize} from "./middlewares/authorize.middleware";
import {findDriverByCpf} from "../services/findDriverByCpf";
import {PreMotorista} from "../entities/pre-motorista";
import {CoordinatesRequest} from "../entities/coordinates-request";
import {RegisterRequestDto} from "./dtos/drivers-dtos";
import {posCadastro} from '../libs/mail/emails';
import {sendEmail} from '../libs/mail';
import {generatePassword} from '../libs/password/password-generator';
import {
  validateDriverExistsInAllTables,
  ValidateDriverExistsInAllTablesResponse
} from "./helpers/validate-driver-exists-in-all-tables";
import {
  cpfAlreadyExistsResponseError,
  driverAlreadyRegisteredError,
  emailAlreadyExistsResponseError
} from "./helpers/response-error-builders";
import multer = require("multer");
import { findDriverSeparateDocumentsByDriverId } from './repositories/driver';

export interface Address {
  line1: string;
  line2?: string;
  line3?: string;
  neighborhood: string;
  city: string;
  state: string;
  postal_code: string;
  country_code: string;
}

export interface Location {
  x: number;
  y: number;
}

export interface DirectionsInfo {
  duration: number;
  distance: number;
  path: string;
}

export interface BodyData {
  token: string;
  directionsInfo: DirectionsInfo;
  location: Location;
  inTravel: boolean;
}

const storage = multer.diskStorage({
  destination(
    req: any,
    file: Express.Multer.File,
    callback: (error: Error | null, destination: string) => void
  ) {
    const address = `${process.cwd()}/public/img/${req.headers.type}`;
    console.log(`trying to upload into: ${address}`);
    callback(undefined, address);
  },
  filename(req, file, cb) {
    cb(undefined, `${Date.now()}.${file.originalname.split(".").pop()}`);
  },
});

const upload = multer({ storage });

export default class DriverController implements ExpressController {
  path = "/driver";
  router: express.Router;

  constructor() {
    this.router = express.Router({});
    this.router.get(
      "/invite_share_link/:id",
      Authorize([ClientType.Driver]),
      this.inviteShareLink
    );
    this.router.post("/upload", upload.single("file"), this.uploadFile);
    this.router.post("/login", this.login);
    this.router.post(
      "/register",
      celebrate({
        [Segments.BODY]: {
          token: Joi.string().required(),
          driver: Joi.object({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            gender: Joi.string().required(),
            cpf: Joi.string().required(),
            email: Joi.string().email().required(),
            address: Joi.string().required(),
            city: Joi.string().required(),
            number: Joi.string().required(),
            state: Joi.string().required(),
            cep: Joi.string().required(),
            addressTwo: Joi.string().allow("").optional(),
            mobileNumber: Joi.number().required(),
            carId: Joi.number().required(),
            carColor: Joi.string().required(),
            carPlate: Joi.string().required(),
            carStatus: Joi.boolean().required(),
            carProductionYear: Joi.number().required(),
            certificateNumber: Joi.string().required(),
            cnhEar: Joi.boolean().required(),
            cnhCategory: Joi.string().required(),
            cnhExpirationDate: Joi.date().required(),
            birthDate: Joi.string().optional(),
            website: Joi.string().optional(),
            description: Joi.string().optional(),
          })
            .required()
            .unknown(true),
          bankAccount: Joi.object({
            type: Joi.string().required(),
            holder: Joi.string().required(),
            cpfCnpj: Joi.string().required(),
            bankCode: Joi.string().required(),
            agencyCode: Joi.string().required(),
            accountNumber: Joi.string().required(),
          }).required(),
          //termVersion: Joi.string().required(),
          invitationCodeReceived: Joi.string().allow("").optional(),
        },
      }),
      this.register
    );
    this.router.post("/get", this.get);
    this.router.post("/update_location", this.updateLocation);
    this.router.get("/categories_cnh", this.getCategoriesCnh);
  }

  /**
   * Logins with firebase Auth Id Token as input. Returns JWT & profile if user is valid
   *
   * @param {express.Request} req In { token: 'x' } x being Firebase Id Token
   * @param {express.Response} res { user: Driver, token: 'y' } y being in app jwt token
   * @param {express.NextFunction} next Error handler
   * @returns
   * @memberof DriverController
   */
  async login(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    try {
      const decodedToken = await (Container.get("firebase.driver") as app.App)
        .auth()
        .verifyIdToken(req.body.token);

      const number = parseInt(
        (decodedToken.firebase.identities.phone[0] as string).substring(1)
      );

      let driver = await Driver.findOne({
        where: { mobileNumber: number },
        relations: ["media"],
      });

      const preMotoristaTelefone = await PreMotorista.findOne({
        where: { contatoMovel: number.toString() },
      });

      if (!driver && !preMotoristaTelefone) {
        let id = (await Driver.insert({ mobileNumber: number })).raw.insertId;
        driver = await Driver.findOne(id);
      }

      if (!driver && preMotoristaTelefone) {
        return res.status(403).json({ message: "Atualize seu cadastro!" });
      }
      
      let keys: ClientJWTDecoded = {
        id: driver.id,
        t: ClientType.Driver,
      };
      let token = jwt.sign(keys, Container.get("token"), {});
      switch (driver.status) {
        case DriverStatus.Blocked:
        case DriverStatus.HardReject:
          next(new HardRejectException(driver.documentsNote));
          break;

        default:
          res.json({ user: driver, token: token });
      }
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Takes firebase token & driver info and returns jwt token signed
   *
   * @param {express.Request} req in body there should be { token : 'x', driver: Driver, bankAccount: BankAccount, invitationCode: string } x being firebase Id Token
   * @param {express.Response} res returns { token: 'y' } y being app signed jwt token
   * @param {express.NextFunction} next in case Token is not valid
   * @memberof DriverController
   */
  async register(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const request: RegisterRequestDto = req.body;

    let decoded: any = jwt.verify(request.token, Container.get("token"));

    const driver = await Driver.findOne(decoded.id, {
      relations: [
        'bankAccount',
        'services'
      ]
    });

    if (driver == null) {
      return next(new InvalidCredentialsException());
    }

    if (isDriverRegistrationCompleted(driver)) {
      return driverAlreadyRegisteredError(res);
    }

    const cpfIsValid = validateBr.cpf(request.driver.cpf);

    if (!cpfIsValid) {
      return res.sendStatus(401).json({ error: "CPF invalid!" });
    }

    const driverCpf = await findDriverByCpf(request.driver.cpf);

    if (driverCpf) {
      if (!isDriverEditingProfile(driver) || !isTheSameDriver(driverCpf, driver)) {
        return cpfAlreadyExistsResponseError(res);
      }
    }

    
    driver.firstName = request.driver.firstName;
    driver.lastName = request.driver.lastName;
    driver.gender = request.driver.gender;
    driver.certificateNumber = request.driver.certificateNumber;
    driver.cnhEar = request.driver.cnhEar;
    driver.cnhExpirationDate = request.driver.cnhExpirationDate;
    driver.cnhCategory = request.driver.cnhCategory;
    driver.cep = request.driver.cep;
    driver.address = request.driver.address;
    driver.number = request.driver.number;
    driver.addressTwo = request.driver.addressTwo;
    driver.city = request.driver.city;
    driver.state = request.driver.state;
    driver.carStatus = request.driver.carStatus;
    driver.carPlate = request.driver.carPlate;
    driver.carColor = request.driver.carColor;
    driver.carProductionYear = request.driver.carProductionYear;
    driver.carId = request.driver.carId;

    driver.birthDate = request.driver.birthDate;
    driver.website = request.driver.website;
    driver.description = request.driver.description;

    if (!driver.bankAccount) {
      driver.bankAccount = BankAccount.create();
    }

    driver.bankAccount.accountNumber = request.bankAccount.accountNumber;
    driver.bankAccount.agencyCode = request.bankAccount.agencyCode;
    driver.bankAccount.bankCode = request.bankAccount.bankCode;
    driver.bankAccount.cpfCnpj = request.bankAccount.cpfCnpj;
    driver.bankAccount.holder = request.bankAccount.holder;
    driver.bankAccount.type = request.bankAccount.type;

    if (request.driver.address.split(",").length > 1) {
      driver.address = request.driver.address.split(",")[0].trim();
      driver.bairro = request.driver.address.split(",")[1].trim();
    }

    driver.services = request.driver.services.map(s => Service.create({ id: s.id }));

    if (!isDriverEditingProfile(driver)) {
      driver.email = request.driver.email;
      driver.cpf = request.driver.cpf;
      
      const response = await validateDriverExistsInAllTables(driver)

      if (response === ValidateDriverExistsInAllTablesResponse.CPF_EXISTS) {
        return cpfAlreadyExistsResponseError(res);
      }

      if (response === ValidateDriverExistsInAllTablesResponse.EMAIL_EXISTS) {
        return emailAlreadyExistsResponseError(res);
      }
    }

    driver.status = DriverStatus.PendingApproval;
    driver.completed = true;
    
    await BankAccount.save(driver.bankAccount)
    await Driver.save(driver);

    const [usuario] = await DriverService.criarUsuarioEPreMotorista(driver);
    const generatedPassword = generatePassword();

    usuario.senha = generatedPassword;
    usuario.senhaCriada = true;

    await Usuario.save(usuario);

    const codIndicacao: string = request.invitationCodeReceived;

    if (codIndicacao) {
      await RedeService.cadastrarUsuarioNaRede(usuario.id, codIndicacao);
    }

    const html = posCadastro(
      `${process.env.WEB_REGISTRATION_URL}cadastro-motorista?hash=${usuario.codIndicacao}`,
      usuario.login,
      generatedPassword,
    );

    sendEmail(usuario.login, "Bem vindo", html, null);


    return res.sendStatus(200);
  }
  /**
   * Gets Driver's registration Info.
   *
   * @param {express.Request} req In { token: 'x' } x being App verified JWT Token
   * @param {express.Response} res Return { driver: Driver, services?: Service[] } services only included when driver is not yet approved
   * @param {express.NextFunction} next Error Handler
   * @returns
   * @memberof DriverController
   */
  async get(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let decoded: any = jwt.verify(req.body.token, Container.get("token"));

    let driver = await Driver.findOne({
      where: { id: decoded.id },
      relations: [
        "media",
        "bankAccount",
        "car",
        "services",
      ],
    });

    if (driver) {
      const preMotorista = await PreMotorista.findOne({
        where: { email: driver.email },
      });

      if (
        (!driver.invitationCodeReceived ||
          driver.invitationCodeReceived == "") &&
        preMotorista &&
        preMotorista.indicador
      ) {
        const user = await Usuario.findOne(preMotorista.indicador);
        if (user && user.codIndicacao) {
          driver.invitationCodeReceived = user.codIndicacao;
          await Driver.save(driver);
        }
      }
    }

    let dadosFranquia = await Driver.query(
      "call retorna_dados_franquia_app_driver(?)",[decoded.id]
    );

    let driverUpdated = await Driver.findOne({
      where: { id: decoded.id },
      relations: [
        "media",
        "bankAccount",
        "car",
        "services",
      ],
    });

    if (driverUpdated == null) {
      next(new InvalidCredentialsException());
      return;
    }

    const driverDocuments = await findDriverSeparateDocumentsByDriverId(decoded.id);

    Object.assign(driverUpdated, driverDocuments);

    if (
      driverUpdated.status == DriverStatus.WaitingDocuments ||
      driverUpdated.status == DriverStatus.PendingApproval ||
      driverUpdated.status == DriverStatus.SoftReject
    ) {
      let services = await Service.find({ relations: ["media"] });
      res.json({ driver: driverUpdated, services: services, dadosFranquia: dadosFranquia[0]});
    } else {
      res.json({ driver: driverUpdated, dadosFranquia: dadosFranquia[0]});
    }
  }

  /**
   *
   *
   * @param {express.Request} req { token, location, inTravel }
   * @param {express.Response} res Status code only
   * @memberof DriverController
   */
  async updateLocation(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    try {
      const {
        token,
        location,
        inTravel,
        directionsInfo,
      } = req.body as BodyData;

      let decoded: any = jwt.decode(token, Container.get("token"));

      Container.get(Redis).driver.setCoordinate(decoded.id, location);

      if (inTravel) {
        let request = await Request.findOne({
          where: { driver: { id: decoded.id } },
          order: { id: "DESC" },
          loadRelationIds: true,
        });

        if (request.status === RequestStatus.Started) {
          const coords = CoordinatesRequest.create({
            coordX: location.x,
            coordY: location.y,
            request_id: request.id,
          });

          await CoordinatesRequest.save(coords);
        }

        const socketId = await Container.get(Redis).rider.getSocketIdByRiderId((request.rider as unknown) as number);
        (Container.get("io") as any)
          .of("/riders")
          .to(socketId)
          .emit("updateLocation", {
            token,
            directionsInfo,
            location,
            inTravel,
          });
      } else {
        (Container.get("io") as any).of("/cms").emit("driverLocationUpdated", {
          id: decoded.id,
          loc: { lat: location.y, lng: location.x },
        });
      }

      res.sendStatus(200);
    } catch (exception) {
      next(new UnknownException(exception));
    }
  }

  async uploadFile(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    try {
      const type = String(req.headers["type"]);

      validateUploadFileTypesOrThrowError(type);

      const address = makeUploadFilePath(req.headers.type, req.file.filename);

      const driver = await findDriverWithDocumentsByMobileNumber(String(req.headers["number"]));
      
      const media = await Media.save({
        type: type,
        address: address,
      } as Media);

      if (type === MediaType.DriverImage) {
        driver.media = media;
      } else {
        const index = driver.documents.findIndex(d => d.type === type);

        if (index === -1) {
          driver.documents.push(media);
        } else {
          driver.documents[index] = media;
        }
        
      }

      await Driver.save(driver);

      res.send({ id: media.id, media: media.address });
    } catch (error) {
      console.log(`error on upload: ${error}`);
      next(new UnknownException(`upload file ${error}`));
    }
  }

  async getCategoriesCnh(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const categoriesCnh = [
      "Categoria A",
      "Categoria B",
      "Categoria C",
      "Categoria D",
      "Categoria E",
      "Categoria AB",
      "Categoria AC",
      "Categoria AD",
      "Categoria AE",
    ];

    res.json({ categoriesCnh });
  }

  async inviteShareLink(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const { id } = req.params;

    const driver = await Driver.findOne(id);

    if (!driver) return res.status(404).json({ Message: "Driver not found!" });

    const user = await Usuario.findOne({ where: { login: driver.email } });

    if (!user) return res.status(404).json({ Message: "User not found!" });

    if (user.codIndicacao === null || user.codIndicacao === undefined) {
      return res
        .status(404)
        .json({ Message: "Indication code does not exist" });
    }

    return res.json({
      codInvite: process.env.LINK_DRIVER + "=" + user.codIndicacao,
    });
  }
}

function isTheSameDriver(driverCpf: Driver, driver: Driver): boolean {
  return (driverCpf.id === driver.id);
}

async function findDriverWithDocumentsByMobileNumber(mobileNumber: string): Promise<Driver> {
  return await Driver.findOne({
    where: { mobileNumber: mobileNumber },
    relations: ["documents"],
  });
}

function makeUploadFilePath(type, filename) {
  return `img/${type}/${filename}`;
}

function validateUploadFileTypesOrThrowError(type: string) {
  const allowedTypes = {
    "driver image": MediaType.DriverImage,
    "proof of address": MediaType.ProofOfAddress,
    "car location contract": MediaType.CarLocationContract,
    cnh: MediaType.Cnh,
    selfie: MediaType.Selfie,
    "car image": MediaType.CarImage,
    "car document": MediaType.CarDocument,
  };

  if (!allowedTypes[type]) {
    throw new Error("invalid type");
  }
}

function isDriverRegistrationCompleted(driver: Driver) {
  return driver.completed && driver.status != DriverStatus.WaitingDocuments;
}

function isDriverEditingProfile(driver: Driver) {
  return driver.completed && driver.status == DriverStatus.WaitingDocuments;
}

