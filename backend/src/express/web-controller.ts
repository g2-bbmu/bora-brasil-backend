import ExpressController from "./interfaces/express.controller.interface";
import * as express from "express";
import { Service } from "../entities/service";
import { Media } from "../entities/media";
import { Driver, DriverStatus } from "../entities/driver";
import { Car } from "../entities/car";
import { BankAccount } from "../entities/bank-account";
import { getConnection } from "typeorm";
import { celebrate, Joi, Segments } from "celebrate";
import { Usuario, Status } from "../entities/usuario";
import {
  PreMotorista,
  Status as StatusPreMotorista,
} from "../entities/pre-motorista";
const multer = require("multer");
import MaskData from "maskdata";
import moment from "moment";
import { sendEmail } from "../libs/mail";
import { DriverService } from "../services/driver-service";
import { RedeService } from "../services/rede-service";
import * as jwt from "jsonwebtoken";
import Container from "typedi";
import { recuperarSenha, posCadastro } from "../libs/mail/emails";
import { findDriverByCpf } from "../services/findDriverByCpf";
import { findPreMotoristaByCpf, findPreMotoristaByEmail } from "../services/findPreMotoristaByCpf";
import { Authorize } from './middlewares/authorize.middleware';
import { ClientType } from '../models/client-jwt-decoded';
import {
    validateDriverExistsInAllTables,
    ValidateDriverExistsInAllTablesResponse
} from './helpers/validate-driver-exists-in-all-tables';
import { cpfAlreadyExistsResponseError, emailAlreadyExistsResponseError, telefoneAlreadyExistsResponseError } from './helpers/response-error-builders';
import { findPreMotoristaByContatoMovel } from './repositories/pre-motorista';
import { findDriverByEmail, findDriverByMobileNumber } from './repositories/driver';

export enum ValidateDriverExistsToCompleteRegistrationResponse {
  EMAIL_EXISTS = 'EMAIL_EXISTS',
  TELEFONE_EXISTS = 'TELEFONE_EXISTS',
  CPF_EXISTS = 'CPF_EXISTS'
}


const storage = multer.diskStorage({
  destination(
    req: any,
    file: Express.Multer.File,
    callback: (error: Error | null, destination: string) => void
  ) {
    const address = `${process.cwd()}/public/img/${req.headers.type}`;
    callback(undefined, address);
  },
  filename(req, file, cb) {
    cb(undefined, `${Date.now()}.${file.originalname.split(".").pop()}`);
  },
});

const upload = multer({ storage });

export default class WebController implements ExpressController {
  path: string = "/web";
  router: express.Router;

  constructor() {
    this.router = express.Router({});
    this.router.get("/service", this.getServices);
    this.router.post("/upload", upload.single("document"), this.upload);
    this.router.get(
      "/usuario/:codIndicacao/nome/",
      this.getNomePorCodIndicacao
    );
    this.router.post("/driver", this.register.bind(this));
    this.router.post("/usuario/cadastro_completo", Authorize([ClientType.Web]), this.cadastroCompleto.bind(this));

    this.router.get(
      "/usuario/cod_indicacao",
      celebrate({
        [Segments.QUERY]: {
          email: Joi.string().allow(null, ""),
          telefone: Joi.string().allow(null, ""),
          cpf: Joi.string().allow(null, ""),
        },
      }),
      this.getCodIndicacao
    );

    this.router.post(
      "/usuario/esqueci_senha",
      celebrate({
        [Segments.BODY]: {
          email: Joi.string().required(),
        },
      }),
      this.esqueciSenha
    );

    this.router.post(
      "/usuario/token_criar_senha",
      celebrate({
        [Segments.BODY]: {
          codIndicacao: Joi.string().required(),
        },
      }),
      this.criarSenhaToken
    );

    this.router.post(
      "/usuario/redefinicao_senha",
      celebrate({
        [Segments.BODY]: {
          email: Joi.string().required(),
          token: Joi.string().required(),
          senha: Joi.string().required(),
          confirmarSenha: Joi.string().required(),
        },
      }),
      this.redefinirSenha
    );

    this.router.post(
      "/usuario/info_pre_cadastro",
      celebrate({
        [Segments.BODY]: {
          email: Joi.string().required(),
          senha: Joi.string().required(),
        },
      }),
      this.getPreCadastro
    );

    this.router.post(
      "/usuario/criar_senha",
      celebrate({
        [Segments.BODY]: {
          email: Joi.string().required(),
          token: Joi.string().required(),
          senha: Joi.string().required(),
          confirmarSenha: Joi.string().required(),
        },
      }),
      this.criarSenha
    );

    this.router.post(
      "/usuario/login",
      celebrate({
        [Segments.BODY]: {
          email: Joi.string().required(),
          senha: Joi.string().required(),
        },
      }),
      this.login
    );
  }

  async getServices(req: express.Request, res: express.Response) {
    res.json(await Service.find());
  }

  async upload(req: express.Request, res: express.Response) {
    if (!req.headers.type) {
      res.status(400).send("Header type is required");
    }

    const address = `img/${req.headers.type}/${req.file.filename}`;
    let media: Media = null;

    media = await Media.save({
      address,
      type: req.headers.type,
    } as Media);

    console.log(media);

    res.json(media);
  }

  async getNomePorCodIndicacao(req: express.Request, res: express.Response) {
    try {
      const { codIndicacao } = req.params;

      const usuario = await Usuario.findOne({
        where: {
          codIndicacao: codIndicacao,
        },
      });

      if (!usuario) return res.status(400).send();

      return res.json({
        nomeCompleto: usuario.nomeCompleto,
      });
    } catch (err) {
      return res.status(500).send();
    }
  }

  async cadastroCompleto(req: express.Request & { user: any }, res: express.Response) {
    const usuario = await Usuario.findOne(req.user.id);

    if (!usuario) {
      return res.sendStatus(401);
    }

    const driver: Driver = Driver.create(req.body.driver as Driver);
    delete driver.id;
    driver.email = usuario.login;

    const response = await validateDriverExistsToCompleteRegistration(driver);

    if (response === ValidateDriverExistsToCompleteRegistrationResponse.EMAIL_EXISTS) {
      return emailAlreadyExistsResponseError(res);
    }

    if (response === ValidateDriverExistsToCompleteRegistrationResponse.TELEFONE_EXISTS) {
      return telefoneAlreadyExistsResponseError(res);
    }

    if (response === ValidateDriverExistsToCompleteRegistrationResponse.CPF_EXISTS) {
      return cpfAlreadyExistsResponseError(res);
    }

    const nomeCompleto = makeDriverFullnameWithFirstAndLastName(driver);

    if (await userIsNotRegisteredInPreMotorista(driver)) {
      const preMotorista = PreMotorista.create({
        nomeCompleto,
        contatoMovel: String(driver.mobileNumber),
        email: driver.email,
        cpf: driver.cpf,
        dataAtivacao: new Date(),
        dataCadastro: new Date(),
        status: StatusPreMotorista.Pendente,
        idTblUsuario: usuario.id
      });

      await PreMotorista.save(preMotorista);
    }

    const carId = req.body?.driver?.carId;
    if (carId) driver.car = await Car.findOne(carId);

    let bankAccount: BankAccount = BankAccount.create(
      req.body.bankAccount as BankAccount
    );
    delete bankAccount?.id;

    driver.completed = true;
    driver.bankAccount = bankAccount;
    driver.status = DriverStatus.PendingApproval;
    driver.cnhCategory = req.body.driver.cnhCategory;
    driver.mediaId = req.body.driver.driverMediaId

    await BankAccount.save(driver.bankAccount);
    await Driver.save(driver);

    usuario.idDriver = driver.id;
    
    await Usuario.save(usuario);

    await this.saveDriverDocuments(req, driver);

    return res.status(200).json({ codIndicacao: usuario.codIndicacao });
  }

  private async saveDriverDocuments(req: express.Request, driver: Driver) {
    const {
      carMediaId,
      carLocationContractMediaId,
      cnhMediaId,
      selfieMediaId,
      carDocumentMediaId,
      proofOfAddressMediaId,
    } = req.body.driver;

    const documents = [
      carMediaId,
      carLocationContractMediaId,
      cnhMediaId,
      selfieMediaId,
      carDocumentMediaId,
      proofOfAddressMediaId,
    ].filter(a => !!a);

    await getConnection().createQueryBuilder().relation(Driver, 'documents').of(driver).add(documents);
  }

  async register(req: express.Request, res: express.Response) {
    const senha = req.body?.usuario?.senha;
    const confirmarSenha = req.body?.usuario?.confirmarSenha;

    if (!senha) {
      return res.status(400).json({ message: "Digite uma senha" });
    }

    if (senha !== confirmarSenha) {
      return res.status(400).json({ message: "As senhas não se coincidem" });
    }

    const codIndicacao: string = req.body.invitationCodeReceived;

    if (!codIndicacao) {
      return res
        .status(400)
        .json({ message: "Não é possível realizar um cadastro sem indicação" });
    }

    const driver: Driver = Driver.create(req.body.driver as Driver);
    delete driver.id;

    const response = await validateDriverExistsInAllTables(driver);

    if (response === ValidateDriverExistsInAllTablesResponse.CPF_EXISTS) {
      return cpfAlreadyExistsResponseError(res);
    }

    if (response === ValidateDriverExistsInAllTablesResponse.EMAIL_EXISTS) {
      return emailAlreadyExistsResponseError(res);
    }

    if (response === ValidateDriverExistsInAllTablesResponse.TELEFONE_EXISTS) {
      return telefoneAlreadyExistsResponseError(res);
    }

    const carId = req.body?.driver?.carId;
    if (carId) driver.car = await Car.findOne(carId);

    let bankAccount: BankAccount = BankAccount.create(
      req.body.bankAccount as BankAccount
    );
    delete bankAccount?.id;

    driver.completed = true;
    driver.bankAccount = bankAccount;
    driver.status = DriverStatus.PendingApproval;
    if (req.body.cnhCategory) {
      driver.cnhCategory = req.body.cnhCategory;
    } else {
      driver.cnhCategory = req.body.driver.cnhCategory;
    }

    await BankAccount.save(driver.bankAccount);
    await Driver.save(driver);

    const [usuario] = await DriverService.criarUsuarioEPreMotorista(driver);

    await RedeService.cadastrarUsuarioNaRede(usuario.id, codIndicacao);

    const html = posCadastro(
      `${process.env.WEB_REGISTRATION_URL}cadastro-motorista?hash=${usuario.codIndicacao}`,
      usuario.login,
      senha
    );

    await this.saveDriverDocuments(req, driver);

    sendEmail(usuario.login, "Bem vindo", html, null);

    return res.status(200).json({ codIndicacao: usuario.codIndicacao });
  }

  async getCodIndicacao(req: express.Request, res: express.Response) {
    const { email, telefone, cpf } = req.query;

    let usuario: Usuario = null;

    if (email) {
      usuario = await Usuario.createQueryBuilder()
        .where("Usuario.login = :login", { login: email })
        .andWhere(
          "Usuario.status != 'Bloqueado' and Usuario.status != 'Cancelado'"
        )
        .getOne();
    }

    if (!usuario && telefone) {
      const preMotorista = await PreMotorista.createQueryBuilder()
        .where("PreMotorista.contatoMovel = :telefone", { telefone: telefone })
        .andWhere(
          "PreMotorista.status != 'Bloqueado' and PreMotorista.status != 'Cancelado'"
        )
        .getOne();

      if (preMotorista) {
        usuario = await Usuario.createQueryBuilder()
          .where("Usuario.login = :email", { email: preMotorista.email })
          .andWhere(
            "Usuario.status != 'Bloqueado' and Usuario.status != 'Cancelado'"
          )
          .getOne();
      }
    }

    if (!usuario && cpf) {
      const preMotorista = await PreMotorista.createQueryBuilder()
        .where("PreMotorista.cpf = :cpf", { cpf: cpf })
        .andWhere(
          "PreMotorista.status != 'Bloqueado' and PreMotorista.status != 'Cancelado'"
        )
        .getOne();

      if (preMotorista) {
        usuario = await Usuario.createQueryBuilder()
          .where("login = :email", { email: preMotorista.email })
          .andWhere(
            "Usuario.status != 'Bloqueado' and Usuario.status != 'Cancelado'"
          )
          .getOne();
      }
    }

    if (!usuario) {
      return res.sendStatus(404);
    }

    const preMotorista = await PreMotorista.createQueryBuilder()
      .where("email = :email", { email: usuario.login })
      .andWhere(
        "PreMotorista.status != 'Bloqueado' and PreMotorista.status != 'Cancelado'"
      )
      .getOne();

    const maskedEmail = MaskData.maskEmail2(usuario.login);

    const maskPhoneOptions = {
      maskWith: "*",
      unmaskedStartDigits: 5,
      unmaskedEndDigits: 2,
    };

    const maskedTelefone = preMotorista?.contatoMovel
      ? MaskData.maskPhone(preMotorista.contatoMovel, maskPhoneOptions)
      : null;
    const maskedCpf = preMotorista?.cpf
      ? preMotorista.cpf.slice(0, 3) + "*".repeat(8)
      : null;

    const driver = await Driver.findOne({
        where: {
          cpf: preMotorista.cpf,
          email: usuario.login
        }
    })

    const response = {
      nome: usuario.nomeCompleto?.split(" ")?.[0] || undefined,
      codIndicacao: usuario.codIndicacao,
      telefone: maskedTelefone,
      email: maskedEmail,
      cpf: maskedCpf,
      senhaCriada: usuario.senhaCriada,
      driverCadastrado: !!driver,
    };

    return res.status(200).json(response);
  }

  async esqueciSenha(req: express.Request, res: express.Response) {
    const { email } = req.body;

    const usuario = await Usuario.findOne({
      where: {
        login: email,
      },
    });

    if (usuario) {
      const expireDate = moment(usuario.dataExpiracaoTokenSenha);

      if (
        !expireDate.isValid() ||
        !expireDate.isAfter(moment().add(59, "minutes").add(30, "seconds"))
      ) {
        usuario.gerarTokenRecuperacaoDeSenha();
        await Usuario.save(usuario);

        const html = recuperarSenha(
          `${process.env.HOST_RESET_PASSWORD}/?email=${usuario.login}&token=${usuario.tokenRecuperacaoSenha}`
        );
        sendEmail(usuario.login, "Recuperação de senha", html, null);
      }
    }

    return res.sendStatus(200);
  }

  async criarSenhaToken(req: express.Request, res: express.Response) {
    const { codIndicacao } = req.body;

    const usuario = await Usuario.findOne({
      where: {
        codIndicacao: codIndicacao,
      },
    });

    if (usuario && !usuario.senhaCriada) {
      const expireDate = moment(usuario.dataExpiracaoTokenSenha);

      if (
        !expireDate.isValid() ||
        !expireDate.isAfter(moment().add(59, "minutes").add(30, "seconds"))
      ) {
        usuario.gerarTokenRecuperacaoDeSenha();
        await Usuario.save(usuario);

        const html = recuperarSenha(
          `${process.env.HOST_CREATE_PASSWORD}/?email=${usuario.login}&token=${usuario.tokenRecuperacaoSenha}`
        );
        sendEmail(usuario.login, "Criar senha", html, null);
      }
    }

    return res.sendStatus(200);
  }

  async criarSenha(req: express.Request, res: express.Response) {
    const { email, senha, confirmarSenha, token } = req.body;

    if (senha !== confirmarSenha) {
      return res.status(400).json({ message: "As senhas não se coincidem" });
    }

    const usuario = await Usuario.findOne({
      where: {
        tokenRecuperacaoSenha: token,
        login: email,
      },
    });

    if (!usuario) {
      return res.status(400).json({ message: "Token inválido" });
    }

    if (usuario.senhaCriada) {
      return res
        .status(400)
        .json({ message: "Esse usuário já possui uma senha" });
    }

    usuario.senha = senha;
    usuario.senhaCriada = true;

    const preMotorista = await PreMotorista.findOne({
      where: { email: usuario.login },
      relations: ["usuarioIndicador"],
    });

    const tokenJWT = jwt.sign(
      { id: usuario.id, t: "web" },
      Container.get("token"),
      { expiresIn: "1h" }
    );

    const response = {
      indicadoPor: preMotorista?.usuarioIndicador?.nomeCompleto,
      nomeCompleto: usuario.nomeCompleto,
      codIndicacao: usuario.codIndicacao,
      telefone: preMotorista?.contatoMovel,
      email: usuario.login,
      cpf: preMotorista?.cpf,
      token: tokenJWT,
    };

    await Usuario.save(usuario);

    const html = posCadastro(
      `${process.env.WEB_REGISTRATION_URL}cadastro-motorista?hash=${usuario.codIndicacao}`,
      usuario.login,
      senha
    );

    sendEmail(usuario.login, "Bem vindo", html, null);
    return res.status(200).json(response);
  }

  async redefinirSenha(req: express.Request, res: express.Response) {
    const { email, senha, confirmarSenha, token } = req.body;

    if (senha !== confirmarSenha) {
      return res.status(400).json({ message: "As senhas não se coincidem" });
    }

    const usuario = await Usuario.findOne({
      where: {
        tokenRecuperacaoSenha: token,
        login: email,
      },
    });

    if (!usuario) {
      return res.status(400).json({ message: "Token inválido" });
    }

    usuario.senha = senha;

    await Usuario.save(usuario);

    return res.sendStatus(200);
  }

  async getPreCadastro(req: express.Request, res: express.Response) {
    const { email, senha } = req.body;

    const usuario = await Usuario.findOne({
      where: {
        login: email,
      },
    });

    const preMotorista = await PreMotorista.findOne({
      where: {
        email: email,
      },
    });

    if (!usuario || !usuario.validarSenha(senha)) {
      return res.status(400).json({ message: "Credenciais inválidas" });
    }

    if (usuario.status !== Status.PreCadastro) {
      return res.status(400).json({
        message:
          "O usuário deve estar em status de pré-cadastro para que a operação seja feita",
      });
    }

    const response = {
      nome: usuario.nomeCompleto,
      email: usuario.login,
      codIndicacao: usuario.codIndicacao,
      telefone: preMotorista?.contatoMovel,
      cpf: preMotorista?.cpf,
    };

    return res.status(200).json(response);
  }

  async login(req: express.Request, res: express.Response) {
    const { email, senha } = req.body;

    const usuario = await Usuario.findOne({ where: { login: email } });

    if (!usuario || !usuario.validarSenha(senha)) {
      return res.status(400).json({ message: "Credenciais inválidas" });
    }

    let driver: Driver = null;

    const preMotorista = await PreMotorista.findOne({ where: { email: email }})

    if (preMotorista) {
      driver = await Driver.findOne({
        where: {
          cpf: preMotorista.cpf,
          email: usuario.login
        }
      })
    }

    const token = jwt.sign(
      { id: usuario.id, t: "web" },
      Container.get("token"),
      { expiresIn: "1h" },
    );

    const response = {
      indicadoPor: preMotorista?.usuarioIndicador?.nomeCompleto,
      nomeCompleto: usuario.nomeCompleto,
      codIndicacao: usuario.codIndicacao,
      telefone: preMotorista?.contatoMovel,
      email: usuario.login,
      cpf: preMotorista?.cpf,
      token: token,
      usuario: { id: usuario.id, t: "web" },
      driverCadastrado: !!driver
    };

    return res.status(200).json(response);
  }
}
async function userIsNotRegisteredInPreMotorista(driver: Driver) {
  return !(await findPreMotoristaByEmail(driver.email));
}

function makeDriverFullnameWithFirstAndLastName(driver: Driver) {
  return [driver.firstName, driver.lastName].filter((a) => !!a)
    .join(" ");
}

async function validateDriverExistsToCompleteRegistration(driver: Driver) {
  const [
    preMotoristaEmail,
    preMotoristaCpf,
    preMotoristaTelefone,
    driverEmail,
    driverCpf,
    driverTelefone
  ] = await Promise.all([
    findPreMotoristaByEmail(driver.email),
    findPreMotoristaByCpf(driver.cpf),
    findPreMotoristaByContatoMovel(String(driver.mobileNumber)),
    findDriverByEmail(driver.email),
    findDriverByCpf(driver.cpf),
    findDriverByMobileNumber(driver.mobileNumber)
  ]);


  if (!preMotoristaEmail) {
    if (driverEmail) {
      return 'EMAIL_EXISTS';
    }
    
    if (driverTelefone || preMotoristaTelefone) {
      return 'TELEFONE_EXISTS';
    }

    if (driverCpf || preMotoristaCpf) {
      return `CPF_EXISTS`
    }
    
  } else {
    
    if (driverEmail) {
      return 'EMAIL_EXISTS';
    }

    if (driverTelefone || (preMotoristaTelefone && preMotoristaTelefone.id !== preMotoristaEmail.id)) {
      return 'TELEFONE_EXISTS';
    }

    if (driverCpf || (preMotoristaCpf && preMotoristaCpf.id !== preMotoristaEmail.id)) {
      return `CPF_EXISTS`
    }

  }
}