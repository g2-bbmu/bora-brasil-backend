export const getIPV4 = (data: string): string => {
    const separateByColon = data.split(':');

    const ipv4 = separateByColon[separateByColon.length - 1];

    return ipv4;
}
