import { Entity,  Column, PrimaryColumn } from 'typeorm';
import {BaseEntity} from "typeorm/index";

@Entity()
export class PowerBI extends BaseEntity {
  @PrimaryColumn({ length: 100 })
  id: string;

  @Column()
  url: string;

  @Column({ length: 1000, nullable: true })
  urlImagem: string;

  @Column()
  nome: string;

  @Column({ length: 1000, nullable: true })
  descricao: string;

  @Column()
  ativo: boolean;

  getReportId(): string {
    if (this.url) {
      return this.url.split('/').slice(-1)[0];
    } else {
      return null;
    }
  }
}
