import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Media} from "./media";
import {Driver} from "./driver";

@Entity()
export class Bank extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { nullable: true })
    value?: string;

    @Column("varchar", { nullable: true })
    label?: string;
}
