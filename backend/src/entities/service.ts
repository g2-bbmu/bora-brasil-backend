import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { ServiceCategory } from "./service-category";
import { Request } from "./request";
import { Region, DistanceFee } from "./region";
import { BooleanTransformer } from "../models/transformers";
import { Media } from "./media";
import { Coupon } from "./coupon";

export enum PaymentTime {
  PrePay = "PrePay",
  PostPay = "PostPay",
}

export enum QuantityMode {
  Singular = "Singular",
  Multiple = "Multiple",
}

@Entity()
export class Service extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    () => ServiceCategory,
    (serviceCategory: ServiceCategory) => serviceCategory.services,
    { onDelete: "CASCADE" }
  )
  category?: ServiceCategory;

  @Column("varchar", {
    nullable: true,
  })
  title?: string;

  @ManyToOne(() => Media, (media: Media) => media.services, {
    onDelete: "SET NULL",
    onUpdate: "CASCADE",
  })
  media?: Media;

  @ManyToMany(() => Coupon, (coupon) => coupon.services)
  coupons?: Coupon[];

  @Column("time", {
    default: "00:00",
  })
  availableTimeFrom: string;

  @Column("time", {
    default: "23:59",
  })
  availableTimeTo: string;

  @Column("tinyint", {
    width: 1,
    default: false,
    transformer: new BooleanTransformer(),
  })
  canEnableVerificationCode: boolean;

  @Column("enum", {
    enum: PaymentTime,
    default: PaymentTime.PostPay,
  })
  paymentTime: PaymentTime;

  @Column("tinyint", {
    default: 0,
  })
  prePayPercent: number;

  @Column("float", {
    nullable: true,
    default: 0,
  })
  priceEstimate: number;

  @Column("enum", {
    enum: QuantityMode,
    default: QuantityMode.Singular,
  })
  quantityMode: QuantityMode;

  @Column("float", {
    default: "1.00",
    precision: 10,
    scale: 2,
  })
  multiplicationFactor: number;

  @Column("tinyint", {
    nullable: false,
    default: 0,
  })
  maxQuantity: number;

  @ManyToMany(() => Region, (region) => region.services)
  @JoinTable()
  regions: Region[];

  @OneToMany(() => Request, (request: Request) => request.service)
  requests: Request[];
}
