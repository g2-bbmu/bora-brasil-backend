import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn, ManyToMany, ManyToOne } from "typeorm";
import CoordinateXY from "../models/coordinatexy";
import { Service } from "./service";
import {Request} from './request'
import { ColumnIntTransformer, ColumnFloatTransformer } from "../models/transformers";
import { LatLng, LatLngLiteral } from "@google/maps";


export enum Category {
    Gold = 'Gold',
    Executive = 'Executive',
    Premium = 'Premium'
}

export enum DistanceFee {
    None = 'None',
    PickupToDestination = 'PickupToDestination'
}

export enum PaymentMethod {
    CashCredit = 'CashCredit',
    OnlyCredit = 'OnlyCredit',
    OnlyCash = 'OnlyCash'
}

export enum SearchRadius {
    Five = 5000,
    Ten = 10000,
}

export enum FeeEstimationMode {
    Static = 'Static',
    Dynamic = 'Dynamic',
    Ranged = 'Ranged',
    RangedStrict = 'RangedStrict',
    Disabled = 'Disabled'
}

export enum BookingMode {
    OnlyNow = "OnlyNow",
    Time = "Time",
    DateTime = "DateTime",
    DateTimeAbosoluteHour = "DateTimeAbosoluteHour"
}

@Entity()
export class Region extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", {
        nullable: true
    })
    name: string;

    @Column('varchar')
    currency: string;

    @Column("tinyint", {
        default: 0
    })
    rangePlusPercent: number;


    @Column("tinyint", {
        default: 0
    })
    rangeMinusPercent: number;

    @Column("tinyint", {
        width: 1,
        default: 1
    })
    enabled: boolean;

    @Column('int', {
        default: 0,
        transformer: new ColumnIntTransformer()
    })
    maxDestinationDistance: number;

    @Column('enum', {
        enum: BookingMode,
        default: BookingMode.DateTime
    })
    bookingMode: BookingMode;


    @Column("polygon", {
        transformer: {
            to(value: LatLngLiteral[][]): string {
                if(value == null) return null;
                let str = value.map((x: LatLngLiteral[]) => {
                    let ar = x.map((y: LatLngLiteral) => `${y.lng} ${y.lat}`);
                    return ar.join(',');
                }).join('),(');
                return `POLYGON((${str}))`;
            },
            from(value: string): LatLng[][] {
                return value.substring(8, value.length - 1).split('),(').map(x => {
                    let res = x.substring(1, x.length - 1).split(',').map(y => {
                        let s = y.split(' ');
                        return {
                            lng: parseFloat(s[0]),
                            lat: parseFloat(s[1])
                        }
                    });
                    return res;
                });
            }
        }
    })
    location: CoordinateXY[][];

    @ManyToMany(() => Service, service => service.regions)
    services:Service[];

    @Column({nullable:true, type:'real'}) 
    baseFare: number;

    @Column({nullable:true, type:'real'})
    perHundredMeters: number;

    @Column("float", {
        default: '0.00',
        precision: 12,
        scale: 2,
        transformer: new ColumnFloatTransformer()
    })
    perMinuteWait: number;

    @Column("enum", {
        enum: FeeEstimationMode,
        default: FeeEstimationMode.Static
    })
    feeEstimationMode: FeeEstimationMode;

    @Column("float", {
        default: '0.00',
        precision: 12,
        scale: 2,
        transformer: new ColumnFloatTransformer()
    })
    perMinuteDrive?: number;

    @ManyToOne(() => Request, (request: Request) => request.region)
    requests: Request[];

    @Column({nullable:true, type:'real'})
    minimumFee: number;

    @Column("float", {
        default: '0.00',
        precision: 10,
        scale: 2,
        transformer: new ColumnFloatTransformer()
    })
    eachQuantityFee: number;

    @Column({
        type: 'enum',
        enum: PaymentMethod,
        default: PaymentMethod.CashCredit
    })
    paymentMethod: PaymentMethod;

    @Column('int', {
        default: 10000,
        transformer: new ColumnIntTransformer()
    })
    searchRadius: number;

    @Column('enum', {
        enum: DistanceFee,
        default: DistanceFee.PickupToDestination
    })
    distanceFeeMode: DistanceFee;

    @Column("tinyint", {
        default: 0
    })
    providerSharePercent: number;

    @Column({
        type: 'float',
        default: '0.00',
        precision: 10,
        scale: 2
    })
    providerShareFlat: number;

    @Column("tinyint", {
        nullable: true
    })
    waitingPeriodMinutes: number;





}
