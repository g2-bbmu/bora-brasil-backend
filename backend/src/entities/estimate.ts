import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Api_preco_estimado_uber extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;
  
  @Column('varchar')
  startLat: string;

  @Column('varchar')
  startLong: string;

  @Column('varchar')
  endLat: string;

  @Column('varchar')
  endLong: string;

  @Column('varchar')
  id_produto_uber: string;

  @Column('varchar')
  produto_uber: string;

  @Column('decimal')
  maior_preco: string;

  @Column('decimal')
  menor_preco: string;

  @Column('varchar')
  distancia: string;

  @Column('varchar')
  bairro: string;

  @Column('varchar')
  duracao: string;

  @Column('int')
  id_categoria_bora: String;

  @Column('varchar')
  categoria_bora: String;

  @Column('varchar')
  cidade: string;

  @Column('varchar')
  uf: string;

  @Column('varchar')
  porcentagem_aplicada: string;

  @Column('varchar')
  valor_calculado: string;

  @Column('datetime')
  data_consulta: string;

  @Column('int')
  idRider: string
}