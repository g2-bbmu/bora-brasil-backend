import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";

import { Request } from "./request";

@Entity("coordinates_request")
export class CoordinatesRequest extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "decimal",
    precision: 9,
    nullable: true,
    scale: 6,
  })
  coordX?: number;

  @Column({
    type: "decimal",
    precision: 8,
    nullable: true,
    scale: 6,
  })
  coordY?: number;

  @ManyToOne((_) => Request)
  @JoinColumn({ name: "request_id" })
  request: Request;

  @Column()
  request_id: number;
}
