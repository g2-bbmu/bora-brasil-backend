import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { Driver } from "./driver";
import { Operator } from "./operator";

export enum OccurrenceStatus {
  FalseAlarm = "False Alarm",
  SystemTest = "System Test",
  RealOccurrence = "Real Occurrence",
  Unknown = "Unknown",
}

export interface IOccurrence {
  id: number;
  status: OccurrenceStatus;
  calledLocalAuthority: boolean;
  details: string;
}

@Entity("occurrences")
class Occurrence extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("enum", {
    default: OccurrenceStatus.Unknown,
    enum: OccurrenceStatus,
  })
  status: string;

  @Column({
    nullable: true,
  })
  calledLocalAuthority?: boolean;

  @Column({
    nullable: true,
  })
  details?: string;

  @CreateDateColumn()
  startDate: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column({ nullable: true })
  acceptedAt: Date;

  @Column({ nullable: true })
  finishedAt: Date;

  @ManyToOne((_) => Driver)
  @JoinColumn({ name: "driver_id" })
  driver: Driver;

  @ManyToOne((_) => Operator, {
    eager: true,
    onUpdate: "CASCADE",
    nullable: true,
  })
  @JoinColumn({ name: "accepted_by" })
  acceptedBy: Operator;

  @ManyToOne((_) => Operator, {
    eager: true,
    onUpdate: "CASCADE",
    nullable: true,
  })
  @JoinColumn({ name: "finished_by" })
  finishedBy: Operator;

  @Column()
  driver_id: number;

  @Column({ nullable: true })
  accepted_by: number;

  @Column({ nullable: true })
  finished_by: number;
}

export default Occurrence;
