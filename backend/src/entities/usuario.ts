 
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, AfterInsert, BeforeInsert, BeforeUpdate, AfterLoad, OneToOne } from 'typeorm';
import { v4 as uuid } from 'uuid';
import moment from 'moment';
import bcrypt from 'bcryptjs';
import {NumericalStringTransformer} from "../models/transformers";

export enum Status {
    Pendente = 'Pendente',
    Cancelado = 'Cancelado',
    Ativo = 'Ativo',
    Bloqueado = 'Bloqueado',
    PreCadastro = 'Pré-Cadastro',
}

export enum Perfil {
    Administrador = 'Administrador',
    Motorista = 'Motorista',
    Financeiro = 'Financeiro',
}

@Entity()
export class Usuario extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nomeCompleto: string;

    @Column({ nullable: true, transformer: new NumericalStringTransformer() })
    cnpj?: string;

    @Column()
    login: string;

    @Column({ type: 'enum', enum: Perfil, nullable: true })
    perfil?: Perfil;

    @Column()
    ativo: boolean;

    @Column()
    senha: string;

    @Column({ type: 'enum', enum: Status, default: Status.Pendente })
    status: Status;

    @Column({ nullable: true, length: 2500 })
    obs?: string;

    @Column({ default: false })
    numeroAcesso: boolean;

    @Column({ default: false })
    acesso: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    dataAtivacao: Date;

    @Column({ length: 200, nullable: true })
    codIndicacao?: string;

    @Column({ nullable: true })
    dataExpiracaoTokenSenha?: Date;

    @Column({ nullable: true, unique: true, length: 36 })
    tokenRecuperacaoSenha?: string;

    @Column({ default: false })
    senhaCriada: boolean;

    tempPassword: string;

    @Column({ nullable: true })
    idDriver?: number;

    public gerarCodigoIndicacao() {
        if (this.codIndicacao) return;
        if (!this.id) return;

        let code = '';
        let first = null;
        let last = null;

        const split = this.nomeCompleto.split(' ');

        if (split.length >= 2) {
            first = split[0];
            last = split[split.length - 1];
        } else if (split.length === 1) {
            first = split[0];
        }

        if (first) code += first;
        if (last) code += last;

        code += String(this.id);

        this.codIndicacao = code;
    }

    public gerarTokenRecuperacaoDeSenha() {
        this.tokenRecuperacaoSenha = uuid();
        this.dataExpiracaoTokenSenha = moment().add(1, 'hour').toDate();
    }

    validarSenha(senha: string): boolean {
        return bcrypt.compareSync(senha, this.senha);
    }

    @BeforeInsert()
    private hashPassword() {
        if (this.senha) {
            this.senha = bcrypt.hashSync(this.senha, 8);
        }
    }

    @BeforeUpdate()
    private encryptPassword(): void {
        if (this.tempPassword !== this.senha) {
            this.senha = bcrypt.hashSync(this.senha, 8);
            this.loadTempPassword();
        }
    }

    @AfterLoad()
    private loadTempPassword(): void {
        this.tempPassword = this.senha;
    }
}