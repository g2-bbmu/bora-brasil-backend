import {Entity, BaseEntity, Column, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Driver} from "./driver";
import {NumericalStringTransformer} from "../models/transformers";

export enum BankAccountType {
    Checking = 'checking',
    Savings = 'savings',
}

@Entity()
export class BankAccount extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("enum", {
       enum: BankAccountType
    })
    type: BankAccountType;

    @Column("varchar")
    holder: string;

    @Column("varchar", {
        transformer: new NumericalStringTransformer()
    })
    cpfCnpj: string;

    @Column("varchar")
    bankCode: string;

    @Column("varchar")
    agencyCode: string;

    @Column("varchar")
    accountNumber: string;

    @OneToOne(() => Driver)
    driver?: Driver;

}