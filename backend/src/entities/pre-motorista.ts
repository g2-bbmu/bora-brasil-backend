import {
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  Column,
  ManyToOne,
  BaseEntity,
} from "typeorm";
import { Usuario } from "./usuario";
import { NumericalStringTransformer } from "../models/transformers";

export enum Status {
  Pendente = "Pendente",
  Cancelado = "Cancelado",
  Ativo = "Ativo",
  Bloqueado = "Bloqueado",
}

@Entity()
export class PreMotorista extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nomeCompleto: string;

  @Column({ nullable: true, transformer: new NumericalStringTransformer() })
  contatoMovel?: string;

  @Column({ nullable: true })
  email: string;

  @Column({ type: "enum", enum: Status, default: Status.Pendente })
  status: Status;

  @Column()
  dataCadastro: Date;

  @Column()
  dataAtivacao: Date;

  @Column({ nullable: true, transformer: new NumericalStringTransformer() })
  cpf?: string;

  @Column({ nullable: true })
  indicador?: number;

  @Column({ nullable: true, transformer: new NumericalStringTransformer() })
  cnpj: string;

  @Column({ nullable: true })
  nivel1?: number;

  @Column({ nullable: true })
  nivel2?: number;

  @Column({ nullable: true })
  nivel3?: number;

  @Column({ nullable: true })
  nivel4?: number;

  @Column({ nullable: true })
  nivel5?: number;

  @ManyToOne(() => Usuario, { nullable: true })
  @JoinColumn({ name: "indicador" })
  usuarioIndicador?: Usuario;

  @ManyToOne(() => Usuario, { nullable: true })
  @JoinColumn({ name: "nivel1" })
  usuarioNivel1?: Usuario;

  @ManyToOne(() => Usuario, { nullable: true })
  @JoinColumn({ name: "nivel2" })
  usuarioNivel2?: Usuario;

  @ManyToOne(() => Usuario, { nullable: true })
  @JoinColumn({ name: "nivel3" })
  usuarioNivel3?: Usuario;

  @ManyToOne(() => Usuario, { nullable: true })
  @JoinColumn({ name: "nivel4" })
  usuarioNivel4?: Usuario;

  @ManyToOne(() => Usuario, { nullable: true })
  @JoinColumn({ name: "nivel5" })
  usuarioNivel5?: Usuario;

  @Column({ name: 'id_tbl_usuario', nullable: true })
  idTblUsuario?: number;
}
