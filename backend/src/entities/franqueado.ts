import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';

export enum Status {
    Pendente = 'Pendente',
    Cancelado = 'Cancelado',
    Ativo = 'Ativo',
    Bloqueado = 'Bloqueado',
}

@Entity()
export class Franqueado extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    razaoSocial: string;

    @Column()
    nomeFantasia: string;

    @Column()
    nomeFranquia: string;

    @Column()
    cnpj: string;

    @Column({ nullable: true })
    inscricaoEstadual?: string;

    @Column({ nullable: true })
    contatoFixo?: string;

    @Column({ nullable: true })
    contatoMovel?: string;

    @Column()
    responsavel: string;

    @Column()
    email: string;

    @Column({ nullable: true })
    cep: string;

    @Column({ nullable: true })
    endereco: string;

    @Column({ nullable: true })
    numero?: string;

    @Column({ nullable: true })
    estado: string;

    @Column({ nullable: true })
    uf: string;

    @Column({ nullable: true })
    cidade: string;

    @Column()
    ativo: boolean;

    @Column()
    dataCadastro: Date;

    @Column({ default: Status.Pendente })
    status: Status;

    @Column({ nullable: true })
    obs?: string;

    @Column({ nullable: true })
    nDomicilio?: string;

    @Column({ nullable: true })
    banco?: string;

    @Column({ nullable: true })
    agencia?: string;

    @Column({ nullable: true })
    digitoAgencia?: string;

    @Column({ nullable: true })
    conta?: string;

    @Column({ nullable: true })
    digitoConta?: string;
}
