import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
  } from "typeorm";


@Entity('log_alteracao_Driver')
export class LogAlteracaoDriver extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idOperator: number;

    @Column()
    idDriver: number;

    @Column()
    data_alteracao: Date;

    @Column()
    tabela: string;

    @Column()
    campo: string;

    @Column()
    valor_anterior: string;

    @Column()
    valor_atual: string;
}
