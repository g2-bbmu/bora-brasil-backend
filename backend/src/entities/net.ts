import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";

@Entity()
export class Net extends BaseEntity {
  @PrimaryGeneratedColumn()
  up: number;

  @Column()
    down: number;

    @Column()
    level: number;

 
}
