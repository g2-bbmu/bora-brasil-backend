import {
  BaseEntity,
  Column,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Entity,
  ManyToMany,
  JoinTable,
  OneToOne,
  JoinColumn,
} from "typeorm";
import { Car } from "./car";
import { Media } from "./media";
import { DriverTransaction } from "./driver-transaction";
import { PaymentRequest } from "./payment-request";
import { Request } from "./request";
import { RequestReview } from "./request-review";
import { Gender } from "../models/enums/enums";
import {
  DateToLongTransformer,
  StringToIntTransformer,
} from "../models/coordinatexy";
import { Service } from "./service";
import { DriverToGateway } from "./driver-to-gateway";
import { DriverWallet } from "./driver-wallet";
import { Fleet } from "./fleet";
import {
  NumericalStringTransformer,
  TimestampTransformer,
} from "../models/transformers";
import { BankAccount } from "./bank-account";

export enum DriverStatus {
  Online = "online",
  Offline = "offline",
  Blocked = "blocked",
  InService = "in service",
  WaitingDocuments = "waiting documents",
  PendingApproval = "pending approval",
  SoftReject = "soft reject",
  HardReject = "hard reject",
  Expired = "expired"
}

export enum PaymentGatewayDocumentsStatus {
  NaoEnviado = "Não Enviado",
  EmAnalise = "Em Análise",
  AprovadoAutomaticamente = "Aprovado Automaticamente",
  AprovadoManualmente = "Aprovado Manualmente",
  Negado = "Negado",
}

export enum CnhCategory {
  A = "Categoria A",
  B = "Categoria B",
  C = "Categoria C",
  D = "Categoria D",
  E = "Categoria E",
  AB = "Categoria AB",
  AC = "Categoria AC",
  AD = "Categoria AD",
  AE = "Categoria AE",
}

export enum CarStatus {
  own = "Own Car",
  rented = "Rented Car",
}

export enum TypeAccount {
  natural = "Natural Person",
  legal = "Legal Person",
}

@Entity()
export class Driver extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", {
    nullable: true,
    transformer: new NumericalStringTransformer(),
  })
  cpf?: string;

  @Column("varchar", {
    nullable: true,
  })
  firstName?: string;

  @Column("varchar", {
    nullable: true,
  })
  lastName?: string;

  @Column("varchar", {
    nullable: true,
  })
  birthDate: string;

  @Column("varchar", {
    nullable: true,
  })
  website?: string;

  @Column("varchar", {
    nullable: true,
  })
  invitationCodeReceived: string;

  @Column("varchar", {
    nullable: true,
  })
  certificateNumber?: string;

  @Column({
    nullable: true,
  })
  cnhEar: boolean;

  @Column("enum", {
    enum: CnhCategory,
  })
  cnhCategory: CnhCategory;

  @Column("date", {
    nullable: true,
    transformer: new DateToLongTransformer(),
  })
  cnhExpirationDate: number;

  @Column("bigint", {
    nullable: true,
    unique: true,
    transformer: new StringToIntTransformer(),
  })
  mobileNumber?: number;

  @Column("varchar", {
    nullable: true,
  })
  email?: string;

  @Column("varchar", {
    nullable: true,
  })
  description: string;

  @Column("enum", {
    nullable: true,
    enum: PaymentGatewayDocumentsStatus,
    default: PaymentGatewayDocumentsStatus.NaoEnviado,
  })
  paymentGatewayDocumentsStatus: PaymentGatewayDocumentsStatus;

  @Column("date", {
    nullable: true,
  })
  paymentGatewayDocumentsPendingDate: Date;

  @Column("date", {
    nullable: true,
  })
  paymentGatewayDocumentsActivatedDate: Date;

  @Column("date", {
    nullable: true,
  })
  paymentGatewayDocumentsEnabledDate: Date;

  @Column("date", {
    nullable: true,
  })
  paymentGatewayDocumentsDeniedDate: Date;

  @OneToMany(() => DriverWallet, (wallet) => wallet.driver)
  wallet: DriverWallet[];

  @Column({ nullable: true })
  carId?: number;

  @ManyToOne(() => Car, (car: Car) => car.drivers, {
    onDelete: "RESTRICT",
    onUpdate: "RESTRICT",
  })
  car?: Car;

  @ManyToOne(() => Fleet, (fleet: Fleet) => fleet.operators)
  fleet?: Fleet;

  @Column("varchar", {
    nullable: true,
  })
  carColor?: string;

  @Column({ default: true })
  carStatus: boolean;

  @Column("int", {
    nullable: true,
  })
  carProductionYear?: number;

  @Column("varchar", {
    nullable: true,
  })
  carPlate?: string;

  @Column("enum", {
    default: DriverStatus.WaitingDocuments,
    enum: DriverStatus,
  })
  status: DriverStatus;

  @Column("smallint", {
    nullable: true,
  })
  rating?: number;

  @Column("smallint", {
    default: 0,
  })
  reviewCount: number;

  @Column({ nullable: true })
  mediaId?: number;

  @ManyToOne(() => Media, (media: Media) => media.drivers2, {
    onDelete: "SET NULL",
    onUpdate: "CASCADE",
  })
  media?: Media;

  @Column("enum", {
    default: Gender.Unknown,
    enum: Gender,
  })
  gender: Gender;

  @Column("timestamp", {
    default: () => "CURRENT_TIMESTAMP",
  })
  registrationTimestamp: Date;

  @Column("timestamp", {
    nullable: true,
    transformer: new TimestampTransformer(),
  })
  lastSeenTimestamp?: number;

  @Column("varchar", {
    nullable: true,
  })
  accountNumber?: string;

  @Column("varchar", {
    nullable: true,
  })
  bankName: string;

  @Column("varchar", {
    nullable: true,
  })
  bankRoutingNumber?: string;

  @Column("varchar", {
    nullable: true,
  })
  bankSwift?: string;

  @Column("varchar", {
    nullable: true,
  })
  cep?: string;

  @Column("varchar", {
    nullable: true,
  })
  address?: string;

  @Column("varchar", {
    nullable: true,
  })
  addressTwo?: string;

  @Column("varchar", {
    nullable: true,
  })
  number?: string;

  @Column("varchar", {
    nullable: true,
  })
  city?: string;

  @Column("varchar", {
    nullable: true,
  })
  bairro?: string;

  @Column("varchar", {
    nullable: true,
  })
  complemento?: string;

  @Column("varchar", {
    nullable: true,
  })
  state?: string;

  @Column("varchar", {
    nullable: true,
  })
  cnpjFranquia?: string;

  @Column("varchar", {
    nullable: true,
  })
  nomeFranquia?: string;

  @Column("tinyint", {
    width: 1,
    default: 0,
  })
  infoChanged: boolean;

  @Column("timestamp", {
    nullable: true,
  })
  acceptTerms: Date;

  @Column("varchar", {
    nullable: true,
  })
  notificationPlayerId?: string;

  @Column("varchar", {
    nullable: true,
  })
  documentsNote?: string;

  @Column({ nullable: true, default: false })
  completed: boolean;

  @OneToOne(() => BankAccount, (bankAccount) => bankAccount.driver, {
    nullable: true,
  })
  @JoinColumn()
  bankAccount?: BankAccount;

  @ManyToMany(() => Media)
  @JoinTable()
  documents: Media[];

  @OneToMany(
    () => DriverTransaction,
    (driver_transaction: DriverTransaction) => driver_transaction.driver,
    {
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
    }
  )
  transactions: DriverTransaction[];

  @OneToMany(
    () => PaymentRequest,
    (payment_request: PaymentRequest) => payment_request.driver,
    { onDelete: "CASCADE", onUpdate: "RESTRICT" }
  )
  paymentRequests: PaymentRequest[];

  @OneToMany(() => Request, (travel: Request) => travel.driver, {
    onDelete: "SET NULL",
    onUpdate: "CASCADE",
  })
  requests: Request[];

  @OneToMany(
    () => RequestReview,
    (requestReview: RequestReview) => requestReview.driver,
    { onDelete: "CASCADE", onUpdate: "RESTRICT" }
  )
  requestReviews: RequestReview[];

  @ManyToMany(() => Service)
  @JoinTable()
  services: Service[];

  @OneToMany(() => DriverToGateway, (driverToGateway) => driverToGateway.driver)
  gatewayIds?: DriverToGateway[];

  async addToWallet(amount: number, cuurrency: string): Promise<DriverWallet> {
    let driver = await Driver.findOne(this.id, { relations: ["wallet"] });
    let wItem: any = driver.wallet.find((x) => x.currency == cuurrency);
    if (wItem == null) {
      wItem = { amount: amount, currency: cuurrency };
      let insert = await DriverWallet.insert({
        amount: amount,
        currency: cuurrency,
        driver: { id: this.id },
      });
      wItem["id"] = insert.raw.insertId;
    } else {
      await DriverWallet.update(wItem.id, { amount: amount + wItem.amount });
      wItem.amount += amount;
    }
    return wItem;
  }

}
