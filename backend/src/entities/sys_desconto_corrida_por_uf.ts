import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, Double } from 'typeorm';

@Entity()
export class Sys_desconto_corrida_por_uf extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;
  
  @Column('float')
  porcentagem: Double;
}