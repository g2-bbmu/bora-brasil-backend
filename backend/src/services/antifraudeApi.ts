import axios from 'axios';

export const antifraudeApi = axios.create({
    headers:{
        Authorization: `Basic ${process.env.TOKEN_KONDUTO}==`,
        'Content-Type': 'application/json'
    },
    baseURL: 'https://api.konduto.com/v1/orders'
});

export const createOrder = async (data: any) => {
    return await antifraudeApi.post('', JSON.stringify(data), {
        headers: {
            'Content-Type': 'application/json',
        }
    });
}