import axios from "axios";
import querystring from "querystring";
import FormData from "form-data";
import * as Sentry from "@sentry/node";
import axiosRetry from "axios-retry";

export enum ZoopWebhookEventTypes {
  SellerCreated = "seller.created",
  SellerEnabled = "seller.enabled",
  SellerActivated = "seller.activated",
  SellerDenied = "seller.denied",
  Ping = "ping",
}

const axiosRetryObj = {
  "axios-retry": { retries: 10 },
};

export const ZoopApi = axios.create({
  headers: {
    Authorization: `Basic ${process.env.TOKEN_ZOOP}==`,
  },
  baseURL: "https://api.zoop.ws/v1/marketplaces/",
});

axiosRetry(ZoopApi, { retries: 0 });

ZoopApi.interceptors.response.use(
  (response) => {
    return response;
  },
  (err) => {
    if (err.response.status >= 400) {
      console.log(err);
      Sentry.captureException(err);
    }
    return Promise.reject(err);
  }
);

export const getSellerByCPFCNPJ = async (
  marketPlaceId: string,
  cpf: string,
  cnpj?: string
): Promise<any | null> => {
  try {
    const query = { taxpayer_id: cpf, ein: cnpj };
    return await ZoopApi.get(
      `${marketPlaceId}/sellers/search?${querystring.stringify(query)}`,
      axiosRetryObj
    );
  } catch (err) {
    return null;
  }
};

export const getSellerAccountBalanceBySellerId = async (
  marketplaceId: string,
  sellerId: string
) => {
  return await ZoopApi.get(
    `/${marketplaceId}/sellers/${sellerId}/balances`,
    axiosRetryObj
  );
};

export const createIndividualSeller = async (
  marketplaceId: string,
  data: any
) => {
  return await ZoopApi.post(
    `/${marketplaceId}/sellers/individuals`,
    JSON.stringify(data),
    { headers: { "Content-Type": "application/json" } }
  );
};

export const updateIndividualSeller = async (
  marketplaceId: string,
  sellerId: string,
  data: any
) => {
  return await ZoopApi.put(
    `/${marketplaceId}/sellers/individuals/${sellerId}`,
    JSON.stringify(data),
    { headers: { "Content-Type": "application/json" } }
  );
};

export const getBuyerByCPF = async (marketPlaceId: string, cpf: string) => {
  try {
    return await ZoopApi.get(
      `${marketPlaceId}/buyers/search?taxpayer_id=${cpf}`,
      axiosRetryObj
    );
  } catch (err) {
    return null;
  }
};

export const createBuyer = async (marketplaceId: string, data: any) => {
  return await ZoopApi.post(`/${marketplaceId}/buyers`, JSON.stringify(data), {
    headers: {
      "Content-type": "application/json",
    },
  });
};

export const getCardById = async (marketPlaceId: string, cardId: string) => {
  try {
    return await ZoopApi.get(
      `/${marketplaceId}/cards/${cardId}`,
      axiosRetryObj
    );
  } catch (err) {
    return null;
  }
};

export const createCardToken = async (
  marketplaceId: string,
  card: {
    holder_name;
    expiration_month;
    expiration_year;
    card_number;
    security_code;
  }
) => {
  const {
    holder_name,
    expiration_month,
    expiration_year,
    card_number,
    security_code,
  } = card;

  return await ZoopApi.post(
    `/${marketplaceId}/cards/tokens`,
    JSON.stringify({
      holder_name,
      expiration_month,
      expiration_year,
      card_number,
      security_code,
    }),
    { headers: { "Content-Type": "application/json" } }
  );
};

export const connectCardWithBuyer = async (
  marketplaceId: string,
  data: { token: string; customer: string }
) => {
  return await ZoopApi.post(`/${marketplaceId}/cards`, JSON.stringify(data), {
    headers: { "Content-Type": "application/json" },
  });
};

export const updateCardById = async (
  marketplaceId: string,
  cardId: string,
  data: any
) => {
  return await ZoopApi.put(`/${marketplaceId}/cards/${cardId}`, data);
};

export const deleteCardById = async (marketplaceId: string, cardId: string) => {
  return await ZoopApi.delete(`/${marketplaceId}/cards/${cardId}`);
};

export const createTransaction = async (marketplaceId: string, data: any) => {
  return await ZoopApi.post(
    `/${marketplaceId}/transactions`,
    JSON.stringify(data),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};

export const reverseTransaction = async (
  marketplaceId: string,
  transactionId: string,
  data: any
) => {
  return await ZoopApi.post(
    `/${marketplaceId}/transactions/${transactionId}/void`,
    JSON.stringify(data),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};

export const createBankAccountToken = async (
  marketplaceId: string,
  data: any
) => {
  return await ZoopApi.post(
    `/${marketplaceId}/bank_accounts/tokens`,
    JSON.stringify(data),
    {
      headers: { "Content-Type": "application/json" },
    }
  );
};

export const connectBankAccountWithCustomer = async (
  marketplaceId: string,
  data: { customer: string; token: string }
) => {
  return await ZoopApi.post(
    `/${marketplaceId}/bank_accounts`,
    JSON.stringify(data),
    {
      headers: { "Content-Type": "application/json" },
    }
  );
};

export const createBankAccountTransfer = async (
  marketplaceId: string,
  bankAccountId: string,
  data: { amount: number; description: string }
) => {
  return await ZoopApi.post(
    `/${marketplaceId}/bank_accounts/${bankAccountId}/transfers`,
    {
      amount: data.amount.toFixed(0),
      description: data.description,
    }
  );
};

export const createDocument = async (
  marketplaceId: string,
  sellerId: string,
  form: FormData
) => {
  return await ZoopApi.post(
    `/${marketplaceId}/sellers/${sellerId}/documents`,
    form,
    {
      headers: {
        "Content-Type": `multipart/form-data; boundary=${form.getBoundary()}`,
      },
    }
  );
};

export const captureTransaction = async (
  marketplaceId: string,
  transactionId: string,
  data: { on_behalf_of: string; amount: number }
) => {
  return await ZoopApi.post(
    `/${marketplaceId}/transactions/${transactionId}/capture`,
    JSON.stringify({
      on_behalf_of: data.on_behalf_of,
      amount: data.amount,
    }),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};

export const marketplaceId = process.env.MARKETPLACE_ID;
