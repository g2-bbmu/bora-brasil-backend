import { Driver } from "../entities/driver";
import { PreMotorista } from '../entities/pre-motorista';

export const findPreMotoristaByCpf = async (cpf: string): Promise<PreMotorista | undefined> => {
    const preMotorista = await PreMotorista.findOne({where: {cpf}})

    return preMotorista;
}

export const findPreMotoristaByEmail = async (email: string): Promise<PreMotorista | undefined> => {
    const preMotorista = await PreMotorista.findOne({where: {email}})

    return preMotorista;
}