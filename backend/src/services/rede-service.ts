import { Usuario } from '../entities/usuario';
import { PreMotorista } from '../entities/pre-motorista';

export class RedeService {
    static async cadastrarUsuarioNaRede(usuarioId: number, codIndicacao: string) {
        const usuarioIndicador = await Usuario.findOne({
            where: {
                codIndicacao: codIndicacao,
            },
        });

        if (!usuarioIndicador) return;

        const usuarioIndicadorPremotorista = await PreMotorista.findOne({
            where: {
                email: usuarioIndicador.login,
            },
        });

        if (!usuarioIndicadorPremotorista) return;

        const usuarioBanco = await Usuario.findOne({
            where: {
                id: usuarioId,
            },
        });

        if (!usuarioBanco) {
            throw new Error(`Usuário não existe: ${usuarioId}`);
        }

        const preMotorista = await PreMotorista.findOne({
            where: {
                email: usuarioBanco.login,
            },
        });

        if (!preMotorista) {
            throw new Error(`Pré-motorista não existe para o usuario: ${usuarioId}`);
        }

        preMotorista.indicador = usuarioIndicador.id;
        preMotorista.nivel1 = usuarioIndicador.id;
        preMotorista.nivel2 = usuarioIndicadorPremotorista.nivel1;
        preMotorista.nivel3 = usuarioIndicadorPremotorista.nivel2;
        preMotorista.nivel4 = usuarioIndicadorPremotorista.nivel3;
        preMotorista.nivel5 = usuarioIndicadorPremotorista.nivel4;

        await PreMotorista.save(preMotorista);
    }
}
