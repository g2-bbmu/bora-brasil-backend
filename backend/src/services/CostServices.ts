import { DistanceFee, Region } from "../entities/region";
import { QuantityMode, Service } from "../entities/service";

export class CostServices {
  static calculateCost(
    region: Region,
    service: Service,
    distance: number,
    duration: number,
    count: number
  ): number {
    let j = region.baseFare;

    if (region.distanceFeeMode == DistanceFee.PickupToDestination) {
      j +=
        (region.perHundredMeters * distance) / 100 +
        region.perMinuteDrive * duration;
    }
    if (service.quantityMode == QuantityMode.Multiple) {
      j += region.eachQuantityFee * count;
    }
    if (j < region.minimumFee) {
      j = region.minimumFee;
    }

    let i = service.multiplicationFactor;

    return i * j;
  }
}
