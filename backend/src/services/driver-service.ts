import { v4 as uuid } from "uuid";

import { Driver } from "../entities/driver";
import { Usuario, Perfil, Status } from "../entities/usuario";
import {
  PreMotorista,
  Status as StatusPreMotorista,
} from "../entities/pre-motorista";

export class DriverService {
  static async criarUsuarioEPreMotorista(
    driver: Driver
  ): Promise<[Usuario, PreMotorista]> {
    const dataAtivacao = new Date();
    const nomeCompleto = makeDriverFullnameWithFirstAndLastName(driver);

    const usuarioDB = await Usuario.findOne({ where: { login: driver.email } });

    const preMotoristaDB = await PreMotorista.findOne({
      where: { email: driver.email },
    });

    let usuario: Usuario = null;
    let preMotorista: PreMotorista = null;

    if (!usuarioDB) {
      usuario = Usuario.create({
        login: driver.email,
        senha: uuid(),
        nomeCompleto,
        ativo: true,
        dataAtivacao: dataAtivacao,
        perfil: Perfil.Motorista,
        status: Status.Pendente,
        idDriver: driver.id,
      });

      await Usuario.save(usuario);
      
    } else {
      const usuarioUpdate = {
        login: driver.email,
        senha: usuarioDB.senha,
        nomeCompleto: nomeCompleto,
        ativo: usuarioDB.ativo,
        dataAtivacao: usuarioDB.dataAtivacao,
        perfil: usuarioDB.perfil,
        status: usuarioDB.status,
      };

      await Usuario.update(usuarioDB.id, usuarioUpdate);
      const usuarioDBUpdated = await Usuario.findOne({
        where: { login: driver.email },
      });

      usuario = usuarioDBUpdated;
    }

    if (!preMotoristaDB) {
      preMotorista = PreMotorista.create({
        nomeCompleto,
        contatoMovel: String(driver.mobileNumber),
        email: driver.email,
        cpf: driver.cpf,
        dataAtivacao: dataAtivacao,
        dataCadastro: dataAtivacao,
        status: StatusPreMotorista.Pendente,
        idTblUsuario: usuario.id
      });

      await PreMotorista.save(preMotorista);
    } else {
      const preMotoristaUpdate = {
        contatoMovel: preMotoristaDB.contatoMovel,
        email: driver.email,
        cpf: preMotoristaDB.cpf,
        dataAtivacao: preMotoristaDB.dataAtivacao,
        dataCadastro: preMotoristaDB.dataAtivacao,
        status: preMotoristaDB.status,
      };

      await PreMotorista.update(usuarioDB.id, preMotoristaUpdate);

      const preMotoristaDBUpdated = await PreMotorista.findOne({
        where: { email: driver.email },
      });

      preMotorista = preMotoristaDBUpdated;
    }

    return [usuario, preMotorista];
  }
}

function makeDriverFullnameWithFirstAndLastName(driver: Driver) {
  return [driver.firstName, driver.lastName]
    .filter((a) => !!a)
    .join(" ");
}

