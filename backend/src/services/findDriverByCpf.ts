import { Driver } from "../entities/driver";

export const findDriverByCpf = async (cpf: string): Promise<Driver | undefined> => {
    const driver = await Driver.findOne({where: {cpf}})

    return driver;
}