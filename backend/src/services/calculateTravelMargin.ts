export class CalculateTravelMargin {
  static margin(
    estimatedTimeInMinutes: number,
    accomplishedTime: number,
    percent: number,
  ) {

    if (accomplishedTime > estimatedTimeInMinutes + (estimatedTimeInMinutes * percent)) {
      return accomplishedTime;
    } else {
      return Math.round(estimatedTimeInMinutes);
    }

  }
}
