import {Server, Socket} from "socket.io";
import { BraintreeGateway } from 'braintree';
import socketioJwt from 'socketio-jwt';
import SocketMiddlewares from "./middlewares";
import ChatController from "./common/chat-controller";
import RequestCommonsController from "./common/request-commons-controller";
import RequestHistoryController from "./common/request-history-controller";
import WalletController from "./common/wallet-controller";
import ServiceController from "./common/service-controller";
import { UserType } from "../models/enums/enums";
import CouponsController from "./rider/coupons-controller";
import EstimaterCOntroller from "./rider/estimate-controller";
import RiderMiscController from "./rider/rider-misc-controller";
import RiderTripController from "./rider/rider-trip-controller";
import Container from "typedi";
import { ClientSocket } from "../models/client-jwt-decoded";
import Logger from 'socket.io-logger';
import { logSocketRequests } from './helpers/logging';
import Redis from '../libs/redis/redis';
var middleware = require('socketio-wildcard')();

export default class DriverNamespace {
    brain?: BraintreeGateway;
    constructor() {
        let mw = new SocketMiddlewares();
        (Container.get('io') as Server).of('/riders').use(socketioJwt.authorize({
            secret: Container.get('token'),
            handshake: true,
            decodedPropertyName: 'user'
        }))
            .use(mw.validateRider)
            .use((socket: Socket, next: (err?: any) => void) => {
                next();
            })
            .use(middleware)
            .on('connection', async function (socket: ClientSocket) {
                socket.on('disconnect', async () => {
                    await Container.get(Redis).rider.deleteRiderSocketId(socket.user.id);
                })
                socket.on('*', function(data){
                    logSocketRequests(socket.id, data);
                });
                await Container.get(Redis).rider.setRiderSocketId(socket.user.id, socket.id);
                new ChatController(socket, UserType.Rider);
                new RequestCommonsController(socket);
                new RequestHistoryController(socket, UserType.Rider);
                new WalletController(socket);
                new ServiceController(socket, UserType.Rider);
                new CouponsController(socket);
                new EstimaterCOntroller(socket);
                new RiderMiscController(socket);
                new RiderTripController(socket);
            });
    }
}