import SocketController from "../interfaces/socket.controller.interface";
import { UserType } from "../../models/enums/enums";
import { Request, RequestStatus } from "../../entities/request";
import UnknownException from "../exceptions/unknown.exception";
import { RequestChat } from "../../entities/request-chat";
import Container from "typedi";
import Notifier from "../../libs/notifier/notifier";
import { ClientType } from "../../models/client-jwt-decoded";
import RideNotAvailableException from "../exceptions/ride-not-available.exception";
import Redis from '../../libs/redis/redis';

export default class ChatController extends SocketController {
    constructor(socket: any, userType: UserType) {
        super(socket, userType)
        socket.on('SendMessage',this.sendMessage.bind(this))
        socket.on('GetMessages',this.getMessages.bind(this))
    }

    async getMessages (callback) {
        let req: Request;
        if(this.socket.user.t == ClientType.Driver) { 
            req = await Request.findOne({where: { driver: {id: this.socket.user.id}}, order: {id: "DESC"}})
        } else {
            req = await Request.findOne({where: { rider: {id: this.socket.user.id}}, order: {id: "DESC"}})
        }
        if(req == undefined || (req.status !== RequestStatus.DriverAccepted && req.status !== RequestStatus.Arrived)) {
            this.throwException(callback, new RideNotAvailableException())
            return
        }
        let messages = await RequestChat.find({where: {request: {id: req.id}}})
        callback(messages)
    }

    async sendMessage(content, callback) {
        let req: Request;
        if(this.socket.user.t == ClientType.Driver) { 
            req = await Request.findOne({where: { driver: {id: this.socket.user.id}}, order: {id: "DESC"}, relations: ['driver', 'rider']})
        } else {
            req = await Request.findOne({where: { rider: {id: this.socket.user.id}}, order: {id: "DESC"}, relations: ['driver', 'rider']})
        }
        if(req == undefined || (req.status !== RequestStatus.DriverAccepted && req.status !== RequestStatus.Arrived)) {
            callback(new RideNotAvailableException())
            return
        }
        let insertResult = await RequestChat.insert({
            request: req,
            content: content as string,
            sentBy: this.socket.user.t == ClientType.Driver ? ClientType.Driver : ClientType.Rider
        });
        let message = await RequestChat.findOne({where: {id: insertResult.raw.insertId}, relations: ['request']})
        callback(message)
        if(this.socket.user.t == ClientType.Driver) {
            const socketId = await Container.get(Redis).rider.getSocketIdByRiderId(req.rider.id);
            (Container.get('io') as any).of('/riders').to(socketId).emit('messageReceived', message);
            Container.get(Notifier).rider.message(req.rider, message)
        } else {
            const socketId = await Container.get(Redis).driver.getSocketIdByDriverId(req.driver.id);
            (Container.get('io') as any).of('/drivers').to(socketId).emit('messageReceived', message);
            Container.get(Notifier).driver.message(req.driver, message)
        }
    }
}