import { TopUpWalletDTO } from "../wallet-controller";
import { Driver } from "../../../entities/driver";
import { Rider } from "../../../entities/rider";
import IGateway from "./gateway.interface";
import Stripe from "stripe";

export default class StripeGateway extends IGateway {
    async charge(dto: TopUpWalletDTO, user: Driver | Rider) {
        let stripe = new Stripe(this.gateway.privateKey, {
            apiVersion: null
        });
        let amount = Math.floor(dto.amount) * 100

        let res = await stripe.charges.create({
            currency: dto.currency.toLowerCase(),
            source: dto.token,
            amount
        })

        if(res.status == 'succeeded') {
            return res.id;
        } else {
            throw new Error(res.status);
        }
    }
}