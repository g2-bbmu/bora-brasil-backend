import { TopUpWalletDTO, CardInfo } from "../wallet-controller";
import { Driver } from "../../../entities/driver";
import { Rider } from "../../../entities/rider";
import IGateway from "./gateway.interface";
import rp from 'request-promise-any';
import * as xmlParser from "xml-js";

export default class PaygateGateway extends IGateway {
    async charge(dto: TopUpWalletDTO, user: Driver | Rider) {
        let cardInfo: CardInfo = JSON.parse(dto.token);
        let text = `<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://www.paygate.co.za/PayHOST" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><SOAP-ENV:Body><ns1:SinglePaymentRequest>`
            + "    <ns1:CardPaymentRequest>"
            + "        <ns1:Account>"
            + `            <ns1:PayGateId>${this.gateway.publicKey}</ns1:PayGateId>`
            + `            <ns1:Password>${this.gateway.privateKey}</ns1:Password>`
            + "        </ns1:Account>"
            + "        <ns1:Customer>"
            + "            <ns1:Title>" + 'Mr' + "</ns1:Title>"
            + "            <ns1:FirstName>" + (user.firstName || 'Unknown') + "</ns1:FirstName>"
            + "            <ns1:LastName>" + (user.lastName || 'Unknown') + "</ns1:LastName>"
            + "            <ns1:Telephone>0" + user.mobileNumber + "</ns1:Telephone>"
            + "            <ns1:Mobile>0" + user.mobileNumber + "</ns1:Mobile>"
            + "            <ns1:Email>" + (user.email || 'something@gmail.com') + "</ns1:Email>"
            + "        </ns1:Customer>"
            + `        <ns1:CardNumber>${cardInfo.cardNumber}</ns1:CardNumber>`
            + `        <ns1:CardExpiryDate>${cardInfo.expiryMonth < 10 ? `0${cardInfo.expiryMonth}` : cardInfo.expiryMonth}${cardInfo.expiryYear.toString().length > 2 ? cardInfo.expiryYear : '20' + cardInfo.expiryYear.toString()}</ns1:CardExpiryDate>`
            + `        <ns1:CVV>${cardInfo.cvv}</ns1:CVV>`
            + "        <ns1:BudgetPeriod>0</ns1:BudgetPeriod>"
            + "        <ns1:Redirect>"
            + "            <ns1:NotifyUrl>" + 'https://www.mytestsite.com/notify' + "</ns1:NotifyUrl>"
            + "            <ns1:ReturnUrl>" + 'https://www.mytestsite.com/return' + "</ns1:ReturnUrl>"
            + "        </ns1:Redirect>"
            + "        <ns1:Order>"
            + "            <ns1:MerchantOrderId>" + "INV101" + "</ns1:MerchantOrderId>"
            + `            <ns1:Currency>${dto.currency}</ns1:Currency>`
            + `            <ns1:Amount>${dto.amount * 100}</ns1:Amount>`
            + "        </ns1:Order>"
            + "    </ns1:CardPaymentRequest>"
            + "</ns1:SinglePaymentRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        let result = await rp({
            method: 'POST',
            uri: 'https://secure.paygate.co.za/payhost/process.trans',
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'text/xml',
                'Accept': 'text/xml',
                'SOAPAction': 'SinglePayment'
            },
            body: text,
            json: false
        });
        let parsed = xmlParser.xml2js(result, { compact: true });
        let status = parsed['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns2:SinglePaymentResponse']['ns2:CardPaymentResponse']['ns2:Status']['ns2:TransactionStatusDescription']._text;
        if (status != 'Approved') {
            console.log(`Unknown Status for PayGate:${parsed['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns2:SinglePaymentResponse']['ns2:CardPaymentResponse']['ns2:Status']}`)
            throw Error(parsed['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns2:SinglePaymentResponse']['ns2:CardPaymentResponse']['ns2:Status']['ns2:ResultDescription']._text);
        }
        return parsed['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns2:SinglePaymentResponse']['ns2:CardPaymentResponse']['ns2:Status']['ns2:TransactionId']._text;
    }
}