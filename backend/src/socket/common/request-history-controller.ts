import SocketController from "../interfaces/socket.controller.interface";
import { UserType } from "../../models/enums/enums";
import { Request } from "../../entities/request";
import { Complaint } from "../../entities/complaint";
import { DriverTransaction } from "../../entities/driver-transaction";
import { RiderTransaction } from "../../entities/rider-transaction";
import { ClientType } from "../../models/client-jwt-decoded";

export default class RequestHistoryController extends SocketController {
    constructor(socket: any, userType: UserType) {
        super(socket, userType)
        socket.on('GetRequestHistory', this.getAll.bind(this))
        socket.on('WriteComplaint', this.writeComplaint.bind(this))
        socket.on('HideHistoryItem', this.hideRequest.bind(this))
        socket.on('GetTransactions', this.getTransactions.bind(this))
    }

    async getAll(callback) {
        if(this.socket.user.t == ClientType.Driver) {
            callback(await Request.find({where: {driver: {id: this.socket.user.id} }, take : 50, order: {id: 'DESC'}}));
        } else {
            callback(await Request.find({where: {rider: {id: this.socket.user.id}, isHidden: false }, take : 50, order: {id: 'DESC'}}));
        }
    }

    async writeComplaint(travelId: number, subject: string, content: string, callback) {
        let request = await Request.findOne(travelId);
        await Complaint.insert({
            request: request,
            requestedBy: this.socket.user.t,
            subject: subject,
            content: content
        })
        callback();
    }

    async hideRequest(travelId: number, callback) {
        await Request.update(travelId, {isHidden: true})
        callback();
    }

    async getTransactions(callback) {
        if(this.socket.user.t == ClientType.Driver) {
            callback(await DriverTransaction.find({where: {driver: {id: this.socket.user.id}}, take : 50, order: {id: 'DESC'}}))
        } else {
            callback(await RiderTransaction.find({where: {rider: {id: this.socket.user.id}}, take : 50, order: {id: 'DESC'}}))
        }
    }
}