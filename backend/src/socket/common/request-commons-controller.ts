import SocketController from "../interfaces/socket.controller.interface";
import { Request, RequestStatus } from "../../entities/request";
import UnknownException from "../exceptions/unknown.exception";
import Container from "typedi";
import { Driver, DriverStatus } from "../../entities/driver";
import { ClientType } from "../../models/client-jwt-decoded";
import Redis from "../../libs/redis/redis";
import { Stats } from "../../models/stats";

import {
  reverseTransaction,
  marketplaceId,
  getSellerByCPFCNPJ,
} from "../../services/PaymentApi";
import { Media, MediaType } from "../../entities/media";
import { recalculateFareDTO } from "../../models/dto/calculate-fare.dto";
import SocketException from "../exceptions/socket-exception";
import { CostServices } from '../../services/CostServices';

export default class RequestCommonsController extends SocketController {
  constructor(socket: any) {
    super(socket);
    socket.on("Cancel", this.cancelService.bind(this));
    socket.on("GetCurrentRequestInfo", this.GetCurrentRequestInfo.bind(this));
    socket.on("ChangeRoute", this.changeRoute.bind(this));
  }

  async changeRoute(
    recalculateFateDTO: recalculateFareDTO,
    callback: (arg0: Request | SocketException) => void,
  ) {
    try {
      let query = this.socket.user.t == ClientType.Driver
        ? { driver: { id: this.socket.user.id } }
        : { rider: { id: this.socket.user.id } };
      let request = await Request.findOne({
        where: query,
        order: { id: "DESC" },
        relations: [
          "driver",
          "driver.media",
          "rider.media",
          "driver.car",
          "rider",
          "service",
          "region",
        ],
      });

      const driverConnectionId = await Container.get(Redis).driver.getSocketIdByDriverId(request.driver.id);
      const riderConnectionId = await Container.get(Redis).rider.getSocketIdByRiderId(request.rider.id);

      if (this.socket.user.t == ClientType.Driver) {
        request.recalculatedTravelDistance =
          recalculateFateDTO.recalculatedTravelDistance;
        request.recalculatedTravelTime = Math.round(recalculateFateDTO.recalculatedTravelTime / 60);
        request.recalculatedTravelPath =
          recalculateFateDTO.recalculatedTravelPath;
      }

      if (this.socket.user.t == ClientType.Rider) {
        request.addresses = recalculateFateDTO.locations.map((x) => x.add);
        request.points = recalculateFateDTO.locations.map((x) => x.loc);
        request.recalculatedTravelDistance =
          recalculateFateDTO.recalculatedTravelDistance;
        request.recalculatedTravelTime = Math.round(recalculateFateDTO.recalculatedTravelTime / 60);
        request.recalculatedTravelPath =
          recalculateFateDTO.recalculatedTravelPath;
        request.estimatedTravelDistance =
          recalculateFateDTO.recalculatedTravelDistance;

        request.costBest = CostServices.calculateCost(
          request.region,
          request.service,
          request.estimatedTravelDistance,
          request.recalculatedTravelTime,
          1
        );
      }

      await Request.save(request);

      callback(request);

      if (driverConnectionId) {
        (Container.get("io") as any)
          .of("/drivers")
          .to(driverConnectionId)
          .emit("routeUpdated", request);
      }

      if (riderConnectionId) {
        (Container.get("io") as any)
          .of("/riders")
          .to(riderConnectionId)
          .emit("routeUpdated", request);
      }
    } catch (error) {
      if (error.json?.error_message != null) {
        callback(new UnknownException(error.json?.error_message));
      } else {
        callback(new UnknownException(error.message));
      }
    }
  }

  async GetCurrentRequestInfo(callback) {
    let query = this.socket.user.t == ClientType.Driver
      ? { driver: { id: this.socket.user.id } }
      : { rider: { id: this.socket.user.id } };

    const request = await Request.findOne({
      where: query,
      order: { id: "DESC" },
      relations: [
        "driver",
        "driver.media",
        "rider.media",
        "driver.car",
        "rider",
        "service",
      ],
    });

    let notFinishedRequestTypes: RequestStatus[] = [
      RequestStatus.Requested,
      RequestStatus.DriverAccepted,
      RequestStatus.Found,
      RequestStatus.Started,
      RequestStatus.WaitingForPostPay,
      RequestStatus.WaitingForPrePay,
      RequestStatus.Booked,
      RequestStatus.Arrived,
    ];

    if (this.socket.user.t == ClientType.Rider) {
      notFinishedRequestTypes.push(RequestStatus.WaitingForReview);
      notFinishedRequestTypes.push(RequestStatus.DriverCanceled);
    }

    if (
      request != undefined &&
      notFinishedRequestTypes.filter((x) => x == request.status).length != 0
    ) {
      if (this.socket.user.t == ClientType.Rider && request.driver != null) {
        let loc = await Container.get(Redis).driver.getCoordinate(
          request.driver.id,
        );
        if (loc != null) {
          if (request.isChecked) {
            request.status = RequestStatus.Finished;
          }
          callback({ request: request, driverLocation: loc });
        } else {
          callback({ request: request });
        }
      } else {
        callback({ request: request });
      }
    } else {
      callback(new UnknownException("No travel was found"));
    }
    if (
      request &&
      this.socket.user.t == ClientType.Rider &&
      request.status === RequestStatus.DriverCanceled &&
      !request.isChecked
    ) {
      request.isChecked = true;
      await Request.save(request);
    }
  }

  async cancelService(callback) {
    if (this.socket.user.t == ClientType.Driver) {
      let request = await Request.findOne({
        where: { driver: { id: this.socket.user.id } },
        order: { id: "DESC" },
        relations: ["rider"],
      });

      const driver = await Driver.findOne(this.socket.user.id);

      if (request.paymentType === "credit") {
        const seller = await getSellerByCPFCNPJ(marketplaceId, driver.cpf);

        const revTransactionData = {
          on_behalf_of: seller.data.id,
          amount: Math.trunc(request.costBest * 100),
        };

        await reverseTransaction(
          marketplaceId,
          request.transactionId,
          revTransactionData,
        );
      }

      await Request.update(request.id, {
        cost: 0,
        costBest: 0,
        status: RequestStatus.DriverCanceled,
        finishTimestamp: new Date().getTime(),
      });

      const socketId = await Container.get(Redis).rider.getSocketIdByRiderId(request.rider.id);
      
      (Container.get("io") as any)
        .of("/riders")
        .to(socketId)
        .emit("cancelTravel");
      await Driver.update(this.socket.user.id, { status: DriverStatus.Online });
    } else {
      let request = await Request.findOne({
        where: { rider: { id: this.socket.user.id } },
        order: { id: "DESC" },
        relations: ["driver"],
      });

      if (request.paymentType === "credit") {
        const seller = await getSellerByCPFCNPJ(
          marketplaceId,
          request.driver.cpf,
        );
        const revTransactionData = {
          on_behalf_of: seller.data.id,
          amount: Math.trunc(request.costBest * 100),
        };
        await reverseTransaction(
          marketplaceId,
          request.transactionId,
          revTransactionData,
        );
      }

      const socketId = await Container.get(Redis).driver.getSocketIdByDriverId(request.driver.id);

      await Request.update(request.id, {
        cost: 0,
        costBest: 0,
        status: RequestStatus.RiderCanceled,
        finishTimestamp: new Date().getTime(),
      });
      (Container.get("io") as any)
        .of("/drivers")
        .to(socketId)
        .emit("cancelTravel");
      await Driver.update(request.driver.id, { status: DriverStatus.Online });
    }

    Container.get(Stats).availableDrivers =
      Container.get(Stats).availableDrivers + 1;
    (Container.get("io") as any)
      .of("/cms")
      .emit("statChanged", { key: "availableDrivers", value: 1 });
    Container.get(Stats).inServiceDrivers =
      Container.get(Stats).inServiceDrivers - 1;
    (Container.get("io") as any)
      .of("/cms")
      .emit("statChanged", { key: "inService", value: -1 });
    callback();
  }
}
