import {Server} from "socket.io";
import SocketController from '../interfaces/socket.controller.interface';
import Occurrence from '../../entities/occurrence';
import {Driver} from '../../entities/driver';
import Container from 'typedi';

export default class CreateOccurrenceController extends SocketController {
    constructor(socket: any) {
        super(socket);
        socket.on('AddOccurrence', this.addOccurrence.bind(this));
    }

    async addOccurrence(callback: (occurrence: Occurrence | []) => void) {
        try {
            const driver = await Driver.findOne(this.socket.user.id);

            if (!driver) {
                callback([]);
                return;
            }

            const occurrenceNotFinished = await Occurrence.findOne({
                where: {
                    finishedAt: null,
                    driver_id: driver.id
                },
                relations: ['driver', 'driver.car']
            })

            if (occurrenceNotFinished) {
                callback([]);
                return;
            }

            const occurrence = Occurrence.create({ driver });
            const valueDate = new Date();
            const useDate = new Date(Date.UTC(valueDate.getFullYear(), valueDate.getMonth(), valueDate.getDate(), valueDate.getHours(), valueDate.getMinutes(), valueDate.getSeconds()));
            occurrence.startDate = useDate
            await occurrence.save();

            const occurrenceCreated = await Occurrence.findOne(occurrence.id, { relations: ['driver', 'driver.car'] });

            (Container.get('io') as Server).of('/cms').emit("occurrenceCreated", occurrenceCreated);
            callback(occurrence)
        } catch (err) {
            callback([]);
        }

    }
}