import SocketController from "../interfaces/socket.controller.interface";
import Container from "typedi";
import Redis from "../../libs/redis/redis";
import { DriverStatus, Driver } from "../../entities/driver";
import { Request, RequestStatus } from "../../entities/request";
import CoordinateXY from "../../models/coordinatexy";
import GeoLib from "../../libs/geo";
import OrderAlreadyTaken from "../exceptions/order-already-taken";
import DistanceCalculationFailedException from "../exceptions/distance-calculation-failed.exception";
import { getRepository } from "typeorm";
import { Stats } from "../../models/stats";
import {
  createTransaction,
  getBuyerByCPF,
  getCardById,
  getSellerByCPFCNPJ,
  marketplaceId,
} from "../../services/PaymentApi";
import { createOrder } from "../../services/antifraudeApi";
import RejectedByAntiFraudException from "../exceptions/rejected-by-anti-fraud.exception";
import PaymentException from "../exceptions/payment-exception";
import { AcceptOrderDTO } from "../../models/dto/acceptOrder.dto";
import { findDriverById } from '../../express/repositories/driver';
import UnknownException from '../exceptions/unknown.exception';
import { GetDriverStatusDto } from '../dto/get-driver-status.dto';

export default class PreServiceController extends SocketController {
  constructor(socket: any) {
    super(socket);
    socket.on("UpdateStatus", this.changeStatus.bind(this));
    socket.on("GetAvailableRequests", this.getRequests.bind(this));
    socket.on("AcceptOrder", this.acceptOrder.bind(this));
    socket.on("GetStatus", this.getStatus.bind(this));
  }

  async getStatus(callback: (response: GetDriverStatusDto) => void): Promise<void> {
    const driver = await findDriverById(this.socket.user.id);

    if (!driver) {
      return this.throwException(callback, new UnknownException('driver not found'))
    }

    let expired = false;

    if (driver.status === DriverStatus.Expired) {
      expired = true;
      driver.status = DriverStatus.Offline;
      await Driver.save(driver);
    }

    return callback({
      driverStatus: driver.status,
      expired
    })
  }

  async changeStatus(turnOnline: boolean, callback) {
    if (!turnOnline) {
      await Container.get(Redis).driver.expire(this.socket.user.id);
      await Container.get(Redis).driver.deleteDriverSocketId(this.socket.user.id);
      (Container.get("io") as any)
        .of("/cms")
        .emit("driverLocationUpdated", { id: this.socket.user.id, loc: null });
    }
    await getRepository("Driver").save({
      id: this.socket.user.id,
      status: turnOnline ? DriverStatus.Online : DriverStatus.Offline,
      lastSeenTimestamp: new Date().getTime(),
    });
    callback();
  }

  async getRequests(callback) {
    let [requests, driver] = await Promise.all([
      Container.get(Redis).request.getForDriver(this.socket.user.id),
      Driver.findOne(this.socket.user.id, { relations: ["services"] }),
    ]);
    if (driver.status !== DriverStatus.Online) {
      callback(new UnknownException("Driver offline"));
      return;
    }
    let driverServiceIds = driver.services.map((x) => x.id);
    requests = requests.filter((x) => {
      return driverServiceIds.includes(x.serviceId);
    });
    for (let request of requests) {
      Container.get(Redis).request.driverNotified(
        request.id,
        this.socket.user.id
      );
    }
    callback(requests);
  }

  async acceptOrder(acceptOrderDTO: AcceptOrderDTO, callback) {
    const driver = await Driver.findOne(this.socket.user.id);
    const directionsInfo = acceptOrderDTO.directionsInfo;
    let [travel, driverLocation] = await Promise.all([
      Request.findOne(acceptOrderDTO.travelId, {
        relations: ["driver", "driver.media", "rider", "service"],
      }),
      Container.get(Redis).driver.getCoordinate(this.socket.user.id),
    ]);
    if (travel == null) {
      this.throwException(callback, new OrderAlreadyTaken());
      return;
    }
    if (
      travel.status != RequestStatus.Requested &&
      travel.status != RequestStatus.Found &&
      travel.status != RequestStatus.Booked
    ) {
      this.throwException(callback, new OrderAlreadyTaken());
      return;
    }
    await Request.update(acceptOrderDTO.travelId, {
      status: RequestStatus.Found,
      driver: { id: this.socket.user.id },
      pickupDistance: acceptOrderDTO.directionsInfo.distance,
      pickupTime: acceptOrderDTO.directionsInfo.duration,
      pickupPath: acceptOrderDTO.directionsInfo.path,
      driverRequestOS: this.socket.handshake.query.os,
      driverRequestVersion: this.socket.handshake.query.ver
    });

    if (travel.paymentType === "credit") {
      try {
        const buyer = await getBuyerByCPF(marketplaceId, travel.rider.cpf);
        const seller = await getSellerByCPFCNPJ(marketplaceId, driver.cpf);

        const { default_debit: cardId, id: buyer_id } = buyer.data;
        const card = await getCardById(marketplaceId, cardId);

        const { default_debit } = buyer.data;

        /*const travelCostAmount = (travel.costBest * 100).toFixed(0);*/

        const createTransactionData = {
          amount: travel.costBest.toFixed(0),
          currency: "BRL",
          description: `Corrida Nº ${travel.id} - Valor inicial`,
          payment_type: "credit",
          capture: false,
          on_behalf_of: seller.data.id,
          source: {
            usage: "single_use",
            amount: travel.costBest.toFixed(0),
            currency: "BRL",
            type: "card",
            card: {
              id: default_debit,
              amount: travel.costBest.toFixed(0),
            },
          },
        };

        const transaction = await createTransaction(
          marketplaceId,
          createTransactionData
        );

        if (transaction.data == null) {
          this.throwException(callback, new RejectedByAntiFraudException());
          await Request.update(acceptOrderDTO.travelId, {
            status: RequestStatus.RejectedByAntiFraud,
            driver: { id: this.socket.user.id },
          });

          const socketId = await Container.get(Redis).rider.getSocketIdByRiderId(travel.rider.id);
      
          (Container.get("io") as any)
            .of("/riders")
            .to(socketId)
            .emit("requestRejectedByAntiFraud", acceptOrderDTO.travelId);
          await this.cancelRequest(acceptOrderDTO.travelId);
          return;
        }

        await Request.update(acceptOrderDTO.travelId, {
          transactionId: transaction.data.id,
        });

        const cardDetails = await getCardById(marketplaceId, default_debit);

        // const ipv4 = getIPV4(this.socket.request.connection.remoteAddress);

        const antifraudeRequestData = {
          id: transaction.data.id,
          total_amount: travel.costBest,
          currency: "BRL",
          purchased_at: `${new Date().toISOString().split(".")[0]}Z`,
          analyze: true,
          customer: {
            id: buyer_id,
            name: `${travel.rider.firstName} ${travel.rider.lastName}`,
            tax_id: travel.rider.cpf,
            phone1: String(travel.rider.mobileNumber),
            email: travel.rider.email,
            created_at: new Date().toISOString().split("T")[0],
          },
          payment: [
            {
              type: "credit",
              bin: card.data.first4_digits,
              last4: card.data.last4_digits,
              expiration_date: `${card.data.expiration_month}20${card.data.expiration_year}`,
              status: "approved",
            },
          ],
          seller: {
            id: seller.data.id,
            name: `${seller.data.first_name} ${seller.data.last_name}`,
            created_at: new Date().toISOString().split("T")[0],
          },
          // Modificação nome escrito no cartão
          billing: {
            name: cardDetails.data.holder_name, //
          },
          // Pega o IPV4 do usuário
          // ip: ipv4 || null,
        };

        const antifraudeResult = await createOrder(antifraudeRequestData);

        if (antifraudeResult?.data?.order?.recommendation !== "APPROVE") {
          this.throwException(callback, new RejectedByAntiFraudException());
          await Request.update(acceptOrderDTO.travelId, {
            status: RequestStatus.RejectedByAntiFraud,
            driver: { id: this.socket.user.id },
          });

          const socketId = await Container.get(Redis).rider.getSocketIdByRiderId(travel.rider.id);
      
          (Container.get("io") as any)
            .of("/riders")
            .to(socketId)
            .emit("requestRejectedByAntiFraud", acceptOrderDTO.travelId);
          await this.cancelRequest(acceptOrderDTO.travelId);
          return;
        }
      } catch (error) {
        this.throwException(callback, new PaymentException());
        await Request.update(acceptOrderDTO.travelId, {
          status: RequestStatus.Expired,
          driver: { id: this.socket.user.id },
        });

        const socketId = await Container.get(Redis).rider.getSocketIdByRiderId(travel.rider.id);

        (Container.get("io") as any)
          .of("/riders")
          .to(socketId)
          .emit("transactionRejected", acceptOrderDTO.travelId);
        await this.cancelRequest(acceptOrderDTO.travelId);
        return;
      }
    }

    let locs: CoordinateXY[] = [travel.points[0], driverLocation];
    let metrics = await Container.get(GeoLib).calculateDistance(locs);
    if (metrics.json.status != "OK") {
      this.throwException(
        callback,
        new DistanceCalculationFailedException(metrics.json.error_message)
      );
      return;
    }
    let dt = new Date();
    let etaPickup = dt.setSeconds(
      dt.getSeconds() + metrics.json.rows[0].elements[0].duration.value
    );
    let _ = await Promise.all([
      Driver.update(this.socket.user.id, { status: DriverStatus.InService }),
      Request.update(acceptOrderDTO.travelId, {
        status: RequestStatus.DriverAccepted,
        etaPickup: etaPickup,
        driver: { id: this.socket.user.id },
      }),
    ]);
    Container.get(Stats).availableDrivers =
      Container.get(Stats).availableDrivers - 1;
    (Container.get("io") as any)
      .of("/cms")
      .emit("statChanged", { key: "availableDrivers", value: -1 });
    Container.get(Stats).inServiceDrivers =
      Container.get(Stats).inServiceDrivers + 1;
    (Container.get("io") as any)
      .of("/cms")
      .emit("statChanged", { key: "inService", value: 1 });
    travel = await Request.findOne(travel.id, {
      relations: ["rider", "driver", "driver.media", "service", "operator"],
    });
    if (travel.operator != null) {
      (Container.get("io") as any)
        .of("/operators")
        .clients((error, clients) => {
          if (error) throw error;
          let _clients = clients.filter((x) => {
            return (
              ((Container.get("io") as any).of("/operators").connected[
                x
              ] as any).user.id == travel.operator.id
            );
          });
          (Container.get("io") as any)
            .of("/operators")
            .to(_clients)
            .emit("driverAccepted", { travel, directionsInfo });
        });
    } else {
      const socketId = await Container.get(Redis).rider.getSocketIdByRiderId(travel.rider.id);

      (Container.get("io") as any)
        .of("/riders")
        .to(socketId)
        .emit("driverAccepted", { travel, directionsInfo });
    }
    await this.cancelRequest(acceptOrderDTO.travelId);
    callback(travel);
  }

  private async cancelRequest(travelId: number) {
    let otherDriverIds = await Container.get(Redis).request.getDriversNotified(
      travelId
    );
    for (let otherDriverId of otherDriverIds) {
      if (this.socket.user.id !== otherDriverId) {
        const socketId = await Container.get(Redis).driver.getSocketIdByDriverId(this.socket.user.id);
        (Container.get("io") as any)
          .of("/drivers")
          .to(socketId)
          .emit("cancelRequest", travelId);
      }
    }
    await Container.get(Redis).request.expire(travelId);
  }
}
