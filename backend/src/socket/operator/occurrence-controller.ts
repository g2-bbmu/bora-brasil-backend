import SocketController from "../interfaces/socket.controller.interface";
import Occurrence, { IOccurrence } from "../../entities/occurrence";
import { Operator } from "../../entities/operator";

export default class OccurrenceController extends SocketController {
  constructor(socket: any) {
    super(socket);
    socket.on("getOccurrences", this.getOccurrences.bind(this));
    socket.on("updateOccurence", this.updateOccurence.bind(this));
    socket.on("finishOccurence", this.finishOccurence.bind(this));
  }

  async getOccurrences(callback: (occurrence: Occurrence[]) => void) {
    const occurrences = await Occurrence.find();

    callback(occurrences);
  }

  async updateOccurence(
    data: IOccurrence,
    data2: any,
    callback: (occurrence: Occurrence | string) => void
  ) {
    const operator_id = data2.operatorId;

    // Como pegar essas informações?
    const { calledLocalAuthority, status, details } = data;
    // ----

    const occurrence = await Occurrence.findOne(data2.occurrenceId);

    if (!occurrence) {
      callback("Occurrence not found!");
    }

    const operator = await Operator.findOne(operator_id);

    if (!operator) {
      callback("Operator not found!");
    }

    Object.assign(occurrence, {
      calledLocalAuthority,
      status,
      details: details,
      operator_id,
      operator,
    });

    await occurrence.save();

    callback(occurrence);
  }

  async finishOccurence(
    data: any,
    callback: (occurrence: Occurrence | string) => void
  ) {
    const occurrence = await Occurrence.findOne(data.occurrenceId);

    if (!occurrence) {
      callback("Occurrence not found!");
    }

    occurrence.finishedAt = new Date();

    await occurrence.save();

    callback(occurrence);
  }
}
