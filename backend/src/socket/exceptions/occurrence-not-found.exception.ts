import SocketException, { SocketStatus } from './socket-exception'

export default class OccurrenceNotFoundException extends SocketException {
    constructor() {
        super(SocketStatus.NotFound, 'Occurrence not found!')
    }
}