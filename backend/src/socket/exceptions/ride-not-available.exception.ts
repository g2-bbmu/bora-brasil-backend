import SocketException, {SocketStatus} from "./socket-exception";

export default class RideNotAvailableException extends SocketException {
    constructor() {
        super(SocketStatus.RideNotAvailable, `Viagem não disponível`)
    }
}