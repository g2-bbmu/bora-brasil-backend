import SocketException, { SocketStatus } from './socket-exception'

export default class ValidationErrorException extends SocketException {
    constructor(message: string) {
        super(SocketStatus.ValidationError, message)
    }
}