import SocketException, { SocketStatus } from './socket-exception'

export default class OccurrenceAlreadyAcceptedException extends SocketException {
    constructor() {
        super(SocketStatus.OccurrenceAlreadyAccepted, 'Occurrence Already Accepted')
    }
}