import SocketException, { SocketStatus } from './socket-exception'

export default class OccurrenceAlreadyFinishedException extends SocketException {
    constructor() {
        super(SocketStatus.OccurrenceAlreadyFinished, 'Occurrence already finished')
    }
}