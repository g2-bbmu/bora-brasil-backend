import SocketException, { SocketStatus } from './socket-exception'

export default class OccurrenceNotAcceptedException extends SocketException {
    constructor() {
        super(SocketStatus.OccurrenceNotAccepted, 'Occurrence Not Accepted')
    }
}