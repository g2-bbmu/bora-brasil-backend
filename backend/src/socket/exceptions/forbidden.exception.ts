import SocketException, { SocketStatus } from './socket-exception'

export default class ForbiddenException extends SocketException {
    constructor() {
        super(SocketStatus.Forbidden, 'Forbidden')
    }
}