import SocketException, { SocketStatus } from './socket-exception'

export default class RiderUpdateException extends SocketException {
    constructor() {
        super(SocketStatus.RiderUpdateException, 'UpdateProfileRiderError!')
    }
}