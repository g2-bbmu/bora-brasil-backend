import SocketException, { SocketStatus } from './socket-exception'

export default class PaymentException extends SocketException {
    constructor() {
        super(SocketStatus.Payment, 'Payment Error!')
    }
}