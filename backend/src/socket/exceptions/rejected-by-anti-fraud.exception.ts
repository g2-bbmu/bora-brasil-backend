import SocketException, { SocketStatus } from './socket-exception'

export default class RejectedByAntiFraudException extends SocketException {
    constructor() {
        super(SocketStatus.RejectedByAntiFraudException, 'RejectedByAntiFraud!');
    }
}