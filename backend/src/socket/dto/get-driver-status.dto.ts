import { DriverStatus } from '../../entities/driver';

export interface GetDriverStatusDto {
    driverStatus: DriverStatus,
    expired: boolean
}