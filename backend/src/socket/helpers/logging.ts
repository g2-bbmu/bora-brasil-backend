export function logSocketRequests(socketId: any, data: any) {
    console.log(`socket-id: ${socketId} request-id: ${data.id} nsp: ${data.nsp} event: ${data.data[0]}`);
}