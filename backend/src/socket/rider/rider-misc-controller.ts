import SocketController from "../interfaces/socket.controller.interface";
import {Promotion} from "../../entities/promotion";
import Container from "typedi";
import Redis from "../../libs/redis/redis";
import {Media, MediaType} from "../../entities/media";
import Uploader from "../../libs/uploader";
import {Rider} from "../../entities/rider";
import {RiderAddress} from "../../entities/rider-address";
import UnknownException from "../exceptions/unknown.exception";
import CoordinateXY from "../../models/coordinatexy";
import {QueryDeepPartialEntity} from "typeorm/query-builder/QueryPartialEntity";
import {createBuyer, getBuyerByCPF, marketplaceId, ZoopApi} from '../../services/PaymentApi';
import { convertToNumericalString } from '../../utils/string';
import RiderUpdateException from '../exceptions/rider-update-exception';

export default class RiderMiscController extends SocketController {
    constructor(socket: any) {
        super(socket);
        socket.on('GetDriversLocations', this.getDriversLocation.bind(this));
        socket.on('UpdateProfile', this.editProfile.bind(this));
        socket.on('UpdateProfileImage', this.changeProfileImage.bind(this));
        socket.on('GetAddresses', this.getAddresses.bind(this));
        socket.on('DeleteAddress', this.deleteAddress.bind(this));
        socket.on('UpsertAddress', this.upsertAddress.bind(this));
        socket.on('GetPromotions', this.getPromotions.bind(this))
    }

    async editProfile(user: Rider, callback) {
        try {

            let _user: QueryDeepPartialEntity<Rider> = user;

            const cpfWithoutMask = convertToNumericalString(user.cpf);

            Object.assign(_user, {cpf: cpfWithoutMask});

            await Rider.update(this.socket.user.id, _user);

            const buyer = await getBuyerByCPF(marketplaceId, user.cpf);


            if (!buyer) {
                const createBuyerData = {
                    first_name: user.firstName,
                    last_name: user.lastName,
                    email: user.email,
                    taxpayer_id: _user.cpf,
                };

                await createBuyer(marketplaceId, createBuyerData);

            }

            callback();
        } catch (err) {
            this.throwException(callback, new RiderUpdateException());  
        }
    }

    async changeProfileImage(buffer, callback) {
        try {
            let mediaId = (await Media.insert({type: MediaType.RiderImage})).raw.insertId
            let uploader = new Uploader();
            let mediaRow = await uploader.doUpload(buffer, mediaId);
            await Rider.update(this.socket.user.id, {media: {id: mediaId}});
            callback(mediaRow);
        } catch (error) {
            this.throwException(callback, new UnknownException(error));
        }
    }

    async getDriversLocation(point: CoordinateXY, callback: (arg0: CoordinateXY[]) => void) {
        let result = await Container.get(Redis).driver.getClose(point, 1000);
        callback(result.map(x => x.location));
    }

    async getAddresses(callback) {
        callback(await RiderAddress.find({where: {rider: {id: this.socket.user.id}}}));
    }

    async deleteAddress(id: number, callback) {
        await RiderAddress.delete(id);
        callback();
    }

    async upsertAddress(address: RiderAddress, callback) {
        address.rider = {id: this.socket.user.id} as Rider;
        await RiderAddress.save(address);
        callback();
    }

    async getPromotions(callback) {
        let now = new Date().getTime();
        let promotions = (await Promotion.find({
            relations: ['media'],
            order: {'id': "DESC"}
        })).filter(x => x.startTimestamp < now && x.expirationTimestamp > now);
        callback(promotions);
    }
}