import SocketController from "../interfaces/socket.controller.interface";
import UnknowException from "../exceptions/unknown.exception";
import SocketException from "../exceptions/socket-exception";
import { In } from 'typeorm';
import { Api_preco_estimado_uber } from '../../entities/estimate';

export default class EstimateController extends SocketController {
    constructor(socket: any) {
        super(socket)
        this.socket.on('AddEstimate', this.addEstimate.bind(this))
    }

    async addEstimate(api_preco_estimado_uber: Api_preco_estimado_uber, callback: (arg0?: SocketException) => void) {

        try {
            await Api_preco_estimado_uber.save(api_preco_estimado_uber);
            callback();
        } catch (e) {
            callback(new UnknowException(e.message));
        }
    }

}