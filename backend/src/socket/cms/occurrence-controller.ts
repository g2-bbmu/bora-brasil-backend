import SocketController from '../interfaces/socket.controller.interface';
import Occurrence, {IOccurrence, OccurrenceStatus} from '../../entities/occurrence';
import {Operator, PermissionDefault} from '../../entities/operator';
import OccurrenceNotFoundException from "../exceptions/occurrence-not-found.exception";
import SocketException from "../exceptions/socket-exception";
import UnknownException from "../exceptions/unknown.exception";
import OccurrenceAlreadyFinishedException from "../exceptions/occurrence-already-finished.exception";
import OccurrenceAlreadyAcceptedException from "../exceptions/occurrence-already-accepted.exception";
import ValidationErrorException from "../exceptions/validation-error.exception";
import Container from "typedi";
import {Server} from "socket.io";
import CMSException, {PermissionDeniedException} from "./exceptions/cms.exception";

export default class OccurrenceController extends SocketController {
    constructor(socket: any) {
        super(socket);
        socket.on('getNotFinishedOccurrences', this.getNotFinishedOccurrences.bind(this));
        socket.on('finishOccurrence', this.finishOccurrence.bind(this));
        socket.on('acceptOccurrence', this.acceptOccurrence.bind(this));
    }

    async getNotFinishedOccurrences(data: any, callback: (occurrence: Occurrence[], err: CMSException) => void) {
        const operator_id = this.socket.user.id

        const operator = await Operator.findOne(operator_id)

        if (!operator || !operator.permissionPanicButton.includes(PermissionDefault.View)) {
            callback(null, new PermissionDeniedException())
            return
        }

        const occurrences = await Occurrence.find({
            where: {
                finishedAt: null,
            },
            relations: ['driver', 'driver.car']
        })

        callback(occurrences, null)
    }

    async acceptOccurrence(data: { id: number }, callback: (occurrence: Occurrence | SocketException | CMSException) => void) {
        const operator_id = this.socket.user.id

        const operator = await Operator.findOne(operator_id)

        if (!operator || !operator.permissionPanicButton.includes(PermissionDefault.View)) {
            callback(new PermissionDeniedException())
            return
        }

        const occurrence = await Occurrence.findOne(data.id)

        if (!occurrence) {
            callback(new OccurrenceNotFoundException())
            return
        }

        if (occurrence.acceptedAt) {
            callback(new OccurrenceAlreadyAcceptedException())
            return
        }

        occurrence.acceptedAt = new Date()
        occurrence.acceptedBy = operator;

        await occurrence.save();
        (Container.get('io') as Server).of('/cms').emit("occurrenceAccepted", { id: occurrence.id, driver_id: occurrence.driver_id });

        callback(occurrence)

    }

    async finishOccurrence(data: IOccurrence, callback: (occurrence: Occurrence | SocketException | CMSException) => void) {
        const operator_id = this.socket.user.id

        const operator = await Operator.findOne(operator_id)

        if (!operator || !operator.permissionPanicButton.includes(PermissionDefault.View)) {
            callback(new PermissionDeniedException())
            return
        }

        const {calledLocalAuthority, status, details, id} = data;

        if (!Object.values(OccurrenceStatus).includes(status)) {
            callback(new ValidationErrorException('Status should be a member of OccurrenceStatus'))
            return;
        }

        const occurrence = await Occurrence.findOne(id)

        if (!occurrence) {
            callback(new OccurrenceNotFoundException())
            return;
        }

        if (occurrence.acceptedAt === null) {
            occurrence.acceptedAt = new Date()
            occurrence.acceptedBy = operator
        }

        if (occurrence.finishedAt !== null) {
            callback(new OccurrenceAlreadyFinishedException())
            return;
        }

        Object.assign(occurrence, {
            calledLocalAuthority: calledLocalAuthority || false,
            status,
            details,
            finishedAt: new Date(),
            finishedBy: operator,
        });

        await occurrence.save();
        (Container.get('io') as Server).of('/cms').emit("occurrenceFinished", { id: occurrence.id, driver_id: occurrence.driver_id });

        callback(occurrence);
    }


}