import Container from "typedi";

export default class RedisRider {
    redis: any;

    constructor() {
        this.redis = Container.get('redis');
    }

    async setRiderSocketId(riderId: number, socketId: string) {
        return await this.redis.setAsync(`socket-connected-rider:${riderId}`, socketId)
    }

    async deleteRiderSocketId(riderId: number) {
        return await this.redis.delAsync(`socket-connected-rider:${riderId}`)
    }

    async getSocketIdByRiderId(riderId: number) {
        return await this.redis.getAsync(`socket-connected-rider:${riderId}`)
    }
}