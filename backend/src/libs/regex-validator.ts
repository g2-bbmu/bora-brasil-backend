export const isValidCpf = (cpf: string) => {
    let Soma = 0;
    let Resto;
    const strCpf = cpf.replace(/\D/g, '');

    if (strCpf === '00000000000') return false;

    for (let i = 1; i <= 9; i += 1) { Soma += parseInt(strCpf.substring(i - 1, i), 10) * (11 - i); }
    Resto = (Soma * 10) % 11;

    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(strCpf.substring(9, 10), 10)) return false;

    Soma = 0;
    for (let i = 1; i <= 10; i += 1) { Soma += parseInt(strCpf.substring(i - 1, i), 10) * (12 - i); }
    Resto = (Soma * 10) % 11;

    if (Resto === 10 || Resto === 11) Resto = 0;
    return Resto === parseInt(strCpf.substring(10, 11), 10);

}