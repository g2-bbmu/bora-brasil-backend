import { customAlphabet } from 'nanoid';

const passwordGenerator = customAlphabet('123456789ABCDEFGHJKMNPQRSTUVWXYZ', 6)

export const generatePassword = (): string => {
    return passwordGenerator();
}