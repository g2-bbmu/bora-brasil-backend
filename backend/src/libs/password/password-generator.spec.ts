import { generatePassword } from './password-generator';

describe('Password Generator', () => {
    it('should return a string', () => {
        const result = generatePassword();

        expect(typeof result).toBe('string');
    });
});