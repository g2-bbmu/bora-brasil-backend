import * as admin from "firebase-admin";
import { RequestChat } from "../../entities/request-chat";
import { Rider } from "../../entities/rider";
import Container from "typedi";
import { Driver } from "../../entities/driver";

export default class RiderNotifier {
  message(rider: Rider, message: RequestChat) {
    (Container.get("firebase.rider") as admin.app.App).messaging().sendToDevice(
      rider.notificationPlayerId,
      {
        notification: {
          body: message.content,
          sound: "notif_sound",
          badge: "1",
          titleLocKey: "notification_new_message_title",
        },
      },
    );
  }

  updateRouteDriver(driver: Driver, message: string) {
    (Container.get("firebase.driver") as admin.app.App).messaging()
      .sendToDevice(driver.notificationPlayerId, {
        notification: {
          body: message,
          sound: "notif_sound",
          badge: "1",
          titleLocKey: "notification_new_message_title",
        },
      });
  }

  updateRouteRider(rider: Rider, message: string) {
    (Container.get("firebase.rider") as admin.app.App).messaging().sendToDevice(
      rider.notificationPlayerId,
      {
        notification: {
          body: message,
          sound: "notif_sound",
          badge: "1",
          titleLocKey: "notification_new_message_title",
        },
      },
    );
  }

  arrived(rider: Rider) {
    (Container.get("firebase.rider") as admin.app.App).messaging().sendToDevice(
      rider.notificationPlayerId,
      {
        notification: {
          sound: "notif_sound",
          titleLocKey: "notification_arrived_title",
          bodyLocKey: "notification_arrived_body",
        },
      },
    );
  }

  started(rider: Rider) {
    (Container.get("firebase.rider") as admin.app.App).messaging().sendToDevice(
      rider.notificationPlayerId,
      {
        notification: {
          sound: "notif_sound",
          titleLocKey: "notification_started_title",
          bodyLocKey: "notification_started_body",
        },
      },
    );
  }

  waitingForPostPay(rider: Rider) {
    (Container.get("firebase.rider") as admin.app.App).messaging().sendToDevice(
      rider.notificationPlayerId,
      {
        notification: {
          sound: "notif_sound",
          titleLocKey: "notification_waiting_for_pay_title",
          bodyLocKey: "notification_waiting_for_pay_body",
        },
      },
    );
  }

  finished(rider: Rider) {
    (Container.get("firebase.rider") as admin.app.App).messaging().sendToDevice(
      rider.notificationPlayerId,
      {
        notification: {
          sound: "notif_sound",
          titleLocKey: "notification_finished_title",
          bodyLocKey: "notification_finished_body",
        },
      },
    );
  }
}
