import nodemailer from 'nodemailer';
const hbs = require('nodemailer-express-handlebars');
const exphbs = require('express-handlebars');
import path from 'path';

const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_SERVER,
    port: Number(process.env.EMAIL_PORT),
    secure: process.env.EMAIL_SSL === 'true',
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS,
    },
});

const viewPath = path.join(__dirname, '../../../views/emails');
/*
transporter.use(
    'compile',
    hbs({
        viewEngine: exphbs.create({
            extname: '.hbs',
            layoutsDir: viewPath,
            defaultLayout: false,
            partialsDir: path.join(viewPath, 'partials'),
        }),
        viewPath,
        extName: '.hbs',
    })
);
*/
export const sendEmail = async (receiver: string, subject: string, html: string, template: string, context?: any) => {
    try {
        const info = await transporter.sendMail({
            from: `Bora Brasil <ativacao@borabrasilmobilidade.com.br>`,
            to: receiver,
            subject,
            html,
            // template,
            // context,
        } as any);

        console.log('Message sent: %s', info.messageId);
    } catch (err) {
        console.log('Email error: ', err);
    }
};
