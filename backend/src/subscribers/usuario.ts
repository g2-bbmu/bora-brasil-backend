import { EventSubscriber, EntitySubscriberInterface, InsertEvent } from 'typeorm';
import { Usuario } from '../entities/usuario';

@EventSubscriber()
export class UsuarioSubscriber implements EntitySubscriberInterface<Usuario> {
    listenTo() {
        return Usuario;
    }

    async afterInsert(event: InsertEvent<Usuario>) {
        const usuario = event.entity;
        usuario.gerarCodigoIndicacao();

        await event.manager.getRepository(Usuario).save(usuario);
    }
}
