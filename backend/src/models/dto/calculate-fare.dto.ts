import CoordinateXY from "../coordinatexy";
import { LocationWithName } from "./service-request.dto";

export interface calculateFareDTO {
  locations?: CoordinateXY[];
  // estimatedTravelDistance: number;
  // estimatedTravelTime: number;
  estimatedTravelPath?: string;
}

export interface recalculateFareDTO {
  locations?: LocationWithName[];
  recalculatedTravelDistance: number;
  recalculatedTravelTime: number;
  recalculatedTravelPath?: string;
}
