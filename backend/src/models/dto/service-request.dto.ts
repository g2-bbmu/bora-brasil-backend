import CoordinateXY from "../coordinatexy";
import { PaymentMethod } from "braintree";

export interface ServiceRequestDto {
  locations: LocationWithName[];
  services: OrderedService[];
  riderId: number;
  intervalMinutes?: number;
  paymentType: "cash" | "credit";
  // estimatedTravelTime: number;
  // estimatedTravelDistance: number;
  estimatedTravelPath: string;
  priceEstimate: number;
}

export interface LocationWithName {
  loc: CoordinateXY;
  add: string;
}

export interface OrderedService {
  serviceId: number;
  quantity: number;
}
