import { DirectionsInfo } from "../../express/driver-controller";

export interface AcceptOrderDTO {
  directionsInfo: DirectionsInfo;
  travelId: number;
}
