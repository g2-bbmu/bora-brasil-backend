/**
* Adiciona todas as imagens que existem na tabela driver, mas não existem na tabela driver_documents_media na tabela driver_documents_media
**/
INSERT INTO taxi.driver_documents_media (SELECT driverId, mediaId FROM (SELECT  
		d.id driverId,
		carLocationContractMediaId mediaId,
        ddm.driverId driverDocumentsMediaDriverId
        
FROM taxi.driver d LEFT JOIN taxi.driver_documents_media ddm on ddm.mediaId = d.carLocationContractMediaId and d.id = ddm.driverId
WHERE ddm.driverId IS NULL AND d.carLocationContractMediaId IS NOT NULL

	UNION

SELECT  
		d.id driverId,
		carMediaId mediaId,
        ddm.driverId driverDocumentsMediaDriverId
        
FROM taxi.driver d LEFT JOIN taxi.driver_documents_media ddm on ddm.mediaId = d.carMediaId and d.id = ddm.driverId
WHERE ddm.driverId IS NULL AND d.carMediaId IS NOT NULL

	UNION
    
SELECT  
		d.id driverId,
		cnhMediaId mediaId,
        ddm.driverId driverDocumentsMediaDriverId
        
FROM taxi.driver d LEFT JOIN taxi.driver_documents_media ddm on ddm.mediaId = d.cnhMediaId and d.id = ddm.driverId
WHERE ddm.driverId IS NULL AND d.cnhMediaId IS NOT NULL

	UNION
    
SELECT  
		d.id driverId,
		selfieMediaId mediaId,
        ddm.driverId driverDocumentsMediaDriverId
        
FROM taxi.driver d LEFT JOIN taxi.driver_documents_media ddm on ddm.mediaId = d.selfieMediaId and d.id = ddm.driverId
WHERE ddm.driverId IS NULL AND d.selfieMediaId IS NOT NULL

	UNION
    
SELECT  
		d.id driverId,
		carDocumentMediaId mediaId,
        ddm.driverId driverDocumentsMediaDriverId
        
FROM taxi.driver d LEFT JOIN taxi.driver_documents_media ddm on ddm.mediaId = d.carDocumentMediaId and d.id = ddm.driverId
WHERE ddm.driverId IS NULL AND d.carDocumentMediaId IS NOT NULL

	UNION
    
SELECT  
		d.id driverId,
		proofOfAddressMediaId mediaId,
        ddm.driverId driverDocumentsMediaDriverId
        
FROM taxi.driver d LEFT JOIN taxi.driver_documents_media ddm on ddm.mediaId = d.proofOfAddressMediaId and d.id = ddm.driverId
WHERE ddm.driverId IS NULL AND d.proofOfAddressMediaId IS NOT NULL) A);


/**
* Corrige todas as imagens de perfil que estão nulas, mas existem na driver no campo driverMediaId
**/
UPDATE taxi.driver SET mediaId = driverMediaId WHERE driverMediaId IS NOT NULL AND mediaId IS NULL;


