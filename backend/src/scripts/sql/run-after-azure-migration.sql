-- Cria cod indicação para todos os usuários
UPDATE taxi.usuario SET codIndicacao = concat(substring_index(nomeCompleto, ' ', 1),substring_index(nomeCompleto, ' ', -1), id) WHERE id > 0;
-- Atualiza status para Pré-cadastro
UPDATE taxi.usuario SET status = 'Pré-Cadastro' WHERE status != 'Bloqueado' and status != 'Cancelado' and id > 0;
DELETE pm FROM taxi.pre_motorista pm LEFT JOIN taxi.usuario u on u.login = pm.email WHERE u.id is null and pm.id > 0
UPDATE taxi.pre_motorista SET cpf = REPLACE(REPLACE(cpf, '.', ''), '-', '') WHERE id > 0;
INSERT INTO `taxi`.`power_bi` (`id`, `url`, `nome`, `ativo`) VALUES ('5e5f4683-f0fb-4c3e-adb2-a548958d9a88', 'https://app.powerbi.com/reportEmbed?reportId=5e5f4683-f0fb-4c3e-adb2-a548958d9a88&autoAuth=true&ctid=c64a677e-65fb-4132-97c8-fe42014abd7c&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLWJyYXppbC1zb3V0aC1yZWRpcmVjdC5hbmFseXNpcy53aW5kb3dzLm5ldC8ifQ%3D%3D', 'Bora Brasil', '1');
