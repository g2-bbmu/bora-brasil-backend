/**
 * change the xlsx filepath and
 * run this script with this command
 *
 * yarn ts-node -r tsconfig-paths/register ./src/scripts/load-database.script.ts
 */
/*
import * as dotenv from 'dotenv';
dotenv.config();

import { createConnection } from 'typeorm';
import Config from '../libs/config';

import moment from 'moment';
import path from 'path';
import xlsx from 'xlsx';

import BoraUsuarios from '../../config/BoraUsuario_202008141302.json';
import BoraPremotoristas from '../../config/BoraPremotorista_202008141302.json';
import BoraFranqueados from '../../config/BoraFranqueado_202008141303.json';
import { Usuario, Perfil } from '../entities/usuario';
import { Franqueado } from '../entities/franqueado';
import { Status } from '../entities/franqueado';
import { PreMotorista } from '../entities/pre-motorista';
import { Driver } from '../entities/driver';

interface BoraUsuario {
    Idusuario: number;
    Usuario: string;
    Cnpj1: string; //''
    Login: string;
    Perfil: 'Administrador' | 'Motorista' | 'Financeiro' | '';
    Ativo: 0 | 1;
    Senha: string;
    Status: 'Ativo' | 'Pendente' | 'Cancelado' | 'Bloqueado' | '';
    Obs: string;
    NumeroAcesso: number;
    Acesso: 0 | 1;
    Data_ativacao: Date;
}

interface BoraPremotorista {
    Idpremotorista: number;
    Nome: string;
    Contato_movel: string; // '(11) 9855-51571',
    Email: string;
    Status: 'Ativo' | 'Pendente' | 'Cancelado' | 'Bloqueado';
    Data_cadastro: Date;
    Data_ativacao: Date;
    Cpf: string; //'0';
    Indicador: string;
    Cnpj1: string;
    Nivel1: string;
    Nivel2: string;
    Nivel3: string;
    Nivel4: string;
    Nivel5: string;
}

interface BoraFranqueado {
    Idfranqueado: 1;
    Razao_social: string;
    Nome_fantasia: string;
    Cnpj: string;
    Inscricao_estadual: string;
    Contato_fixo: string;
    Contato_movel: string;
    Responsavel: string;
    Email: string;
    Cep: string;
    Endereco: string;
    Numero: string;
    Estado: string;
    Uf: string;
    Cidade: string;
    Ativo: 0 | 1;
    Data_cadastro: Date;
    Status: 'Ativo' | 'Pendente' | 'Cancelado' | 'Bloqueado';
    Obs: string;
    Cnpj1: string;
    Ndomicilio: string;
    Banco: string;
    Agencia: string;
    Digag: string;
    Conta: string;
    Digconta: string;
}

const connectToDatabase = async () => {
    const dbName = process.env.NODE_ENV == 'docker' ? 'taxi_docker' : config.settings.mysql.database;
    await createConnection({
        type: 'mysql',
        host: process.env.NODE_ENV == 'docker' ? 'mysql' : config.settings.mysql.host,
        port: process.env.NODE_ENV == 'docker' ? 3306 : config.settings.mysql.port,
        username: process.env.NODE_ENV == 'docker' ? 'root' : config.settings.mysql.user,
        password: process.env.NODE_ENV == 'docker' ? 'defaultpassword' : config.settings.mysql.password,
        database: dbName,
        synchronize: true,
        migrationsRun: false,
        legacySpatialSupport: false,
        entities: ['./src/entities/*.ts', `${__dirname}/entities/*.js`],
        migrations: ['./src/migration/*.ts', `${__dirname}/migration/*.js`],
        subscribers: ['./src/subscribers/*.ts', `${__dirname}/subscribers/*.js`],
    });
};

const transformToNumericalString = (value: string) => {
    return value?.replace(/[^\d]+/g, '') || '';
};

const convertDate = (value: string): Date => {
    const r = moment(value, 'DD/MM/YYYY HH:MM:SS');

    if (r.isValid()) {
        return r.toDate();
    }
    return new Date();
};

const convertMobileNumber = (value: string): string => {
    if (!value) return null;

    const number = parseInt(transformToNumericalString(value), 10);
    const n2 = '55' + number.toString();
    const result = parseInt(n2, 10);

    if (Number.isNaN(result)) throw new Error(`Invalid Mobile Number: ${value}`);

    return String(result);
};

const convertStatus = (value: string): Status | null => {
    switch (value) {
        case Status.Ativo:
            return Status.Ativo;
        case Status.Bloqueado:
            return Status.Bloqueado;
        case Status.Cancelado:
            return Status.Cancelado;
        case Status.Pendente:
            return Status.Pendente;
        default:
            return Status.Pendente;
    }
};

const capitalizeEach = (value: string) => {
    return value
        .toLowerCase()
        .split(' ')
        .map((s) => s.charAt(0).toUpperCase() + s.slice(1))
        .join(' ');
};

const convertUsuario = (boraUsuario: BoraUsuario): Partial<Usuario> => {
    return {
        id: Number(boraUsuario.Idusuario),
        nomeCompleto: capitalizeEach(boraUsuario.Usuario).trim(),
        cnpj: boraUsuario.Cnpj1 === '' ? null : boraUsuario.Cnpj1.trim(),
        login: boraUsuario.Login.toLowerCase().trim(),
        perfil:
            boraUsuario.Perfil === ''
                ? null
                : boraUsuario.Perfil === 'Administrador'
                ? Perfil.Administrador
                : boraUsuario.Perfil === 'Financeiro'
                ? Perfil.Financeiro
                : boraUsuario.Perfil === 'Motorista'
                ? Perfil.Motorista
                : null,
        ativo: boraUsuario.Ativo === 1,
        senha: boraUsuario.Senha,
        status: convertStatus(boraUsuario.Status),
        obs: boraUsuario.Obs ? boraUsuario.Obs.trim() : null,
        numeroAcesso: boraUsuario.NumeroAcesso === 1,
        acesso: boraUsuario.Acesso === 1,
        dataAtivacao: moment(boraUsuario.Data_ativacao).toDate(),
    };
};

const convertPremotorista = (boraPremotorista: BoraPremotorista): Partial<PreMotorista> => {
    return {
        id: Number(boraPremotorista.Idpremotorista),
        nomeCompleto: capitalizeEach(boraPremotorista.Nome).trim(),
        contatoMovel: convertMobileNumber(boraPremotorista.Contato_movel) || null,
        email: boraPremotorista.Email === '0' ? null : boraPremotorista.Email.toLowerCase(),
        status: convertStatus(boraPremotorista.Status),
        dataCadastro: moment(boraPremotorista.Data_cadastro, 'YYYY-MM-DD').toDate(),
        dataAtivacao: moment(boraPremotorista.Data_ativacao, 'YYYY-MM-DD').toDate(),
        cpf: boraPremotorista.Cpf === '0' ? null : boraPremotorista.Cpf || null,
        indicador: boraPremotorista.Indicador === '0' ? null : Number(boraPremotorista.Indicador),
        cnpj: boraPremotorista.Cnpj1,
        nivel1: boraPremotorista.Nivel1 === '0' ? null : Number(boraPremotorista.Nivel1),
        nivel2: boraPremotorista.Nivel2 === '0' ? null : Number(boraPremotorista.Nivel2),
        nivel3: boraPremotorista.Nivel3 === '0' ? null : Number(boraPremotorista.Nivel3),
        nivel4: boraPremotorista.Nivel4 === '0' ? null : Number(boraPremotorista.Nivel4),
        nivel5: boraPremotorista.Nivel5 === '0' ? null : Number(boraPremotorista.Nivel5),
    };
};

const convertFranqueado = (boraFranqueados: BoraFranqueado): Partial<Franqueado> => {
    return {
        id: Number(boraFranqueados.Idfranqueado),
        razaoSocial: boraFranqueados.Razao_social.toUpperCase(),
        nomeFantasia: boraFranqueados.Nome_fantasia.toUpperCase(),
        cnpj: boraFranqueados.Cnpj,
        inscricaoEstadual: boraFranqueados.Inscricao_estadual || null,
        contatoFixo: convertMobileNumber(boraFranqueados.Contato_fixo) || null,
        contatoMovel: convertMobileNumber(boraFranqueados.Contato_movel) || null,
        responsavel: capitalizeEach(boraFranqueados.Responsavel),
        email: boraFranqueados.Email.toLowerCase(),
        cep: transformToNumericalString(boraFranqueados.Cep),
        endereco: boraFranqueados.Endereco ? capitalizeEach(boraFranqueados.Endereco) : null,
        numero: boraFranqueados.Numero ? String(boraFranqueados.Numero).toUpperCase() : null,
        estado: boraFranqueados.Estado ? capitalizeEach(boraFranqueados.Estado) : null,
        uf: boraFranqueados.Uf?.toUpperCase() || null,
        cidade: boraFranqueados.Cidade ? capitalizeEach(boraFranqueados.Cidade) : null,
        ativo: boraFranqueados.Ativo === 1,
        dataCadastro: moment(boraFranqueados.Data_cadastro).toDate(),
        status: convertStatus(boraFranqueados.Status),
        obs: boraFranqueados.Obs || null,
        nDomicilio: boraFranqueados.Ndomicilio || null,
        banco: boraFranqueados.Banco?.toUpperCase() || null,
        agencia: boraFranqueados.Agencia ?? null,
        digitoAgencia: boraFranqueados.Digag ?? null,
        conta: boraFranqueados.Conta ?? null,
        digitoConta: boraFranqueados.Digconta ?? null,
    };
};

const run = async () => {
    await connectToDatabase();
    const workbook = xlsx.readFile(path.resolve(`${process.cwd()}/config/BoraFranqueado-atualizada.xlsx`), {
        type: 'binary',
        cellDates: true,
        cellNF: false,
        cellText: false,
    });

    const boraFranqueadoAtualizado = xlsx.utils.sheet_to_json<BoraFranqueado>(workbook.Sheets['Atualizada Sheila']);
    const boraUsuarios = (BoraUsuarios as any).BoraUsuario as BoraUsuario[];
    const boraPremotoristas = (BoraPremotoristas as any).BoraPremotorista as BoraPremotorista[];
    const boraFranqueado = (BoraFranqueados as any).BoraFranqueado as BoraFranqueado[];

    const convertedFranqueados = boraFranqueadoAtualizado.map(convertFranqueado);
    const convertedUsuarios = boraUsuarios.map(convertUsuario);
    const convertedPremotoristas = boraPremotoristas.map(convertPremotorista);

    // // console.log(convertedUsuarios.map((a) => Usuario.create(a)));

    // await Franqueado.save(convertedFranqueados.map((a) => Franqueado.create(a)));
    // await Usuario.insert(convertedUsuarios.map((a) => Usuario.create(a)));

    const fillUsuarios = [
        13541,
        17723,
        20285,
        20831,
        2103,
        21104,
        2175,
        2203,
        22078,
        2299,
        2308,
        238,
        2420,
        24495,
        2476,
        257,
        30981,
        3396,
        3766,
        3781,
        4639,
        498,
        531,
        5346,
        5494,
        5622,
        5724,
        590,
        591,
        6123,
        6361,
        6493,
        7060,
        7099,
        7255,
        7298,
        7351,
        7388,
        7678,
        7802,
        7873,
        7954,
        80,
        8010,
        8030,
        8246,
        8376,
        8581,
        8900,
        9522,
        9601,
        9921,
        9943,
        6290,
        3482,
        13606,
        3082,
        13001,
        6639,
        5569,
        2893,
        9599,
        2730,
        9639,
        9600,
        565,
        6878,
        9638,
        276,
        2188,
        9637,
        5639,
        331,
        9597,
        9598,
        4937,
        826,
        3007,
        2060,
        11958,
    ].map((n) =>
        Usuario.create({
            id: n,
            acesso: true,
            ativo: true,
            cnpj: '34.082.275/0001-33',
            dataAtivacao: new Date(),
            login: 'preenchido@usuario.com',
            nomeCompleto: 'Usuário Preenchido',
            numeroAcesso: true,
            perfil: Perfil.Motorista,
            senha: 'e10adc3949ba59abbe56e057f20f883e'.toUpperCase(),
            status: Status.Pendente,
        })
    );

    await Usuario.insert(fillUsuarios);
    await PreMotorista.insert(convertedPremotoristas.map((a) => PreMotorista.create(a)));

    process.exit(0);
};

const config = new Config();
config.init().then(run);
*/