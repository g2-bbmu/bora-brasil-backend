-- SELECT *, substring_index(substring_index(address, '/', 2), '/', -1) as newType FROM BoraBrasil.media where substring_index(substring_index(address, '/', 2), '/', -1) != type order by id desc;

UPDATE BoraBrasil.media SET type = substring_index(substring_index(address, '/', 2), '/', -1) where substring_index(substring_index(address, '/', 2), '/', -1) != type and id > 0;