const dotenv = require('dotenv') ;

dotenv.config();

const BASE_DIR = process.env.NODE_ENV === 'production' ? 'dist' : 'src';

module.exports = [
    {
        name: 'default',
        type: 'mysql',
        host: process.env.DB_HOST,
        port: 3306,
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME,
        logging: true,
        synchronize: false,
        migrationsRun: false,
        dropSchema: false,
        entities: [`${BASE_DIR}/entities/*.{ts,js}`],
        migrations: [`${BASE_DIR}/migration/*.{ts,js}`],
        subscribers: [`${BASE_DIR}/subscribers/*.{ts,js}`],
        cli: {
            migrationsDir: 'src/migration',
            subscribersDir: 'src/subscribers',
        },
    },
];
