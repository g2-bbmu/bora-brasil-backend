FROM ridyio/taxi
WORKDIR /app

COPY ./backend/dist/taxi-backend .
COPY ./cms/dist public

# docker build -t gcr.io/bora-brasil-g2/taxi:latest . && docker push gcr.io/bora-brasil-g2/taxi:latest
