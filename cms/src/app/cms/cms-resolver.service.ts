import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Observable, throwError} from "rxjs";

export interface CmsResolverData {
    hasPanicButtonPermission: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class CmsResolverService implements Resolve<CmsResolverData> {

    constructor() {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CmsResolverData> | Promise<CmsResolverData> | CmsResolverData {
        return new Observable((observer) => {
            const user = JSON.parse(localStorage.getItem('user'));

            const response = {
                hasPanicButtonPermission: false,
            };

            if (!user) {
                observer.next(response);
                observer.complete();
                return;
            }

            response.hasPanicButtonPermission = !!user.onlyPanicButton

            if (response.hasPanicButtonPermission && window.location.pathname !== '/panic') {
                window.location.href = '/panic';
                throwError('panic redirection');
                return;
            }

            observer.next(response);
            observer.complete();
        });
    }
}
