import { TestBed } from '@angular/core/testing';

import { CmsResolverService } from './cms-resolver.service';

describe('CmsResolverService', () => {
  let service: CmsResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CmsResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
