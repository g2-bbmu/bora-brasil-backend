import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverProfilePaymentGatewayComponent } from './driver-profile-payment-gateway.component';

describe('DriverProfilePaymentGatewayComponent', () => {
  let component: DriverProfilePaymentGatewayComponent;
  let fixture: ComponentFixture<DriverProfilePaymentGatewayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverProfilePaymentGatewayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverProfilePaymentGatewayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
