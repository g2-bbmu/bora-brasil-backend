import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Driver} from "../../../../../@models/entities/driver";
import {Media} from "../../../../../@models/entities/media";
import {BackendService} from "../../../../../@services/backend/backend.service";

@Component({
  selector: 'app-driver-profile-payment-gateway',
  templateUrl: './driver-profile-payment-gateway.component.html',
  styleUrls: ['./driver-profile-payment-gateway.component.css']
})
export class DriverProfilePaymentGatewayComponent implements OnInit {
  driver: Driver;
  constructor(private route: ActivatedRoute, private backend: BackendService) { }

  async ngOnInit(): Promise<void> {
    const driverId = parseInt(this.route.parent.snapshot.paramMap.get('id'));
    this.driver = (await this.backend.getRows<Driver>({table: 'Driver', filters: {id: driverId}})).data[0]
  }

}
