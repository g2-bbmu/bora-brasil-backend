import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DriverWallet } from "src/app/@models/entities/driver-wallet";
import { BackendService } from "src/app/@services/backend/backend.service";
import { NzMessageService } from "ng-zorro-antd";
import { TagColorService } from "src/app/@services/tag-color/tag-color.service";
import { environment } from "src/environments/environment";
import { Media } from "src/app/@models/entities/media";
import {Franqueado} from "../../../../../../../backend/src/entities/franqueado";
import { camelCase } from "camel-case";
import { HttpService } from "../../../../@services/http/http.service";
import {
  Driver,
  DriverStatus,
  PaymentGatewayDocumentsStatus,
} from "../../../../@models/entities/driver";
import { PreMotorista } from "../../../../../../../backend/src/entities/pre-motorista";
import { Usuario } from "../../../../../../../backend/src/entities/usuario";
import { Net } from 'src/app/@models/entities/net';
import { SysLogAlteracaoDriver } from 'src/app/@models/entities/sys-log-alteracao-driver';

@Component({
  selector: "app-driver-profile",
  templateUrl: "./driver-profile.component.html",
  styleUrls: ["./driver-profile.component.css"],
})
export class DriverProfileComponent implements OnInit {
  driver: Driver;
  preMotorista: PreMotorista;
  topWallet: DriverWallet;
  emailIndicador: string;
  franqueados: Franqueado[];
  tagColor: TagColorService;
  operador: any;
  environment: any;
  isLoading: boolean;
  documentosEmAprovacao: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private _tagColor: TagColorService,
    private backend: BackendService,
    private message: NzMessageService,
    private http: HttpService,
  ) {
    this.tagColor = _tagColor;
    this.environment = environment;
  }

  refreshPage() {
    window.location.reload();
  }

  ngOnInit(): void {
    this.operador = JSON.parse(localStorage.getItem('user'));

    console.log(this.operador);
    this.route.data.subscribe(async (x) => {
      this.driver = x.item;
      let franquias = await this.backend.getRows<Franqueado>({
        table: "franqueado",
      });
      this.franqueados = franquias.data;
      let wallets = await this.backend.getRows<DriverWallet>({
        table: "DriverWallet",
        filters: { driver: { id: x.item.id } },
      });
      this.topWallet = wallets.data.sort((a, b) => {
        return b.amount - a.amount;
      })[0];

      if (this.driver.media != null) {
        let image = await this.backend.getRows<Media>(
          { table: "Media", filters: { id: this.driver.media } },
        );
        this.driver.media = image.data[0];
      } else {
        let driverMedia =
          (await this.backend.getRows<Media>(
            { table: "Media", filters: { id: this.driver.mediaId } },
          ));
        if (
          driverMedia != null && driverMedia.data != null &&
          driverMedia.data.length > 0
        ) {
          let data = driverMedia.data[0];
          this.driver.media = data;
        }
      }

      let emailDriver = this.driver.email;
      let patorcinadorId =
        (await this.backend.getRows<PreMotorista>(
          { table: "pre_motorista", filters: { email: emailDriver } },
        )).data[0].indicador;
      let patrocinador =
        (await this.backend.getRows<Usuario>(
          { table: "usuario", filters: { id: patorcinadorId } },
        )).data[0];
      this.driver.patrocinador = patrocinador.nomeCompleto;
    });
  }

  toCamelCase(value: string) {
    return camelCase(value);
  }

  async sendDocuments(driverId: number) {
    this.isLoading = true;

    this.http.sendDriverDocumentsToPaymentGateway(driverId)
      .then(() => {
        this.driver.paymentGatewayDocumentsStatus =
          PaymentGatewayDocumentsStatus.EmAnalise;
        this.message.create(
          "success",
          "Os documentos foram enviados com sucesso!",
        );
      }).catch(() => {
        this.message.create(
          "error",
          "Ocorreu um erro ao enviar os documentos. Tente novamente mais tarde.",
        );
      }).finally(() => {
        this.isLoading = false;
      });

      let hoje = new Date();
       let log = Object.create(SysLogAlteracaoDriver);
       log.idOperator = this.operador.id;
       log.idDriver = this.driver.id;
       log.data_alteracao = hoje;
       log.tabela = 'Driver';
       log.campo = 'paymentGatewayDocumentsStatus';
       log.valor_atual = 'Enviou documentos para zoop';
       await this.backend.saveRow('log_alteracao_Driver', log, false); 
  }

  async changeStatus(status: string) {
    let statusAnterior = this.driver.status;
    
    await this.backend.saveRow(
      "Driver",
      { id: this.driver.id, status: status },
    );
    this.driver.status = status as DriverStatus;

    let hoje = new Date();
       let log = Object.create(SysLogAlteracaoDriver);
       log.idOperator = this.operador.id;
       log.idDriver = this.driver.id;
       log.data_alteracao = hoje;
       log.tabela = 'Driver';
       log.campo = 'status';
       log.valor_anterior = statusAnterior;
       log.valor_atual = this.driver.status;
       await this.backend.saveRow('log_alteracao_Driver', log, false); 
  }

  async changePaymentGatewayDocumentsStatus(
    paymentGatewayDocumentsStatus: string,
  ) {
    await this.backend.saveRow(
      "Driver",
      {
        id: this.driver.id,
        paymentGatewayDocumentsStatus: paymentGatewayDocumentsStatus,
      },
    );
    this.driver.paymentGatewayDocumentsStatus =
      paymentGatewayDocumentsStatus as PaymentGatewayDocumentsStatus;
    this.refreshPage();
  }

  async alterarFranquia(cnpjFranquia: string,nomeFranquia: string)  
  {
    if(confirm("Deseja realmente alterar a franquia do motorista para : "+nomeFranquia)) {
    let cnpjFranquiaAntes = this.driver.cnpjFranquia;

    let result = await this.backend.saveRow(
      "driver",
      {
        id: this.driver.id,
        cnpjFranquia: cnpjFranquia,
        nomeFranquia: nomeFranquia,
      },
       );

       if(!result){
        this.message.error('Ocorreu algum erro ao tentar alterar a franquia do motorista');
        return null;
      }

    this.driver.cnpjFranquia = cnpjFranquia;
    this.driver.nomeFranquia = nomeFranquia;

       let hoje = new Date();
       let log = Object.create(SysLogAlteracaoDriver);
       log.idOperator = this.operador.id;
       log.idDriver = this.driver.id;
       log.data_alteracao = hoje;
       log.tabela = 'Driver';
       log.campo = 'cnpjFranquia';
       log.valor_anterior = cnpjFranquiaAntes;
       log.valor_atual = cnpjFranquia;
       await this.backend.saveRow('log_alteracao_Driver', log, false);
    }
  }

  async alterarIndicador(indicador: string)
  {
    let emailDriver = this.driver.email;

    let patrocinador =(await this.backend.getRows<Usuario>(
          { table: "usuario", filters: { login: indicador } },
        )).data[0];

        if(!patrocinador){
          this.message.error('Não foi possivel encontrar o usuário indicador');
          return null;
        }

    let driverUsuario =
        ( await this.backend.getRows<Usuario>(
            { table: "usuario", filters: { login : emailDriver }},
        )).data[0];

    let driverPreMotorista =
        (await this.backend.getRows<PreMotorista>(
          { table: "pre_motorista", filters: { id_tbl_usuario: driverUsuario.id } },
        )).data[0];

    let patrocinadorAnteriorId = driverPreMotorista.indicador;

     let redeDriver =
    (await this.backend.getRows<Net>(
      { table: "Net", filters: { up : driverUsuario.id, level : !0 } },
    )).count;

    /*if(redeDriver != 0){
      this.message.error('Não foi possível alterar pois esse motorista já indicou outros motoristas.');
      return null;
    } */

    if(confirm("O motorista "+ this.driver.firstName +" possui "+ redeDriver +" motoristas em sua rede. Deseja realmente alterar o indicador para "+ patrocinador.nomeCompleto +"?")) {
      let result = await this.backend.saveRow(
        "pre_motorista",
        {
          id : driverPreMotorista.id,
          indicador: patrocinador.id,
          nivel1: patrocinador.id,
        },
         );
         if(!result){
          this.message.error('Ocorreu algum erro ao tentar alterar o indicador');
          return null;
        }

      this.driver.patrocinador = patrocinador.nomeCompleto;

      let hoje = new Date();
    let log = Object.create(SysLogAlteracaoDriver);
    log.idOperator = this.operador.id;
    log.idDriver = this.driver.id;
    log.data_alteracao = hoje;
    log.tabela = 'Driver';
    log.campo = 'Indicador';
    log.valor_anterior = patrocinadorAnteriorId.toString();
    log.valor_atual = patrocinador.id.toString();
    await this.backend.saveRow('log_alteracao_Driver', log, false);
    }

    //this.refreshPage();
  }
}
