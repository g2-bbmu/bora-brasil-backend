import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendService} from 'src/app/@services/backend/backend.service';
import {Service} from 'src/app/@models/entities/service';
import {Car} from 'src/app/@models/entities/car';
import {Driver} from 'src/app/@models/entities/driver';
import {Fleet} from 'src/app/@models/entities/fleet';
import {environment} from 'src/environments/environment';
import {Media} from 'src/app/@models/entities/media';
import {Bank} from "../../../../../../../../backend/src/entities/bank";
import {formatDate} from "@angular/common";
import {NzMessageService} from "ng-zorro-antd";
import { SysLogAlteracaoDriver } from 'src/app/@models/entities/sys-log-alteracao-driver';

@Component({
  selector: "app-driver-profile-details",
  templateUrl: "./driver-profile-details.component.html",
  styleUrls: ["./driver-profile-details.component.css"],
})
export class DriverProfileDetailsComponent implements OnInit {
  form = new FormGroup({});
    currentTimestamp: number;
    model: Driver;
    log: SysLogAlteracaoDriver;
  services: any[] = [];
  operador: any;
  banks: Bank[];
  root: string;
  showUploadList = {
    showRemoveIcon: false,
  };
  fileList = [];
  fields: FormlyFieldConfig[] = [
    {
      type: "flex-layout",
      templateOptions: {
        fxLayout: "column",
      },
      fieldGroup: [
        {
          type: "flex-layout",
          templateOptions: {
            fxLayout: "row",
            title: "Informações Gerais",
          },
          fieldGroup: [
            {
              key: "cpf",
              type: "input",
              templateOptions: {
                label: "CPF",
                placeholder: "CPF",
                required: true,
              },
            },
            {
              key: "firstName",
              type: "input",
              templateOptions: {
                label: "Nome",
                placeholder: "Nome",
                required: true,
              },
            },
            {
              key: "lastName",
              type: "input",
              templateOptions: {
                label: "Sobrenome",
                placeholder: "Sobrenome",
                required: true,
              },
            },
            {
              key: "gender",
              type: "select",
              templateOptions: {
                label: "Genero",
                placeholder: "Genero",
                options: [
                  { label: "Desconhecido", value: "unknown" },
                  { label: "Feminino", value: "female" },
                  { label: "Masculino", value: "male" },
                ],
              },
            },
            {
              key: "mobileNumber",
              type: "input",
              templateOptions: {
                label: "Numero celular",
                placeholder: "Numero celular",
                required: true,
              },
            },
            {
              key: "cnhEar",
              type: "select",
              templateOptions: {
                label: "EAR",
                placeholder: "Selecione",
                options: [
                  { label: "Sim", value: true },
                  { label: "Não", value: false },
                ],
                required: true,
              },
            },
            {
              key: "cnhCategory",
              type: "select",
              templateOptions: {
                label: "Categoria CNH",
                placeholder: "Selecione",
                options: [
                  { label: "Categoria A", value: "Categoria A" },
                  { label: "Categoria B", value: "Categoria B" },
                  { label: "Categoria C", value: "Categoria C" },
                  { label: "Categoria D", value: "Categoria D" },
                  { label: "Categoria E", value: "Categoria E" },
                  { label: "Categoria AB", value: "Categoria AB" },
                  { label: "Categoria AC", value: "Categoria AC" },
                  { label: "Categoria AD", value: "Categoria AD" },
                  { label: "Categoria AE", value: "Categoria AE" },
                ],
                required: true,
              },
            },
          ],
        },
        {
          type: "flex-layout",
          templateOptions: {
            fxLayout: "row",
          },
          fieldGroup: [
            {
              key: "email",
              type: "input",
              className: "flex-1",
              templateOptions: {
                label: "E-mail",
                placeholder: "E-mail",
              },
            },
            {
              key: "certificateNumber",
              type: "input",
              templateOptions: {
                label: "Número CNH",
                placeholder: "000000000",
              },
            },
            {
              key: "cnhExpirationDate",
              type: "input",
              templateOptions: {
                label: "Data Validade CNH",
                placeholder: "DD/MM/AAAA",
                disabled: true,
              },
              hooks: {
                onInit: function () {
                  // var date = new Date(this.model.value).toLocaleDateString("en-us")
                  // this.model.value = date;
                },
              },
            },
          ],
        },
        {
          type: "flex-layout",
          templateOptions: {
            fxLayout: "row",
            title: "Informações do Automóvel",
          },
          fieldGroup: [
            {
              key: "fleet",
              type: "select",
              templateOptions: {
                label: "Dono da frota",
                options: [],
              },
            },
            {
              key: "carId",
              type: "select",
              templateOptions: {
                label: "Modelo do automóvel",
                placeholder: "Modelo do automóvel",
              },
            },
            {
              key: "carColor",
              type: "input",
              templateOptions: {
                label: "Cor do automóvel",
                placeholder: "Cor do automóvel",
              },
            },
            {
              key: "carProductionYear",
              type: "input",
              defaultValue: 2020,
              templateOptions: {
                label: "Ano",
                placeholder: "Ano",
                type: "number",
                min: 1900,
                max: 2050,
              },
            },
            {
              key: "carPlate",
              type: "input",
              templateOptions: {
                label: "Placa",
                placeholder: "Placa",
                maxLength: 15,
              },
            },
            {
              key: "carStatus",
              type: "select",
              templateOptions: {
                label: "Carro Próprio/Alugado",
                placeholder: "Sim ou Não",
                options: [
                  { label: "Próprio", value: true },
                  { label: "Alugado", value: false },
                ],
              },
            },
          ],
        },
        {
          type: "flex-layout",
          templateOptions: {
            fxLayout: "row",
            title: "Informações Bancárias",
          },
          fieldGroup: [
            {
              key: "bankAccount.holder",
              type: "input",
              templateOptions: {
                label: "Nome Completo",
                placeholder: "Nome Completo",
              },
            },
            {
              key: "bankAccount.cpfCnpj",
              type: "input",
              templateOptions: {
                label: "CPF / CNPJ",
                placeholder: "Informe o CPF/CNPJ",
              },
            },
            {
              key: "bankAccount.bankCode",
              type: "select",
              templateOptions: {
                label: "Banco",
                placeholder: "Banco",
              },
            },
            {
              key: "bankAccount.agencyCode",
              type: "input",
              templateOptions: {
                label: "Numero da agência",
                placeholder: "Numero da agência",
              },
            },
            {
              key: "bankAccount.accountNumber",
              type: "input",
              templateOptions: {
                label: "Numero da conta",
                placeholder: "Numero da conta",
              },
            },
            {
              key: "bankAccount.type",
              type: "select",
              templateOptions: {
                label: "Tipo da Conta",
                placeholder: "Tipo da Conta",
                options: [
                  { label: "Conta Poupança", value: "savings" },
                  { label: "Conta Corrente", value: "checking" },
                ],
              },
            },
          ],
        },
        {
          type: "flex-layout",
          templateOptions: {
            fxLayout: "row",
            title: "Informações de Endereço",
          },
          fieldGroup: [
            {
              key: "cep",
              type: "input",
              templateOptions: {
                label: "CEP",
                placeholder: "CEP",
              },
            },
            {
              key: "address",
              type: "input",
              templateOptions: {
                label: "Logradouro",
                placeholder: "Logradouro",
              },
            },
            {
              key: "number",
              type: "input",
              templateOptions: {
                label: "Número",
                placeholder: "Número",
              },
            },
            {
              key: "addressTwo",
              type: "input",
              templateOptions: {
                label: "Complemento",
                placeholder: "Complemento",
              },
            },
            {
              key: "city",
              type: "input",
              templateOptions: {
                label: "Cidade",
                placeholder: "Cidade",
              },
            },
            {
              key: "state",
              type: "select",
              templateOptions: {
                label: "Estado",
                placeholder: "Estado",
                options: [
                  { label: "Acre", value: "AC" },
                  { label: "Alagoas", value: "AL" },
                  { label: "Amapá", value: "AP" },
                  { label: "Amazonas", value: "AM" },
                  { label: "Bahia", value: "BA" },
                  { label: "Ceará", value: "CE" },
                  { label: "Distrito Federal", value: "DF" },
                  { label: "Espirito Santo", value: "ES" },
                  { label: "Goiás", value: "GO" },
                  { label: "Maranhão", value: "MA" },
                  { label: "Mato Grosso do Sul", value: "MS" },
                  { label: "Mato Grosso", value: "MT" },
                  { label: "Minas Gerais", value: "MG" },
                  { label: "Pará", value: "PA" },
                  { label: "Paraíba", value: "PB" },
                  { label: "Paraná", value: "PR" },
                  { label: "Pernambuco", value: "PE" },
                  { label: "Piauí", value: "PI" },
                  { label: "Rio de Janeiro", value: "RJ" },
                  { label: "Rio Grande do Norte", value: "RN" },
                  { label: "Rio Grande do Sul", value: "RS" },
                  { label: "Rondônia", value: "RO" },
                  { label: "Roraima", value: "RR" },
                  { label: "Santa Catarina", value: "SC" },
                  { label: "São Paulo", value: "SP" },
                  { label: "Sergipe", value: "SE" },
                  { label: "Tocantins", value: "TO" },
                ],
              },
            },
          ],
        },
      ],
    },
  ];
  avatarUrl = null;
  constructor(
    private route: ActivatedRoute,
    private backend: BackendService,
    private message: NzMessageService,
    private router: Router
  ) {
    this.root = environment.root;
  }

  ngOnInit(): void {
    this.route.data.subscribe(async (x) => {
      this.model = x.item;
      if (x.item.mediaId != null) {
        let media = (
          await this.backend.getRows<Media>({
            table: "Media",
            filters: { id: x.item.mediaId },
          })
        ).data[0];
        this.avatarUrl = makeAvatarPathFromMedia(media);
      }

      this.fileList = this.fileList.slice();
      this.backend
        .getRows<Car>({ table: "Car" })
        .then((y) => {
          this.backend.getField(
            "carId",
            this.fields
          ).templateOptions.options = y.data.map((z) => {
            return { label: z.title, value: z.id };
          });
        });
      this.backend
        .getRows<Fleet>({ table: "Fleet" })
        .then((y) => {
          this.backend.getField(
            "fleet",
            this.fields
          ).templateOptions.options = y.data.map((z) => {
            return { label: z.name, value: z.id };
          });
        });
      this.backend
        .getRows<Bank>({ table: "Bank" })
        .then((y) => {
          this.banks = y.data;
          this.backend.getField(
            "bankAccount.bankCode",
            this.fields
          ).templateOptions.options = this.banks.map((z) => {
            return { label: z.value + " - " + z.label, value: z.id };
          });
          this.form
            .get("bankAccount.bankCode")
            .setValue(
              this.banks.filter(
                (z) => z.value == this.model.bankAccount?.bankCode
              )[0].id
            );
        });

      this.backend
        .getRows<Service>({ table: "Service" })
        .then((y) => {
          this.services = y.data.map((z) => {
            return {
              key: z.id,
              title: z.title,
              direction:
                this.model.services == null ||
                  this.model.services.filter((i) => i.id == z.id).length < 1
                  ? "left"
                  : "right",
            };
          });
        });
    });
  }

  ngAfterViewInit() {
    /* Fixes */
    /* Data CNH */
    const timestamp = this.model.cnhExpirationDate;
    this.currentTimestamp = timestamp;
    const date = new Date(timestamp);
    let formattedDate = formatDate(date, "dd/MM/yyyy", "en", "+0000");
    this.form.get("cnhExpirationDate").setValue(formattedDate);

    var veiculoAlugado = this.model.carStatus;
    this.form.get("carStatus").setValue(veiculoAlugado);

    var bankAccountType = this.model.bankAccount.type;
    this.form.get("bankAccount.type").setValue(bankAccountType);

    var state = this.model.state;
    this.form.get("state").setValue(state);
  }

  async onSubmit() {
    this.operador = JSON.parse(localStorage.getItem("user"));
    if (this.operador.cnpjFranquia != "000") {
      if (
        this.model.paymentGatewayDocumentsStatus == "Em Análise" ||
        this.model.paymentGatewayDocumentsStatus == "Aprovado Manualmente"
      ) {
        alert(
          "Não é possivel alterar os dados de um motorista cujo os documentos estão em analise ou já aprovado."
        );
        return null;
      }
    }


    // Handle data before send
    this.model.cnhExpirationDate = this.currentTimestamp; // ref: 1641006000000
    this.model.bankAccount.cpfCnpj = this.form.get("bankAccount.cpfCnpj").value;
    
    const bankCode = this.banks.filter(
      (z) => z.id == this.form.get("bankAccount.bankCode").value
    )[0];

    if (!bankCode) {
      return this.message.error('Código do banco selecionado é inválido');
    }

    this.model.bankAccount.bankCode = bankCode.value;

    this.model.services = this.services
      .filter((x) => x.direction == "right")
      .map((x) => {
        return { id: x.key };
      });

    await this.backend.saveRow("BankAccount", this.model.bankAccount, false);
    let res: any = await this.backend.saveRow(
      "Driver",
      this.model,
      true,
      this.fields,
      ["services", "media", "bankAccount"]
    );
    if (this.model.id == null) {
      this.router.navigate(["../view", res.id], { relativeTo: this.route });
    }

    let hoje = new Date();
    let log = Object.create(SysLogAlteracaoDriver);
    
    log.idOperator = this.operador.id;
    log.idDriver = this.model.id;
    log.data_alteracao = hoje;
    log.tabela = 'Driver';
    log.campo = 'Cadastro-Geral';
    
    let logAlteracao: any = await this.backend.saveRow('log_alteracao_Driver', log, false);

    console.log(logAlteracao);
  }

  uploaded(event: any) {
    if (event.type == "success") {
      this.model.media = event.file.response;
      this.avatarUrl = makeAvatarPathFromMedia(this.model.media);
    } else {
      this.model.media = null;
    }
  }

  public customPatterns = { "0": { pattern: new RegExp("[a-zA-Z]") } };
}

const maskDate = (value, status) => {
  let v = value.replace(/\D/g, "").slice(0, 10);

  if (status && v.length == 8) {
    return `${v.slice(0, 2)}/${v.slice(2, 4)}/${v.slice(4)}`;
  }
  if (status && v.length == 7) {
    return "0" + `${v.slice(0, 1)}/${v.slice(1, 3)}/${v.slice(3)}`;
  }

  return null;
};

function makeAvatarPathFromMedia(media: Media): any {
  return environment.root + media.address;
}
