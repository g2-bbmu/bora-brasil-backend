import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
    selector: 'formly-field-input',
    template: `
   <input type="input" 
        [formControl]="formControl" 
        [formlyAttributes]="field"
        mask="d0/M0/0000">`,
})
export class FormlyFieldInput extends FieldType {}

