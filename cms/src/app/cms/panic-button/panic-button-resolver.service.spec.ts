import { TestBed } from '@angular/core/testing';

import { PanicButtonResolverService } from './panic-button-resolver.service';

describe('PanicButtonResolverService', () => {
  let service: PanicButtonResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PanicButtonResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
