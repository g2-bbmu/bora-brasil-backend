import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanicButtonComponent } from './panic-button.component';
import {PanicButtonResolverService} from './panic-button-resolver.service';


const routes: Routes = [
  { path: '', component: PanicButtonComponent, resolve: { panicButtonData: PanicButtonResolverService }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanicButtonRoutingModule { }
