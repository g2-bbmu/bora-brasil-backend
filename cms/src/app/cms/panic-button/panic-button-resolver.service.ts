import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Occurrence} from './panic-button.component';
import {SocketService} from "../../@services/socket/socket.service";
import {NzMessageService} from "ng-zorro-antd";
import CMSException from "../../@models/cms-exception";

export interface PanicButtonData {
    watchingOccurrences: { [key: number]: Occurrence };
    occurrencesPendingAccept: Occurrence[];
}

@Injectable({
    providedIn: 'root'
})
export class PanicButtonResolverService implements Resolve<PanicButtonData> {

    constructor(
        private readonly socket: SocketService,
        private message: NzMessageService
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PanicButtonData> | Promise<PanicButtonData> | PanicButtonData {
        const occurrencesPendingAccept: Occurrence[] = []
        const watchingOccurrences: { [key: number]: Occurrence } = {}

        return new Promise<PanicButtonData>((resolve, reject) => {
            this.socket.emit('getNotFinishedOccurrences', null, (result: Occurrence[], err?: CMSException) => {
                if (err) {
                    if (err.message) {
                        this.message.error(err.message);
                    }
                    return;
                }

                occurrencesPendingAccept.push(...result.filter(r => r.acceptedBy === null))
                const watchingOccurrencesMap = result.reduce((a, r) => {
                    a[r.driver.id] = r
                    return a;
                }, {})

                Object.assign(watchingOccurrences, watchingOccurrencesMap);

                resolve({
                    occurrencesPendingAccept,
                    watchingOccurrences
                })
            })
        })
    }
}
