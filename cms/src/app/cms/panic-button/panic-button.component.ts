import {
  Component,
  Inject,
  LOCALE_ID,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { SocketService } from "src/app/@services/socket/socket.service";
import { Observable, Subscription } from "rxjs";
import { Stats } from "src/app/@models/stats";
import { DriverLocationWithId } from "src/app/@models/coordinatexy";
import { GoogleMap, MapInfoWindow } from "@angular/google-maps";
import { Driver } from "../../@models/entities/driver";
import { Operator } from "../../@models/entities/operator";
import {
  NzModalRef,
  NzModalService,
  NzNotificationService,
} from "ng-zorro-antd";
import { ActivatedRoute } from "@angular/router";
import { PanicButtonData } from "./panic-button-resolver.service";

export enum OccurrenceStatus {
  FalseAlarm = "False Alarm",
  SystemTest = "System Test",
  RealOccurrence = "Real Occurrence",
  Unknown = "Unknown",
}

export interface Occurrence {
  id: number;
  status: OccurrenceStatus;
  calledLocalAuthority: boolean;
  details: string;
  createdAt: Date;
  startDate: Date;
  finishedAt?: Date;
  acceptedBy?: Operator;
  driver: Driver;
}

@Component({
  selector: "app-panic-button",
  templateUrl: "./panic-button.component.html",
  styleUrls: ["./panic-button.component.css"],
})
export class PanicButtonComponent implements OnInit, OnDestroy {
  autoZoom = true;
  drivers: DriverLocationWithId[] = [];
  selectedDriverId: number;
  isInfoVisible: boolean;
  isLoading: boolean;
  @ViewChild(MapInfoWindow, { static: false })
  infoWindow: MapInfoWindow;
  @ViewChild(GoogleMap, { static: false })
  map: GoogleMap;
  incidentStatus = Object.values(OccurrenceStatus);
  soundAlert = new Audio(`../../../assets/ringtones/police-siren.mp3`);
  incidentAlerts = [];
  occurrencesPendingAccept = [];
  watchingOccurrences: { [key: number]: Occurrence } = {};
  currentFormValues = {} as any;
  driversLocations = {} as any;
  pendingOccurrencesNotificationsRef: { [occurrenceId: number]: NzModalRef } =
    {};
  subscriptions: Subscription[] = [];

  constructor(
    private socket: SocketService,
    @Inject(LOCALE_ID) private locale: string,
    private notification: NzNotificationService,
    private modal: NzModalService,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.isInfoVisible = false;
    this.soundAlert.loop = true;
    this.soundAlert.load();

    this.activatedRoute.data.subscribe(
      (data: { panicButtonData: PanicButtonData }) => {
        this.watchingOccurrences = data.panicButtonData.watchingOccurrences;
        this.occurrencesPendingAccept =
          data.panicButtonData.occurrencesPendingAccept;

        this.getDriversLocation();

        for (const pendingOccurrence of this.occurrencesPendingAccept) {
          this.displayNotificationToAcceptOccurrence(pendingOccurrence);
        }
      },
    );

    this.subscriptions.push(
      this.socket.fromEvent<Occurrence>("occurrenceCreated")
        .subscribe((occurrence) => {
          this.displayNotificationToAcceptOccurrence(occurrence);
          this.getDriversLocation();
        }),
    );

    this.subscriptions.push(
      this.socket.fromEvent("driverLocationUpdated").subscribe((x: any) => {
        if (!(x.id in this.watchingOccurrences)) {
          return;
        }
        const index = this.drivers.findIndex((y) => y.driverId === x.id);
        if (index < 0) {
          this.drivers.push({ driverId: x.id, location: x.loc });
          this.drivers = this.drivers.slice();
        } else {
          if (x.loc !== null) {
            this.drivers[index].location = x.loc;
          }
          this.drivers = this.drivers.slice();
        }
        if (this.autoZoom) {
          this.centerMap();
        }
      }),
    );

    this.subscriptions.push(
      this.socket.fromEvent("occurrenceAccepted")
        .subscribe(({ id, driver_id }: { id: number; driver_id: number }) => {
          if (this.pendingOccurrencesNotificationsRef[id]) {
            this.pendingOccurrencesNotificationsRef[id].close();
            delete this.pendingOccurrencesNotificationsRef[id];
            this.notification.create(
              "info",
              "Ocorrência aceita",
              "A ocorrência foi aceita por outro operador",
            );
            this.soundAlert.pause();
          }
        }),
    );

    this.subscriptions.push(
      this.socket.fromEvent("occurrenceFinished")
        .subscribe(({ id, driver_id }: { id: number; driver_id: number }) => {
          this.deleteDriverLocationByDriverId(driver_id);
        }),
    );
  }

  getTimeFormatForQuery(q: string) {
    switch (q) {
      case ("day"):
        return 'h"';
      case ("week"):
        return "W,y";
      case ("month"):
        return "M/d";
      case ("year"):
        return "MMM y";
    }
  }

  centerMap() {
    if (this.drivers.length === 0) {
      this.map.zoom = 1;
      return;
    }
    if (this.drivers.length === 1) {
      this.map.center = this.drivers[0].location;
      this.map.zoom = 16;
      return;
    }
    const latlngbounds = new google.maps.LatLngBounds();
    for (const location of this.drivers) {
      latlngbounds.extend(location.location);
    }
    this.map.fitBounds(latlngbounds);
  }

  colorForCount(count: number) {
    if (count === 0) {
      return "#87d068";
    } else if (count < 10) {
      return "orange";
    } else {
      return "#CF1322";
    }
  }

  openInfoWindow(driverId: number) {
    this.selectedDriverId = driverId;
    this.isInfoVisible = true;
  }

  handleCancel() {
    this.isInfoVisible = false;
  }

  refreshPage() {
    window.location.reload();
  }

  finishOccurrences() {
    this.isLoading = true;
    const { calledLocalAuthority, status, details, id } =
      this.watchingOccurrences[this.selectedDriverId];
    this.socket.emit("finishOccurrence", {
      calledLocalAuthority: !!calledLocalAuthority,
      status,
      details,
      id,
    }, () => {
      this.isLoading = false;
      this.isInfoVisible = false;
      delete this.watchingOccurrences[this.selectedDriverId];
      this.drivers = this.drivers.filter((d) =>
        d.driverId !== this.selectedDriverId
      );
    });

    this.refreshPage();
  }

  acceptOccurrence(occurrenceId: number) {
    this.pendingOccurrencesNotificationsRef[occurrenceId]?.close();
    delete this.pendingOccurrencesNotificationsRef[occurrenceId];
    this.socket.emit("acceptOccurrence", { id: occurrenceId }, () => {
      this.soundAlert.pause();
    });
  }

  handleSubmit() {
    this.modal.confirm({
      nzTitle: "<i>Você deseja encerrar a ocorrência?</i>",
      nzContent:
        "<b>As informações serão guardadas e a ocorrência será finalizada</b>",
      nzOnOk: () => this.finishOccurrences(),
      nzOkText: "Sim",
      nzCancelText: "Não",
    });
  }

  onSelect(event) {
    console.log(event);
  }

  private displayNotificationToAcceptOccurrence(occurrence: Occurrence) {
    this.soundAlert.focus();
    this.soundAlert.play().then();
    // occurrence.createdAt = new Date(
    //   occurrence.createdAt.setHours(occurrence.createdAt.getHours() - 3),
    // );
    this.watchingOccurrences[occurrence.driver.id] = occurrence;
    this.pendingOccurrencesNotificationsRef[occurrence.id] = this.modal.warning(
      {
        nzTitle: `<i>Ocorrência nº ${occurrence.id}</i>`,
        nzContent:
          "<b>Uma nova ocorrência foi aberta! <br>Clique no botão abaixo para aceitar e acompanhar a ocorrência.</br>",
        nzOnOk: () => this.acceptOccurrence(occurrence.id),
        nzOkText: "Aceitar",
      },
    );
    const notification = new Notification(
      `Ocorrência nº ${occurrence.id} aberta`,
      {
        body: "Uma nova ocorrência foi aberta!",
      },
    );
  }

  private deleteDriverLocationByDriverId(driverId: number) {
    const index = this.drivers.findIndex((y) => y.driverId === driverId);
    if (index >= 0) {
      this.drivers.splice(index, 1);
      this.drivers = this.drivers.slice();
    }
  }

  private getDriversLocation() {
    this.socket.emit(
      "getDriversHistoryLocation",
      null,
      (result2: DriverLocationWithId[]) => {
        this.drivers = result2.filter((r) =>
          r.driverId in this.watchingOccurrences
        );
        this.centerMap();
      },
    );
  }
}
