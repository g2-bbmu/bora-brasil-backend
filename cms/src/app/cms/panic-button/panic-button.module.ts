import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanicButtonRoutingModule } from './panic-button-routing.module';
import { PanicButtonComponent } from './panic-button.component';
import { SharedModule } from 'src/app/@components/shared.module';
import {NzModalModule} from 'ng-zorro-antd';

@NgModule({
  declarations: [PanicButtonComponent],
    imports: [
        CommonModule,
        PanicButtonRoutingModule,
        SharedModule,
        NzModalModule
    ]
})
export class PanicButtonModule { }
