import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Service } from 'src/app/@models/entities/service';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { BackendService } from 'src/app/@services/backend/backend.service';
import { Region } from 'src/app/@models/entities/region';
import { ServiceCategory } from 'src/app/@models/entities/service-category';
import { environment } from 'src/environments/environment';
import { Media } from 'src/app/@models/entities/media';

@Component({
  selector: 'app-management-services-view',
  templateUrl: './management-services-view.component.html',
  styleUrls: ['./management-services-view.component.css']
})
export class ManagementServicesViewComponent implements OnInit {
  form = new FormGroup({});
  model: Service = {};
  root: string;
  showUploadList = {
    showRemoveIcon: true
  };
  fileList = [];
  services: any[] = [];
  fields: FormlyFieldConfig[] = [
    {
      type: 'flex-layout',
      templateOptions: {
        fxLayout: 'column'
      },
      fieldGroup: [
        {
          type: 'flex-layout',
          templateOptions: {
            fxLayout: 'row',
            title: ''
          },
          fieldGroup: [
            {
              key: 'title',
              type: 'input',
              templateOptions: {
                label: 'Title',
                placeholder: 'Title',
                required: true
              }
            },
            {
              key: 'category',
              type: 'select',
              templateOptions: {
                label: 'Category',
                placeholder: 'Category',
                required: true,
                options: []
              }
            },
            {
              key: 'multiplicationFactor',
              type: 'input',
              templateOptions: {
                label: 'Multiplication Factor',
                placeholder: 'Multiplication Factor',
                type: 'number'
              }
            },
          ]
        },
        {
          key: 'canEnableVerificationCode',
          type: 'checkbox',
          templateOptions: {
            label: 'Verification Available'
          }
        }

      ]
    }
  ];
  constructor(private route: ActivatedRoute, private backend: BackendService, private router: Router) { }

  ngOnInit(): void {
    this.root = environment.root;
    this.route.data.subscribe(x => {
      this.model = x.item;
      if(x.item.media != null) {
        this.backend.getRows<Media>({ table: 'Media', filters: {id: x.item.media}}).then(y => {
          this.fileList.push({
            uid: -1,
            name: y.data[0].title,
            status: 'done',
            url: environment.root + y.data[0].address
          });
          this.fileList = this.fileList.slice()
        })
      }
      this.backend.getRows<ServiceCategory>({table: 'ServiceCategory'}).then(y => {
        this.backend.getField('category', this.fields).templateOptions.options = y.data.map(z => { return { label: z.title, value: z.id }})
      })
      this.backend.getRows<Region>({ table: 'Region'}).then(y => {
        this.services = y.data.map(z => {
          return {
          key: z.id,
          title: z.name,
          direction: (this.model.regions == null || this.model.regions.filter(i=>i == z.id).length < 1) ? 'left' : 'right'
          }
        });
      });
    });
  }

  async onSubmit() {
    this.model.regions = this.services.filter(x => x.direction == 'right').map(x => { return {id: x.key}});
    await this.backend.saveRow('Service', this.model, true, this.fields, ['media', 'regions']);
    if(this.model.id == null) {
      this.router.navigate(['../'], {relativeTo: this.route});
    } else {
      this.router.navigate(['../../'], {relativeTo: this.route});
    }
  }

  uploaded(event: any) {
    if(event.type == 'success') {
      this.model.media = event.file.response;
    } else {
      this.model.media = null;
    }
  }

}
