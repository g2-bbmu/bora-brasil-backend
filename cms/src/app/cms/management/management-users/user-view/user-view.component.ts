import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendService } from 'src/app/@services/backend/backend.service';
import { PermissionDefault, Operator } from 'src/app/@models/entities/operator';
import { NzMessageService } from 'ng-zorro-antd';
import { Franqueado } from '../../../../../../../backend/src/entities/franqueado';
import { SysLogAlteracaoDriver } from 'src/app/@models/entities/sys-log-alteracao-driver';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {
  form = new FormGroup({});
  permissionChangeIndicadorMotorista : boolean;
  permissionChangeFranquiaMotorista : boolean;
  franqueados: Franqueado[];
  cnpjFranquiaOperator: any;
  nomeFranquiaOperator: any;
  operador: any;
  model: Operator;
  newOperator: Operator;
  permissions = ['Driver','Rider','Region', 'Operator', 'PaymentRequest', 'PaymentGateway', 'Car', 'Service', 'Complaint', 'PanicButton'];
  permissionValues = {};
  fields: FormlyFieldConfig[] = [
    {
      type: 'flex-layout',
      templateOptions: {
        fxLayout: 'column'
      },
      fieldGroup: [
        {
          type: 'flex-layout',
          templateOptions: {
            fxLayout: 'row',
            title: ''
          },
          fieldGroup: [
            {
              key: 'firstName',
              type: 'input',
              templateOptions: {
                label: 'First Name',
                placeholder: 'First Name',
                required: true
              }
            },
            {
              key: 'lastName',
              type: 'input',
              templateOptions: {
                label: 'Last Name',
                placeholder: 'Last Name',
                required: true
              }
            },
            {
              key: 'userName',
              type: 'input',
              templateOptions: {
                label: 'CPF',
                placeholder: 'CPF',
                required: true
              }
            },
            {
              key: 'phoneNumber',
              type: 'input',
              templateOptions: {
                label: 'Phone Number',
                placeholder: 'Phone Number'
              }
            },
            {
              key: 'mobileNumber',
              type: 'input',
              templateOptions: {
                label: 'Mobile Number',
                placeholder: 'Mobile Number',
                required: true
              }
            },
            {
              key: 'address',
              type: 'input',
              templateOptions: {
                label: 'E-mail',
                placeholder: 'E-mail',
                required: true
              }
            },
            {
              key: 'password',
              type: 'input',
              templateOptions: {
                label: 'Senha',
                placeholder: 'Senha',
                required: true
              }
            }
          ]
        }

      ]
    }
  ];
  constructor(private route: ActivatedRoute, private router: Router, private backend: BackendService, private message: NzMessageService) { }

  ngOnInit(): void {
    this.operador = JSON.parse(localStorage.getItem('user'));
    this.route.data.subscribe(async (x) => {
      this.model = x.item;
      let franquias = await this.backend.getRows<Franqueado>({
        table: "franqueado",
      });
      this.franqueados = franquias.data;
      for(let per of this.permissions) {
        let perItem = x.item[`permission${per}`];
        this.permissionValues[per] = [
          { label: 'View', value: 'view', checked: perItem.includes(PermissionDefault.View) },
          { label: 'Update', value: 'update', checked: perItem.includes(PermissionDefault.Update) },
          { label: 'Delete', value: 'delete', checked: perItem.includes(PermissionDefault.Delete) }
        ]
      }
    });
    /*this.permissionChangeFranquiaMotorista = this.model.permissionChangeIndicadorMotorista;
    this.permissionChangeIndicadorMotorista = this.model.permissionChangeFranquiaMotorista;*/
    this.permissionChangeFranquiaMotorista = false;
    this.permissionChangeIndicadorMotorista = false;

    if(this.model.permissionChangeFranquiaMotorista == 1){
      this.permissionChangeFranquiaMotorista = true;
    }
    if(this.model.permissionChangeIndicadorMotorista == 1){
      this.permissionChangeIndicadorMotorista = true;
    }

  }

  async onSubmit() {
    for(let per in this.permissionValues) {
      this.model[`permission${per}`] = (this.permissionValues[per] as any[]).filter(x => x.checked).map(x => x.value);
    }
    if(this.model.userName == 'admin' && !this.model['permissionOperator'].includes(PermissionDefault.Update)) {
      this.message.error('Limiting admin user\'s operator Update capacities is not possible.');
      return;
    }

    await this.backend.saveRow('Operator', this.model, true,this.fields, this.permissions.map(x=> `permission${x}`));

    let newOperator =
        (await this.backend.getRows<Operator>(
          { table: "operator", filters: { userName: this.model.userName } },
        )).data[0]; 

        newOperator.permissionChangeIndicadorMotorista = 0;
        newOperator.permissionChangeFranquiaMotorista = 0;
    
        if(this.permissionChangeFranquiaMotorista == true){
          newOperator.permissionChangeFranquiaMotorista = 1;
        }
        if(this.permissionChangeIndicadorMotorista == true){
          newOperator.permissionChangeIndicadorMotorista = 1;
        }
    
        if(this.operador.cnpjFranquia != '000'){
          newOperator.cnpjFranquia = this.operador.cnpjFranquia;
          newOperator.nomeFranquia = this.operador.nomeFranquia;
        }           
        
    await this.backend.saveRow('Operator',
     {  
      id: newOperator.id,
      cnpjFranquia: this.cnpjFranquiaOperator,
      nomeFranquia: this.nomeFranquiaOperator,
      permissionChangeFranquiaMotorista: newOperator.permissionChangeFranquiaMotorista,
      permissionChangeIndicadorMotorista: newOperator.permissionChangeIndicadorMotorista
     },
      false);

    let hoje = new Date();
       let log = Object.create(SysLogAlteracaoDriver);
       log.idOperator = this.operador.id;
       log.idDriver = newOperator.id;
       log.data_alteracao = hoje;
       log.tabela = 'Operator';
       log.campo = 'Cadastro-Geral';
       await this.backend.saveRow('log_alteracao_Driver', log, false); 

    if(this.model.id == null) {
      this.router.navigate(['../'], {relativeTo: this.route});
    } else {
      this.router.navigate(['../../'], {relativeTo: this.route});
    }
  }

  async alterarFranquia(cnpjFranquia: string,nomeFranquia: string)  
  {   
    this.model.cnpjFranquia = cnpjFranquia;
    this.model.nomeFranquia = nomeFranquia;  

    this.cnpjFranquiaOperator = cnpjFranquia;
    this.nomeFranquiaOperator = nomeFranquia;
  }
}
