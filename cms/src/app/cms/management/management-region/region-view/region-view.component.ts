import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {GoogleMap} from '@angular/google-maps';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendService} from 'src/app/@services/backend/backend.service';
import {NzMessageService} from 'ng-zorro-antd';
import {Subscription} from 'rxjs';
import {Region} from 'src/app/@models/entities/region';
import {CURRENCY_LIST} from 'src/app/currencies';

@Component({
    selector: 'app-region-view',
    templateUrl: './region-view.component.html',
    styleUrls: ['./region-view.component.css']
})
export class RegionViewComponent implements OnInit {
    form = new FormGroup({});
    model: Region = {};
    mapLoaded = false;
    fields: FormlyFieldConfig[] = [
        {
            type: 'flex-layout',
            templateOptions: {
                fxLayout: 'column'
            },
            fieldGroup: [
                {
                    type: 'flex-layout',
                    templateOptions: {
                        fxLayout: 'row',
                        title: ''
                    },
                    fieldGroup: [
                        {
                            key: 'name',
                            type: 'input',
                            templateOptions: {
                                label: 'Name',
                                placeholder: 'Name',
                                required: true
                            }
                        },
                        {
                            key: 'currency',
                            type: 'select',
                            templateOptions: {
                                label: 'Currency',
                                placeholder: 'Currency',
                                options: CURRENCY_LIST,
                                required: true
                            }
                        }

                    ]
                }

            ]
        }
    ];

    fieldsBottom: FormlyFieldConfig[] = [
        {
            type: 'flex-layout',
            templateOptions: {
                fxLayout: 'column'
            },
            fieldGroup: [
                {
                    type: 'flex-layout',
                    templateOptions: {
                        fxLayout: 'row',
                        title: 'Fee Calculation'
                    },
                    fieldGroup: [
                        {
                            key: 'distanceFeeMode',
                            type: 'select',
                            templateOptions: {
                                label: 'Distance Fee Mode',
                                placeholder: 'Distance Fee Mode',
                                options: [
                                    {label: 'None', value: 'None'},
                                    {label: 'Pickup To Destination', value: 'PickupToDestination'}
                                ]
                            }
                        },
                        {
                            key: 'baseFare',
                            type: 'input',
                            templateOptions: {
                                label: 'Base Fare',
                                placeholder: 'Base Fare',
                                type: 'number'
                            }
                        },
                        {
                            key: 'perHundredMeters',
                            type: 'input',
                            templateOptions: {
                                label: 'Per 100m',
                                placeholder: 'Per 100m',
                                type: 'number'
                            }
                        },
                        {
                            key: 'perMinuteDrive',
                            type: 'input',
                            templateOptions: {
                                label: 'Per Minute',
                                placeholder: 'Per Minute',
                                type: 'number'
                            }
                        },
                        {
                            key: 'minimumFee',
                            type: 'input',
                            templateOptions: {
                                label: 'Minimum Fee',
                                placeholder: 'Minimum Fee',
                                type: 'number'
                            }
                        }
                    ]
                },
                {
                    type: 'flex-layout',
                    templateOptions: {
                        fxLayout: 'row',
                        title: 'Estimation'
                    },
                    fieldGroup: [
                        {
                            key: 'feeEstimationMode',
                            type: 'select',
                            templateOptions: {
                                label: 'Fee Estimation Mode',
                                placeholder: 'Fee Estimation Mode',
                                options: [
                                    {label: 'Static', value: 'Static'},
                                    {label: 'Dynamic', value: 'Dynamic'},
                                    {label: 'Ranged', value: 'Ranged'},
                                    {label: 'Ranged Strict', value: 'RangedStrict'},
                                    {label: 'Disabled', value: 'Disabled'}
                                ]
                            }
                        },
                        {
                            key: 'rangeMinusPercent',
                            type: 'input',
                            templateOptions: {
                                label: 'Minus Percent',
                                placeholder: 'Minus Percent',
                                type: 'number',
                                min: 0,
                                max: 100
                            }
                        },
                        {
                            key: 'rangePlusPercent',
                            type: 'input',
                            templateOptions: {
                                label: 'Plus Percent',
                                placeholder: 'Plus Percent',
                                type: 'number',
                                min: 0,
                                max: 100
                            }
                        }
                    ]
                },
                {
                    type: 'flex-layout',
                    templateOptions: {
                        fxLayout: 'row',
                        title: 'Payment'
                    },
                    fieldGroup: [
                        {
                            key: 'paymentMethod',
                            type: 'select',
                            templateOptions: {
                                label: 'Payment Method',
                                placeholder: 'Payment Method',
                                options: [
                                    {label: 'Cash & Credit', value: 'CashCredit'},
                                    {label: 'Only Credit', value: 'OnlyCredit'},
                                    {label: 'Only Cash', value: 'OnlyCash'}
                                ]
                            }
                        },
                        {
                            key: 'providerSharePercent',
                            type: 'input',
                            templateOptions: {
                                label: 'Commission Percent',
                                placeholder: 'Commission Percent',
                                type: 'number',
                                min: 0,
                                max: 100
                            }
                        },
                        {
                            key: 'providerShareFlat',
                            type: 'input',
                            templateOptions: {
                                label: 'Commission Flat',
                                placeholder: 'Commission Flat',
                                type: 'number',
                                min: 0,
                            }
                        }
                    ]
                },
                {
                    type: 'flex-layout',
                    templateOptions: {
                        fxLayout: 'row',
                        title: 'Misc'
                    },
                    fieldGroup: [
                        {
                            key: 'bookingMode',
                            type: 'select',
                            templateOptions: {
                                label: 'Booking Mode',
                                options: [
                                    {label: 'Only Now', value: 'OnlyNow'},
                                    {label: 'Time', value: 'Time'},
                                    {label: 'Date & Time', value: 'DateTime'},
                                    {label: 'Date & Time (Only Abosolute Hour)', value: 'DateTimeAbosoluteHour'}
                                ]
                            }
                        },
                        {
                            key: 'maxDestinationDistance',
                            type: 'input',
                            templateOptions: {
                                label: 'Maximum Destination Distance (meters)',
                                type: 'number',
                                min: 0
                            }
                        },
                        {
                            key: 'searchRadius',
                            type: 'input',
                            templateOptions: {
                                label: 'Search Radius (meters)',
                                type: 'number',
                                min: 0
                            }
                        }
                    ]
                },
                {
                    type: 'flex-layout',
                    templateOptions: {
                        fxLayout: 'row',
                        title: 'Cancellation Settings'
                    },
                    fieldGroup: [
                        {
                            key: 'waitingPeriodMinutes',
                            type: 'input',
                            templateOptions: {
                                label: 'Waiting Period Minutes',
                                type: 'number',
                                min: 0
                            }
                        }
                    ]
                }

            ]
        }
    ]

    @ViewChild(GoogleMap, {static: false}) map: GoogleMap;
    center = {lat: 24, lng: 12};
    markerOptions = {draggable: false};
    markerPositions: google.maps.LatLngLiteral[] = [];
    zoom = 4;
    display?: google.maps.LatLngLiteral;
    drawingManager: google.maps.drawing.DrawingManager;
    subscription: Subscription;

    constructor(private route: ActivatedRoute, private router: Router, private backend: BackendService, private messageService: NzMessageService) {
    }

    ngOnInit(): void {
    }

    idleMap(event: any) {
        if (this.mapLoaded)
            return;
        this.mapLoaded = true;
        this.route.data.subscribe(x => {
            this.model = x.item;
            if (this.model.id != null) {
                this.backend.getField('currency', this.fields).templateOptions.disabled = true;
            }
            if (this.model.location != null) {
                const latlngbounds = new google.maps.LatLngBounds();
                for (const poly of this.model.location) {
                    for (const location of poly) {
                        latlngbounds.extend(location);
                    }
                }
                this.map.fitBounds(latlngbounds);
            }
        });
        this.drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            map: this.map._googleMap,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [google.maps.drawing.OverlayType.POLYGON]
            }
        });
        google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (event) => {
            if (event.type === google.maps.drawing.OverlayType.POLYGON) {
                let ar = event.overlay.getPath().getArray();
                ar.push(ar[0]);
                if (this.model.location == null) {
                    this.model.location = [ar];
                } else {
                    this.model.location.push(ar);
                }
            }
        });
    }

    async onSubmit() {
        await this.backend.saveRow('Region', this.model, true, [...this.fields, ...this.fieldsBottom], ['location']);
        this.router.navigate(['management/regions'], {relativeTo: this.route.root});
    }

    clearMap() {
        this.model.location = [];
        this.model = JSON.parse(JSON.stringify(this.model));
    }
}
