import {DriverWallet} from "./driver-wallet";
import {Car} from "./car";
import {Fleet} from "./fleet";
import {Media} from "./media";
import {Gender} from "../enums/enums";
import {DriverTransaction} from "./driver-transaction";
import {RequestReview} from "./request-review";
import {Service} from "./service";
import {DriverToGateway} from "./driver-to-gateway";
import {BankAccount} from "./bank-account";

export enum DriverStatus {
    Online = 'online',
    Offline = 'offline',
    Blocked = 'blocked',
    InService = 'in service',
    WaitingDocuments = 'waiting documents',
    PendingApproval = 'pending approval',
    SoftReject = 'soft reject',
    HardReject = 'hard reject',
}

export enum PaymentGatewayDocumentsStatus {
    NaoEnviado = 'Não Enviado',
    EmAnalise = 'Em Análise',
    AprovadoAutomaticamente = 'Aprovado Automaticamente',
    AprovadoManualmente = 'Aprovado Manualmente',
    Negado = 'Negado',
}

export enum CnhCategory {
    A = 'Categoria A',
    B = 'Categoria B',
    C = 'Categoria C',
    D = 'Categoria D',
    E = 'Categoria E',
    AB = 'Categoria AB',
    AC = 'Categoria AC',
    AD = 'Categoria AD',
    AE = 'Categoria AE',
}

export enum CarStatus {
    own = 'Own Car',
    rented = 'Rented Car',
}

export enum TypeAccount {
    natural = 'Natural Person',
    legal = 'Legal Person',
}

export class Driver {
    id: number;


    cpf?: string;


    firstName?: string;


    lastName?: string;

    cnpjFranquia: string;

    nomeFranquia: string;

    birthDate: string;


    website?: string;

    certificateNumber?: string;


    cnhEar: boolean;


    cnhCategory: CnhCategory;


    cnhExpirationDate: number;

    mobileNumber?: number;


    email?: string;

    description: string;

    bankAccount: BankAccount;

    paymentGatewayDocumentsStatus: PaymentGatewayDocumentsStatus;


    paymentGatewayDocumentsPendingDate?: Date;

    paymentGatewayDocumentsActivatedDate?: Date;


    paymentGatewayDocumentsEnabledDate?: Date;

    paymentGatewayDocumentsDeniedDate?: Date;


    wallet: DriverWallet[];

    car?: Car;

    fleet?: Fleet;

    carColor?: string;

    carStatus: boolean;

    carProductionYear?: number;

    carPlate?: string;

    carMedia?: Media;

    status: DriverStatus;

    rating?: number;

    reviewCount: number;

    mediaId?: number;

    media?: Media;


    gender: Gender;


    registrationTimestamp: Date;

    lastSeenTimestamp?: number;

    accountNumber?: string;

    bankName: string;


    bankRoutingNumber?: string;

    bankSwift?: string;


    cep?: string;


    address?: string;

    addressTwo?: string;

    number?: string;

    city?: string;

    state?: string;

    infoChanged: boolean;

    acceptTerms: Date;

    notificationPlayerId?: string;

    documentsNote?: string;

    completed: boolean;

    documents: Media[];

    transactions: DriverTransaction[];

    paymentRequests: PaymentRequest[];

    requests: Request[];

    requestReviews: RequestReview[];

    services: Service[];

    gatewayIds?: DriverToGateway[];

    patrocinador: string;
}
