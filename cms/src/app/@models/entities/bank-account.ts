export class BankAccount {

    id: number;
    holder: string;
    cpfCnpj: string;
    bankCode: string;
    agencyCode: string;
    accountNumber: string;
    type: string;
}
