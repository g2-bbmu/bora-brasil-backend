
export class SysLogAlteracaoDriver {
    id: number;

    idOperator: number;

    idDriver: number;

    data_alteracao: Date;

    tabela: string;

    campo: string;

    valor_anterior: string;

    valor_atual: string;
}
