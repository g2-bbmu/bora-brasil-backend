import {BrowserModule, Title} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {en_US, NZ_I18N} from 'ng-zorro-antd';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import ptBR from '@angular/common/locales/pt-PT';
import {FormlyModule} from '@ngx-formly/core';
import {FlexLayoutType} from './@components/flex-layout.type';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {SharedModule} from './@components/shared.module';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {BackendHttpInterceptor} from "./@services/http/backend-http-interceptor";
import {FormlyFieldInput} from "./cms/drivers/formly-field-input";
import { NgxMaskModule, IConfig } from 'ngx-mask';
export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};


registerLocaleData(en);
registerLocaleData(ptBR);

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        FlexLayoutType,
        FormlyFieldInput
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SharedModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        TranslateModule.forRoot({
                defaultLanguage: 'en',
                loader: {
                    provide: TranslateLoader,
                    useFactory: HttpLoaderFactory,
                    deps: [HttpClient]
                }
            }
        ),
        FormlyModule.forRoot(
            {
                types: [
                    { name: 'flex-layout', component: FlexLayoutType },
                    { name: 'dateFormatInput', component: FormlyFieldInput },
                ],
            }
        ),
        NgxMaskModule.forRoot(options),
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
    ],
    providers: [
        {provide: NZ_I18N, useValue: en_US},
        {provide: LOCALE_ID, useValue: 'pt-PT'},
        {provide: HTTP_INTERCEPTORS, useClass: BackendHttpInterceptor, multi: true}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(private title: Title, private translator: TranslateService) {
        translator.get('branding.page.title').subscribe(x => {
            title.setTitle(x);
        })
    }
}
